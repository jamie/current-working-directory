#!/usr/bin/python3

import os
import datetime
import re

for root, dirs, files in os.walk("src", topdown=False):
    for name in files:
        if name == 'discussion.mdwn':
            continue;     
        if name.endswith("comment"):
            continue
        new_root = root.replace("src", "content")
        if not os.path.isdir(new_root):
            os.mkdir(new_root)
        new_name = name.replace(".mdwn", ".md")
        old_path = os.path.join(root, name)
        new_path = os.path.join(new_root, new_name)
        stat = os.stat(old_path)
        date = datetime.datetime.fromtimestamp(stat.st_mtime).isoformat()
        with open(old_path,'r') as f:
            lines = f.readlines()
        with open(new_path, 'w') as f:
            f.writelines("---\n")
            header_open = True 
            for line in lines:
                if header_open:
                    if not re.search('^\[\[\!(meta|tag)', line):
                        f.writelines("date: {0}\n".format(date))
                        f.writelines("---\n\n")
                        header_open = False
                    else:
                        title_line = re.search('^\[\[\!meta title="?([^"]+)"?\]\]', line)
                        tags_line = re.search('^\[\[\!tag "?(.*)"?\]\]', line)
                        if title_line:
                            f.writelines("title: \"{0}\"\n".format(title_line.group(1)))
                        elif tags_line:
                            tags = tags_line.group(1).strip().split(' ')
                            f.writelines("tags: [\"{0}\"]\n".format(('","'.join(tags))))
                        else:
                            print("Unknown meta tag: {0}".format(line))
                else:
                    f.writelines(line)

        #print("{0} => {1} with date: {2}".format(old_path, new_path, date))
