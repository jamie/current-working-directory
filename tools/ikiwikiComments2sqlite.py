#!/usr/bin/python3

import os
import datetime
import re
import sqlite3

db = sqlite3.connect('comments.db')
for root, dirs, files in os.walk("src", topdown=False):
    for name in files:
        if not name.endswith("comment"):
            continue
        full_path = os.path.join(root, name)
        in_content = False
        with open(full_path,'r') as f:
            lines = f.readlines()
            author = 'Anonymous'
            date = None
            content = ""
            path = root[3:] 
            for line in lines:
                author_line = re.search('username="([^"]+)"', line)
                if author_line:
                    author = author_line.group(1)
                    continue
                date_line = re.search('date="([0-9]+-[0-9]+-[0-9]+T[0-9:]+Z)"', line)
                if date_line:
                    date_iso_string = date_line.group(1)
                    date_object = datetime.datetime.strptime(date_iso_string,"%Y-%m-%dT%H:%M:%S%z")
                    date = date_object.timestamp()
                    continue
                content_line = re.search('content="""', line)
                if content_line:
                    in_content = True
                    continue
                end_content_line = re.search('"""\]', line)
                if end_content_line:
                    content = content.replace('\\\"', '"')
                    sql = "INSERT INTO comment VALUES(null, 1, ?, ?, ?, ?)"
                    print(content)
                    print(date)
                    db.cursor().execute(sql, (path, date, author, content))
                    db.commit()
                    continue
                elif in_content:
                    content += line
                else:
                    continue



