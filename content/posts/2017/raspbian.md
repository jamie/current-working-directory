---
title: "Late to the Raspberry Pi party"
tags: ["debian", "sysadmin"]
date: 2017-05-18T23:37:06.532298
---

I finally bought my first raspberry pi to setup as a router and wifi access point.

It wasn't easy.

I first had to figure out what to buy. I think that was the hardest part.

I ended up with:

 * Raspberry PI 3 Model B A1.2GHz 64-bit quad-core ARMv8 CPU, 1GB RAM (Model number: RASPBERRYPI3-MODB-1GB)
 * Transcend USB 3.0 SDHC / SDXC / microSDHC / SDXC Card Reader, TS-RDF5K (Black). I only needed this because I don't have one already and I will need a way to copy a raspbian image from my laptop to a micro SD card.
 * Centon Electronics Micro SD Card 16 GB (S1-MSDHC4-16G). This is the micro sd card.
 * Smraza Clear case for Raspberry Pi 3 2 Model B with Power Supply,2pcs Heatsinks and Micro USB with On/Off Switch. And this is the box to put it all in.

I already have a cable matters USB to ethernet device, which will provide the second ethernet connection so this device can actually work as a router.

I studiously [followed the directions to download the raspbian image and copy it to my micro sd card](https://www.raspberrypi.org/documentation/installation/installing-images/). I also [touched a file on the boot partition called ssh](https://www.raspberrypi.org/documentation/remote-access/ssh/) so ssh would start automatically. Note: I first touched the ssh file on the root partition (sdb2) before realizing it belonged on the boot partition (sdb1). And, despite ambiguous directions found on the Internet, lowercase 'ssh' for the filename seems to do the trick.

Then, I found the IP address with the help of NMAP (`sudo nmap -sn 192.168.69.*`) and tried to ssh in but alas...

    Connection reset by 192.168.69.116 port 22

No dice.

So, I re-mounted the sdb2 partition of the micro sd card and looked in var/log/auth.log and found:

    May  5 19:23:00 raspberrypi sshd[760]: error: Could not load host key: /etc/ssh/ssh_host_ed25519_key
    May  5 19:23:00 raspberrypi sshd[760]: fatal: No supported key exchange algorithms [preauth]
    May  5 19:23:07 raspberrypi sshd[762]: error: key_load_public: invalid format
    May  5 19:23:07 raspberrypi sshd[762]: error: Could not load host key: /etc/ssh/ssh_host_rsa_key
    May  5 19:23:07 raspberrypi sshd[762]: error: key_load_public: invalid format
    May  5 19:23:07 raspberrypi sshd[762]: error: Could not load host key: /etc/ssh/ssh_host_dsa_key
    May  5 19:23:07 raspberrypi sshd[762]: error: key_load_public: invalid format
    May  5 19:23:07 raspberrypi sshd[762]: error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key
    May  5 19:23:07 raspberrypi sshd[762]: error: key_load_public: invalid format

How did that happen? And wait a minute...

    0 jamie@turkey:~$ ls -l /mnt/etc/ssh/ssh_host_ecdsa_key
    -rw------- 1 root root 0 Apr 10 05:58 /mnt/etc/ssh/ssh_host_ecdsa_key
    0 jamie@turkey:~$ date
    Fri May  5 15:44:15 EDT 2017
    0 jamie@turkey:~$

Are the keys embedded in the image? Isn't that wrong?

I fixed with:

    0 jamie@turkey:mnt$ sudo rm /mnt/etc/ssh/ssh_host_*
    0 jamie@turkey:mnt$ sudo ssh-keygen -q -f /mnt/etc/ssh/ssh_host_rsa_key -N '' -t rsa
    0 jamie@turkey:mnt$ sudo ssh-keygen -q -f /mnt/etc/ssh/ssh_host_dsa_key -N '' -t dsa
    0 jamie@turkey:mnt$ sudo ssh-keygen -q -f /mnt/etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa
    0 jamie@turkey:mnt$ sudo ssh-keygen -q -f /mnt/etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
    0 jamie@turkey:mnt$

NOTE: I just did a second installation and this didn't happen. Maybe something went wrong as I experiment with SSH vs ssh on the boot partition?

Then I could ssh in. I removed the `pi` user account and added my ssh key to /root/.ssh/authorized_keys and put a new name "mondragon" in the /etc/hostname file.

And... I upgraded to Debian stretch and rebooted.

Then, I followed [these instructions for fixing the wifi](https://raspberrypi.stackexchange.com/questions/53245/pi-3-raspbian-stretch-testing-disables-wifi) (replacing the firmware does still work for me).

I plugged my cable matters USB/Ethernet adapter into the device so it would be recognized, but left it dis-connected.

Next I started to configure the device to be a wifi access point [using this excellend tutorial](https://frillip.com/using-your-raspberry-pi-3-as-a-wifi-access-point-with-hostapd/), but decided I wanted to setup my networks using systemd-networkd instead.

Since /etc/network/interaces already had eth0 set to manual (because apparently it is controlled by dhcpcd instead), I didn't need any modifications there.

However, I wanted to use the dhcp client built-in to systemd-networkd, so to prevent dhcpcd from obtaining an IP address, I purged dhcpcd:

    apt-get purge dhcpcd5

I was planning to also use systemd-networkd to name the devices ([using \*.link files](https://www.freedesktop.org/software/systemd/man/systemd.link.html)) but nothing I could do could convince systemd to rename them, so I gave up and added /etc/udev/rules.d/70-persistent-net.rules:

		SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="b8:27:eb:ce:b5:c3", ATTR{dev_id}=="0x0", ATTR{type}=="1", NAME:="wan"
		SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="a0:ce:c8:01:20:7d", ATTR{dev_id}=="0x0", ATTR{type}=="1", NAME:="lan"
		SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="b8:27:eb:9b:e0:96", ATTR{dev_id}=="0x0", ATTR{type}=="1", NAME:="wlan"
		 
(If you are copying and pasting the mac addresses will have to change.)

Then I added the following files:

    root@mondragon:~# head /etc/systemd/network/*
    ==> /etc/systemd/network/50-lan.network <==
    [Match]
    Name=lan

    [Network]
    Address=192.168.69.1/24

    ==> /etc/systemd/network/55-wlan.network <==
    [Match]
    Name=wlan

    [Network]
    Address=10.0.69.1/24

    ==> /etc/systemd/network/60-wan.network <==
    [Match]
    Name=wan

    [Network]
    DHCP=v4
    IPForward=yes
    IPMasquerade=yes
    root@mondragon:~#

Sadly, IPMasquerade doesn't seem to work either for some reason, so...

    root@mondragon:~# cat /etc/systemd/system/masquerade.service 
    [Unit]
    Description=Start masquerading because Masquerade=yes not working in wan.network.

    [Service]
    Type=oneshot
    ExecStart=/sbin/iptables -t nat -A POSTROUTING -o wan -j MASQUERADE

    [Install]
    WantedBy=network.target
    root@mondragon:~#

And, systemd DHCPServer worked, but then it didn't and I couldn't figure out how to debug, so...

    apt-get install dnsmasq

Followed by:

    root@mondragon:~# cat /etc/dnsmasq.d/mondragon.conf 
    # Don't provide DNS services (unbound does that).
    port=0

    interface=lan
    interface=wlan

    # Only provide dhcp services since systemd-networkd dhcpserver seems
    # flakey.
    dhcp-range=set:cable,192.168.69.100,192.168.69.150,255.255.255.0,4h
    dhcp-option=tag:cable,option:dns-server,192.168.69.1
    dhcp-option=tag:cable,option:router,192.168.69.1

    dhcp-range=set:wifi,10.0.69.100,10.0.69.150,255.255.255.0,4h
    dhcp-option=tag:wifi,option:dns-server,10.0.69.1
    dhcp-option=tag:wifi,option:router,10.0.69.1

    root@mondragon:~#

It would probably be simpler to have dnsmasq provide DNS service also, but I happen to like unbound:

    apt-get install unbound

And...

    root@mondragon:~# cat /etc/unbound/unbound.conf.d/server.conf 
    server:
        interface: 127.0.0.1
        interface: 192.168.69.1
        interface: 10.0.69.1

        access-control: 192.168.69.0/24 allow
        access-control: 10.0.69.0/24 allow

        # We do query localhost for our stub zone: loc.cx
        do-not-query-localhost: no

        # Up this level when debugging.
        log-queries: no
        logfile: ""
        #verbosity: 1

        # Settings to work better with systemcd
        do-daemonize: no
        pidfile: ""
    root@mondragon:~# 

Now on to the wifi access point.

    apt-get install hostapd

And the configuration file:

    root@mondragon:~# cat /etc/hostapd/hostapd.conf
    # This is the name of the WiFi interface we configured above
    interface=wlan

    # Use the nl80211 driver with the brcmfmac driver
    driver=nl80211

    # This is the name of the network
    ssid=peacock

    # Use the 2.4GHz band
    hw_mode=g

    # Use channel 6
    channel=6

    # Enable 802.11n
    ieee80211n=1

    # Enable WMM
    wmm_enabled=1

    # Enable 40MHz channels with 20ns guard interval
    ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

    # Accept all MAC addresses
    macaddr_acl=0

    # Use WPA authentication
    auth_algs=1

    # Require clients to know the network name
    ignore_broadcast_ssid=0

    # Use WPA2
    wpa=2

    # Use a pre-shared key
    wpa_key_mgmt=WPA-PSK

    # The network passphrase
    wpa_passphrase=xxxxxxxxxxxx

    # Use AES, instead of TKIP
    rsn_pairwise=CCMP
    root@mondragon:~#

The hostapd package doesn't have a systemd start up file so I added one:

    root@mondragon:~# cat /etc/systemd/system/hostapd.service 
    [Unit]
    Description=Hostapd IEEE 802.11 AP, IEEE 802.1X/WPA/WPA2/EAP/RADIUS Authenticator
    Wants=network.target
    Before=network.target
    Before=network.service

    [Service]
    ExecStart=/usr/sbin/hostapd /etc/hostapd/hostapd.conf

    [Install]
    WantedBy=multi-user.target
    root@mondragon:~#

My last step was to modify /etc/ssh/sshd_config so it only listens on the lan and wlan interfaces (listening on wlan is a bit of a risk, but also useful when mucking with the lan network settings to ensure I don't get locked out).

