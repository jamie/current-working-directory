---
title: "Diversity doesn't help the bottom line"
tags: ["debian","mayfirst", "oppression"]
date: 2018-04-30T09:54:09.577453
---

A Google software engineer's [sexist screed against diversity](http://gizmodo.com/exclusive-heres-the-full-10-page-anti-diversity-screed-1797564320) has been making the rounds lately. 

Most notable are the offensive and mis-guided statements about gender
essentialism, which honestly make the thing hard to read at all.

What seems lost in the hype, however, is that his primary point seems quite
accurate. In short: If Google successfully diversified it's workforce, racial
and gender tensions would *increase* not decrease,  divisiveness would spread
and, with all liklihood, Google could be damaged. 

Imagine what would happen if the thousands of existing, mostly male, white and
Asian engineers, the majority of whom are convinced that they play no part in
racism and sexism, were confronted with thousands of smart and ambitious women,
African Americans and Latinos who were becoming their bosses, telling them to
work in different ways, and taking "their" promotions.

It would be a revolution! I'd love to see it. Google's bosses definitely do
not. 

That's why none of the diversity programs at Google or any other major tech
company are having any impact - because they are not designed to have an
impact. They are designed to boost morale and make their existing engineers
feel good about what they do.

Google has one goal: to make money. And one strategy: to design software
that people want to use. One of their tactics that is highly effective is
building tightly knit groups of programmers who work well together. If the
creation of hostile, racist and sexist environments is a by-product - well,
it's not one that affects their bottom line.

Would Google make better software with a more diverse group of engineers?
Definitely! For one, if African American engineers were working on their facial
recognition software, it's doubtful [it would have mistaken people with black
faces for gorillas](https://www.usatoday.com/story/tech/2015/07/01/google-apologizes-after-photos-identify-black-people-as-gorillas/29567465/).

However, if the perceived improvement in software outweighed the risks of
diversification, then Google would not waste any time on feel-good programs and
trainings - they would simply build a jobs pipeline and change their job
outreach programs to recruit substantially more female, African Americans and
Latino candidates.

In the end, this risk avoidance and failure to perceive the limitations of
homogeneity is the achiles heel of corporate software design.

Our challenge is to see what we can build outside the confines of corporate
culture that prioritizes profits, production efficiency, and stability. What
can we do with teams that are willing to embrace racial and gender tension,
risk diviseveness and be willing to see benefits beyond releasing version 1.0?

