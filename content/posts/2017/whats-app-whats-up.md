---
title: "What's Up with WhatsApp?"
tags: ["debian", "security", "whatsapp"]
date: 2017-01-13T21:02:48.069248
---

Despite my jaded feelings about corporate Internet services in general, I was suprised to learn that [WhatsApp's end-to-end encryption was a lie](https://www.theguardian.com/technology/2017/jan/13/whatsapp-backdoor-allows-snooping-on-encrypted-messages). In short, it is possible to send an encrypted message to a user that is intercepted and effectively de-crypted without the sender's knowledge.

However, I was even more surprised to read [Open Whisper Systems critique of the original story](https://whispersystems.org/blog/there-is-no-whatsapp-backdoor/), claiming that it is not a backdoor because the WhatsApp sender's client is always notified when a message is de-crypted. 

The Open Whisper Systems post acknowledges that the WhatsApp sender can choose to disable these notifications, but claims that is not such a big deal because the WhatsApp server has no way to know which clients have this feature enabled and which do not, so intercepting a message is risky because it could result in the sender realizing it.

However, there is a fairly important piece of information missing, namely: as far as I can tell, the setting to notify users about key changes is disabled by default. 

So, using the default installation, your end-to-end encrypted message could be intercepted and decrypted without you or the party you are communicating with knowing it. How is this not a back door? And yes, if the interceptor can't tell whether or not the sender has these notifications turned on, the interceptor runs the risk of someone knowing they have intercepted the message. Great. That's better than nothing. Except that there is strong evidence that many powerful governments on this planet routinely risk exposure in their pursuit of compromising our ability to communicate securely. And... not to mention non-governmental (or governmental) adversaries for whom exposure is not a big deal.

Furthermore a critical reason for end-to-end encrption is so that your provider does not have the technical capacity to intercept your communications. That's simply not true of WhatsApp. It *is* true of Signal and OMEMO, which requires the active participation of the sender to compromise the communication. 

Why in the world would you distribute a client that not only has the ability to surpress such warnings, but has it enabled by default?

Some may argue that users regularly dismiss notifications like "fingerprint has changed" and that this problem is the achilles heal of secure communications. I agree. But... there is still a monumental difference between a user absent-mindedly dismissing an important security warning and never seeing the warning in the first place.

This flaw in WhatsApp is a critical reminder that secure communications doesn't just depend on a good protocol or technology, but on trust in the people who design and maintain our systems. 

