---
title: "Docker in Debian"
tags: ["debian", "sysadmin"]
date: 2017-10-10T12:10:04.790767
---

It's not easy getting Docker to work in Debian.

It's not in stable at all:

    0 jamie@turkey:~$ rmadison docker.io
    docker.io  | 1.6.2~dfsg1-1~bpo8+1 | jessie-backports | source, amd64, armel, armhf, i386
    docker.io  | 1.11.2~ds1-5         | unstable         | source, arm64
    docker.io  | 1.11.2~ds1-5         | unstable-debug   | source
    docker.io  | 1.11.2~ds1-6         | unstable         | source, armel, armhf, i386, ppc64el
    docker.io  | 1.11.2~ds1-6         | unstable-debug   | source
    docker.io  | 1.13.1~ds1-2         | unstable         | source, amd64
    docker.io  | 1.13.1~ds1-2         | unstable-debug   | source
    0 jamie@turkey:~$ 

And a [problem with runc](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=877329) makes it really hard to get it working on Debian unstable.

These are the steps I took to get it running today (2017-10-10).

Remove runc (allow it to remove containerd and docker.io):

    sudo apt-get remove runc

Install docker-runc (now in testing)

    sudo apt-get install docker-runc

Fix containerd package to depend on docker-runc instead of runc:

    mkdir containerd
    cd containerd
    apt-get download containerd 
    ar x containerd_0.2.3+git20170126.85.aa8187d~ds1-2_amd64.deb
    tar -xzf control.tar.gz
    sed -i s/runc/docker-runc/g control
    tar -c md5sums control | gzip -c > control.tar.gz
    ar rcs new-containerd.deb debian-binary control.tar.gz data.tar.xz
    sudo dpkg -i new-containerd.deb

Fix docker.io package to depend on docker-runc instead of runc.

    mkdir docker
    cd docker
    apt-get download docker.io
    ar x docker.io_1.13.1~ds1-2_amd64.deb
    tar -xzf control.tar.gz
    sed -i s/runc/docker-runc/g control
    tar -c {post,pre}{inst,rm} md5sums control | gzip -c > control.tar.gz
    ar rcs new-docker.io.deb debian-binary control.tar.gz data.tar.xz
    sudo dpkg -i new-docker.io.deb

Symlink `docker-runc` => `runc`

    sudo ln -s /usr/sbin/docker-runc /usr/sbin/runc

Keep `apt-get` from upgrading until this bug is fixed:

    printf "# Remove when docker.io and containerd depend on docker-runc
    # instead of normal runc
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=877329
    Package: runc 
    Pin: release * 
    Pin-Priority: -1 

    Package: containderd 
    Pin: release * 
    Pin-Priority: -1 

    Package: docker.io
    Pin: release * 
    Pin-Priority: -1" | sudo tee /etc/apt/preferences.d/docker.pref

Thanks to [coderwall](https://coderwall.com/p/hes3ha/change-the-dependencies-of-a-deb-package) for tips on manipulating deb files.
