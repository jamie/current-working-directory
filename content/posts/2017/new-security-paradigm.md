---
title: "Re-thinking Web App Security"
tags: ["debian", "security"]
date: 2017-02-15T11:56:30.507661
---

An organizer friend interested in activating a rapid response network to counter Trump-era ICE raids on immigrants asked me about any existing simple and easy tools that could send out emergency SMS/text message alerts. 

I thought about it and ended up writing my first [pouchdb](https://pouchdb.com/) web application to accomplish the task. For the curious, you can see it [in action](https://sms.workingdirectory.net/) and [browse the source code](https://gitlab.com/jamie/basenotify). To use it to send SMS, you have to [register for a Twilio account](https://www.twilio.com/try-twilio) - you can get a free account that has very restricted SMS sending capability or pay for full functionality.

The project is unlike anything I have done before.

I chose pouchdb because it stores all of your contacts *in your browser* not on a server somewhere in the so-called cloud. (You can also choose to sync to a server, a feature I have not yet implemented.)

The implications of storing your data locally are quite profound. 

## Classic Web App

Let's first consider the more common web application: You visit a web site (the same web site that your colleagues visit, or in the case of a massive application like gmail.com, the same web site that everyone in the world visits). Then, you login with your own unique username and password, which grants you access to the portion the database that you are suppose to have access to.

For most use-cases, this model is fairly ideal:

  * If you have a technically competent host, your data is backed up regularly and the database is available nearly 100% of the time
  * If you have a politically trust-worthy host, your host will notify you and put up a fight before turning any of your data over to a government agent
  * If you drop your phone in the toilet you can always login from another computer to access your data
  * If you save your password in your browser and your laptop is stolen, you can always change your password to prevent the thief from accessing your data
  * You can easily share your data with others by creating new usernames and passwords

However, there are some downsides:

 * If your host is not technically competent or polically trust-worthy, you could lose all of your data to a hard drive crash or subpoena
 * Even if your host *is* competent, all of your data is one previously undiscovered vulnerability away from being hacked
 * Even if your host *is* politically trust-worthy, you cannot always stop a subpoena, particularly given the legal escalations of tools like national security letters

## pouchdb no sync

Assuming you are accessing your database on a device with an encrypted disk *and* you manage your own backups, pouchdb without synchoronizing provides the most privacy and autonomy. You have complete control of your data and you are not dependent on any server operator.

However, the trade-offs are harsh:

 * Availability: if you lose your device, you would need to restore from backup - which is much more difficult than simply logging in from another device
 * Collaboration: you simply can't share this data with anyone else

It seems this model is fairly useless except in very tight corner cases.

## pouchdb that synchronizes to a server

With this model, you avoid the trade-offs of the no sync model (hooray!). However, you also lose all of the privacy benefits, and it's even *worse*: your data can be compromised either via a server breach or via a compromise of any of the devices you are using. If any of these devices lack encrypted disks, then it's borderline reckless.

On the other hand, you gain a huge benefit in terms of reliability. If the server goes down, loses your data, fails to backup or is taken offline by a legal order, you can still function perfectly well and can optionally choose to sync to a different host.

## Conclusions

Ultimately, we need to better evaluate the trade-offs between privacy and availability for each given use of a database and try to make the best decision.

And... keep working on new models. For example, it seems an ideal middle ground would be to sync in a peer-to-peer fashion with our colleagues (see [PeerPouch](https://github.com/natevw/PeerPouch)) or sync to a server under your control in your office.
