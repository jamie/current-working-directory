---
title: "Networking in 2016"
tags: ["debian","desktop"]
date: 2016-08-21T10:20:03.316586
---

So many options, so little time.

Many years ago I handled all my network connections via /etc/network/interfaces.

Then, I was in desperate need of a Internet connection and all I had was a borrowed USB cell phone modem. My friends running [NetworkManager](https://wiki.gnome.org/Projects/NetworkManager) just plugged the stick in and were online. I was left with the task of figuring out how to manually configure this piece of hardware without being online. I ended up borrowing my friend's computer. Then, when I got home, I installed NetworkManager.

Once I had NetworkManager installed, I decided it was easier to find, connect to and manage passwords of wireless networks using a graphical tool rather than digging through the copious output of commands run from my terminal and trying to keep track of the passwords. So long wireless.

Then I had to help a colleague get on our Virtual Private Network. Wow. There's a NetworkManager GUI for that too. If I'm going to support my colleauge with this tool... I guess I should use it as well. I also managed to write a dispatcher script in /etc/NetworkManager/dispatcher.d that calls `su -c "/usr/bin/smbnetfs /media/smbnetfs" -l jamie` when it receives and action of "vpn-up" and `umount /media/smbnetfs 2>/dev/null` on "vpn-down." Now I can mount the samba share by simply connecting to the VPN via NetworkManager.

My cable connections are almost always configured using DHCP. Almost everything else is in NetworkManager, why not move enp1s0f2 as well?

What's left? My final piece is my bridge. I still [run and manage my own KVM guests](/posts/2012/managing-kvm/) and I have a bridge to handle that traffic. I first decided to move this functionality to systemd.network because systemd can not only handle the bridge, but can also handle IP Forwarding, DHCP service, and best of all, IP Masquerading. Well, almost... not IP Masquerading after all, [at least not yet](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=787480).

Without IP masquerading, I figured I'd go with NetworkManager. Having all networking in the same place gives me an illusion of control at best and at worst makes it easier to debug. So, I setup a crufty script in /etc/NetworkManager/dispatcher.d that configures masquerading via `iptables` everytime either my wireless or wired network goes up or down, which I'm not crazy about. Maybe when [#787480](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=787480) is fixed I'll got back to systemd. I also edited /etc/sysctl.conf to enable #net.ipv4.ip_forward=1. Then I changed it back and added my own file in /etc/sysctl.d to do the same thing. Then I deleted that file and added `sysctl net.ipv4.ip_forward=1` and `sysctl net.ipv4.ip_forward=0` to my crufty dispatcher script. I decided to do without DHCP - I can manually configure the few KVM instances that I run. 

Now /etc/network/interfaces is so lonely.

## Update 2017-08-17

For some reason placing `su -c "/usr/bin/smbnetfs /media/smbnetfs" -l jamie` in my `/etc/NetworkManager/dispatcher.d/` script stopped working. According to `journalctl`:

    Aug 21 10:01:13 turkey systemd[1]: session-c18.scope: Killing process 27385 (smbnetfs) with signal SIGTERM.
    Aug 21 10:01:13 turkey systemd[1]: session-c18.scope: Killing process 27390 (smbnetfs) with signal SIGTERM.
    Aug 21 10:01:13 turkey systemd[1]: session-c18.scope: Killing process 27391 (smbnetfs) with signal SIGTERM.

I have no idea why and wasn't really sure how to debug it (running the command `su -c "/usr/bin/smbnetfs /media/smbnetfs" -l jamie` via a root terminal was successful).

So I decided to create a system service:

    0 jamie@turkey:~$ cat /etc/systemd/system/mount-borges.service
    [Unit]
    Description=Mount PTP's SMB file server borges

    [Service]
    User=jamie
    ExecStart=/usr/bin/smbnetfs /media/smbnetfs -f
    ExecStop=/bin/umount /media/smbnetfs
    0 jamie@turkey:~$

And now instead of calling `su -c "/usr/bin/smbnetfs /media/smbnetfs" -l jamie` on vpn-up and `umount /media/smbnetfs 2>/dev/null` on vpn-down I call `systemctl start mount-borges` and `systemctl stop mount-borges` instead.


