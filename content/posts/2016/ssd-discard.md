---
title: "Wait... is that how you are supposed to configure your SSD card?"
tags: ["debian","sysadmin"]
date: 2016-09-08T13:43:46.232288
---

I bought a laptop with only SSD drives a while ago and based on a limited amount of reading, added the "discard" option to my /etc/fstab file for all partitions and happily went on my way expecting to avoid the [performance degradation problems that happen on SSD cards without this setting](https://en.wikipedia.org/wiki/Trim_(computing)).

Yesterday, after a [several month ordeal](https://support.mayfirst.org/ticket/11870), I finally installed SSD drives in one of [May First/People Link's](https://mayfirst.org/) servers and started doing more research to find the best way to set things up.

I was quite surprised to [learn that my change in /etc/fstab accomplished nothing](http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/). Well, not entirely true, my /boot partition was still getting empty sectors reported to the SSD card.

Since my filesystem is on top of LVM and LVM is on top of an encrypted disk, those messages from the files system to the disk were not getting through. I learned that when I tried to run the `fstrim` command on one of the partitions and received the message that the disk didn't support it. Since my /boot partition is not in LVM or encrypted, it worked on /boot.

I then made the necessary changes to /etc/lvm/lvm.conf and /etc/crypttab, restarted and... same result. Then I ran `update-initramfs -u` and rebooted and now fstrim works. I decided to remove the discard option from /etc/fstab and will set a cron job to run fstrim periodically.

Also, I learned of some [security implications of using trim on an encrypted disk](http://asalor.blogspot.de/2011/08/trim-dm-crypt-problems.html) which don't seem to outweigh the benefits.


