---
title: "Trusted Mobile Device: How hard could it be?"
tags: ["debian","mobile","security","signal"]
date: 2016-08-31T23:39:41.406545
---

I bought a new phone. After my experiences with [signal](/posts/2016/signal/) and the helpful comments readers gave regarding the ability to run android and signal without Google Play using [microg](https://microg.org/) I thought I would give it a shot.

Since microg reports that [signature spoofing](https://github.com/microg/android_packages_apps_GmsCore/wiki/Signature-Spoofing) is required and comes out-of-the-box with [omnirom](http://omnirom.org/) I thought I'd aim for installing omnirom's version of Android 6 (marshmallow) after years of using [cyanomgenmod](http://www.cyanogenmod.org/)'s version of Android.

The Nexus line of phones seemed well-supported by omnirom in particular (and the alternative ROM community in general) so I bought a Nexus 5x.

I carefully followed the [directions for installing omnirom](https://docs.omnirom.org/Installing_Omni_on_your_device) however when it came time to boot into omnirom, I just got the boot sequence animation over and over again.

Frustrated, I decided to go back to cyanogenmod and see if I could [use one of the microg recommended methods for getting signature spoofing to work](https://github.com/microg/android_packages_apps_GmsCore/wiki/Signature-Spoofing). The easiest seemed to be [Needle by moosd](https://github.com/moosd/Needle) but alas [no such luck with Marshmallow](https://github.com/moosd/Needle/issues/16). Someone else [forked the code and might fix it one day](https://github.com/ale5000-git/tingle/issues/2). I then spent too much time trying to understand what [xposed is](http://repo.xposed.info/) before I gave up understanding it and just tried to install it ([woops, looks like the installer page is out of date](http://repo.xposed.info/module/de.robv.android.xposed.installer) so instead I followed [sketchy instructions from a forum thread](http://forum.xda-developers.com/showthread.php?t=3034811)). Well, to make a long story short it resulted in a boot loop.

So, I decided to return to omnirom. After reading some vague references to omnirom and supersu, I decided to flash both of them together and voila, it worked!

Next, I decided to enable full disk encryption. Not so fast. After clicking through the screens and hitting the final confirmation, my phone rebooted and spent the next 5 hours showing me the omnirom boot animation. Somehow, powering down and starting again resulted in a working machine, but no disk encryption.

After much web searching, guessing and trial and error, I fixed the problem by clicking on the SuperSU option to "Full unroot" the device (I pressed "no" when prompted to attempt to restore stock image). Then I rebooted and followed the directions to encrypt the device. And it worked! Hooray!

I had to reboot and re-flash the supersu to regain su privileges.

All was great. 

The first root action I decided to take was to install the [cryptfs](https://f-droid.org/repository/browse/?fdfilter=cryptfs&fdid=org.nick.cryptfs.passwdmanager) program from [f-droid](https://f-droid.org/) because using the same password to decrypt your device as you use to unlock the screen seems either tedious or insecure.

That process didn't work so well. I got a message saying: use this command from a root shell before you reboot: `vdc cryptfs changepw <password>`. I followed the advice, carefully typing in my 12 character password which includes numbers and letters.

Then, I happily did what I expected to be my last reboot when, to my horror, I was prompted to decrypt my disk with ... a numeric-only keypad. 

That wasn't going to work. At this point I had already spent 5 days and about 8 hours on this project. Sigh. So, I started over.

Guess what? It only took me 25 minutes but, it seems that cryptfs is broken. Even with a numeric password it fails. Ok, I guess I need a long pin to unlock my phone. This time it only took my 15 minutes to wipe and re-install everything.

There are only two positive things I can think of:

 * [TWRP](https://twrp.me/), which provides the recovery image, is really great. Everytime something went wrong I booted into the TWRP recovery image and could fix anything.
 * I'm starting to get used to the error on startup warning me that "Your device is corrupt. It can't be trusted and may not work properly." It's a good thing to remember about all digital devices.

p.s. I haven't even tried to install microg yet... which was the whole point.
