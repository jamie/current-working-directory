---
title: "Mobile Instant Messaging"
tags: ["debian", "mobile", "signal", "xmpp", "tox"]
date: 2016-06-01T21:04:22.535258
---

Now that I've [gone down the signal road](/posts/2016/signal/), when can I get off of it?

The two contenders I've found for more politically conscious mobile-friendly instant messaging are [Tox](https://tox.chat/) and [XMPP](https://en.wikipedia.org/wiki/XMPP).

And the problems to solve are:

 * Implement end-to-end encryption 
 * Receive messages the moment they are sent without draining the battery

[The Tox Project](https://tox.chat) has a great design (peer-to-peer) and [handles end-to-end encryption](https://tox.chat/faq.html#how-tox-privacy) as part of its core design.

However, [the Antox mobile client has not only failed to solve the battery drain problem but seems to also have a serious bandwidth issue as well](https://github.com/Antox/Antox/issues/159). Also, what is up with this [drama](https://blog.tox.im/2016/04/01/litigation/)?

How about XMPP?

For years, XMPP clients have been using [OTR](https://en.wikipedia.org/wiki/Off-the-Record_Messaging) - and many instant messaging applications support it, including [ChatSecure](https://chatsecure.org/). And now, there seems to be quite a bit of excitement around implementing a [better protocol called OMEMO](https://en.wikipedia.org/wiki/OMEMO). Incidentally, the OMEMO protocol uses the same [Double Ratchet (previously referred to as the Axolotl Ratchet)](https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm) protocol that Signal uses.

While there are some [irritating hiccups](https://github.com/anurodhp/Monal/issues/9#issuecomment-208063040) around using the available GPL library for Double Ratchet on iPhone apps, it seems like these issues are being sorted out and soon we'll have some kind of standard way for XMPP clients to exchange end-to-end encrypted messages.

Let's move on to notifications and battery drain. 

If you believe the author of the Android Conversations App [there doesn't seem to be a problem](https://conversations.im/#optimizations). However, [client state indication](https://xmpp.org/extensions/xep-0352.html) seems to be the only issue related to battery drainage he addresses and [I couldn't figure out how to easily install that extension on our Debian Jessie prosody instance](https://support.mayfirst.org/ticket/11822#comment:1) since it isn't included in the [prosody-modules package in Debian](https://packages.debian.org/jessie-backports/prosody-modules) and it requires [additional modules for it to work](https://modules.prosody.im/mod_csi.html).

Chris Ballinger, the author of ChatSecure (iOS and Android XMPP app), is [less optimistic](https://chatsecure.org/blog/fixing-the-xmpp-push-problem/) about this problem in general. In short, he thinks we need a proper "push" mechanism and has even [started to implement one](https://chatsecure.org/blog/chatsecure-v32-push/). At the same time, a new (and different) XMPP standard called [Push Notifications - XEP-0357](https://xmpp.org/extensions/xep-0357.html) has been released and it is [even implemented in Prosody](https://modules.prosody.im/mod_cloud_notify.html) (although not yet available in Debian).

So the future seems bright, right? Well, not exactly. All of this "push" activity seems to solve this problem: A federated/decentralized application cannot properly use [Apple's APNs](https://en.wikipedia.org/wiki/Apple_Push_Notification_Service) or Google's [GCM](https://en.wikipedia.org/wiki/Google_Cloud_Messaging). In the [words](https://chatsecure.org/blog/fixing-the-xmpp-push-problem/) of Chris Ballinger: 

> The biggest problem is that there is no easy way to send push messages
> between my app and your app. For me to send a push to one of your app’s users
> on iOS, I must first obtain an APNs SSL certificate/key pair from you, and
> one of your user’s ‘push token’ that uniquely identifies their device to
> Apple. These push tokens are potentially sensitive information because they
> allow Apple to locate your device (in order to send it a push).

So, even if we manage to get one of these two standards for push notifications up and running, we have only succeeded in solving Signal's centralization problem, not the dependence on Google Play Services and Apple Push Network (in fact it's quite mysterious to me [how you could even use the Prosody implementation of Push Notifications with GCM or APN](https://hg.prosody.im/prosody-modules/file/218a3d3f7f97/mod_cloud_notify/README.markdown)).

So... what we really would need would be to figure out how to implement one of these two push standards and then get it to work with an alternative to GCM and APN (perhaps [MQTT](http://mqtt.org/))? Which, I think [would require changes to the XMPP client](https://ollieparsley.com/2013/05/20/using-mqtt-as-a-gcm-replacement-for-android-push-notifications/).

Geez. I may be on Signal longer than I planned.
