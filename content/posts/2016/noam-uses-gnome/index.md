---
title: "Noam use Gnome"
tags: ["debian","family"]
date: 2016-08-13T21:04:54.104877
---

I don't quite remember when I read [John Goerzen](http://changelog.complete.org)'s post about [teaching a 4 year old to use the linux command line with audio](http://changelog.complete.org/archives/6915-a-4-year-old-linux-command-line-and-microphone) on [planet Debian](http://planet.debian.org/). According to the byline it was published nearly 2 years before Noam was born, but I seem to remember reading it in the weeks after his birth when I was both thrilled at the prospect of teaching my kid to use the command line and, in my sleepless stupor, not entirely convinced he would ever be old enough.

Well, the time came this morning. He found an old USB key board and discovered that a green light came on when he plugged it in. He was happily hitting the keys when Meredith suggested we turn on the monitor and open a program so he could see the letters appear on the screen and try to spell his name.

After 10 minutes in Libre Office I remembered John's blog and was inspired to start writing a bash script in my head (I would have to stop the fun with Libre Office to write it so the pressure was on...). In the end it was only a few minutes and I came up with:

    #!/bin/bash

    while [ 1 ]; do
      read -p "What shall I say? "
      espeak "$REPLY"
    done

It was a hit. He said what he wanted to hear and hit the keys, my job was to spell for him.

![Noam uses Gnome Terminal to talk](/posts/2016/noam-uses-gnome/noam-uses-gnome-terminal.png)

Oh, also: he discovered key combinations that did things that were unsurprising to me (like taking the screen grab above) and also things that I'm still scratching my head about (like causing a prompt on the screen that said: "Downloading shockwave plugin." No thanks. And, how did he do that? 
