---
title: "Signal and Google Cloud Services"
tags: ["debian","signal","mobile","security"]
date: 2016-06-01T19:08:13.814413
---

I just installed [Signal](https://whispersystems.org/blog/signal/) on my Android phone.
 
It wasn't an easy decision. I have been running [Cyanogenmod](http://www.cyanogenmod.org/), a Google-free version of Android, and installing apps from [F-Droid](https://f-droid.org/), a repository of free software android apps, for several years now. This setup allows me to run all the applications I need without Google accessing any of my cell phone data. It has been a remarkably successful experiment leaving me with all the phone software I need. And it's consistent with my belief that Google's size, reach and goals are a menace to the left's ability to develop the autonomous communications systems on the Internet that we need to achieve any meaningful political change.
 
However, if I want to install Signal, I [have to](https://github.com/WhisperSystems/Signal-Android/issues/560) install Google Play, and the only way to install Google Play is to install Google's base line set of apps and to connect your cell phone to a Gmail account. 
 
Why in the world would Signal require Google Play? There is plenty of discussion of the technical debate on this topic, but politically it boils down to this: security is about trade-offs, and the trade-offs you find important are based on your politics. While I consider Signal to be on the same team in the big picture, I think Signal's winning a short term victory for more massively adopted end-to-end encryption at the expense of a longer term and more important struggle for autonomous communication systems specifically for communities fighting corporate power (of which Google itself is an important target) and fighting US hegemony on a global scale.
 
Furthermore, Signal's lead developer's [outright hostility to alternate clients connecting to the centralized signal servers](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165) demonstrates another political decision that favors central control over any confidence in a broader movement control over what could have become a power new protocol for secure communication. And his [refusal to grant an exemption for other developers to use just the encryption algorythm](https://github.com/anurodhp/Monal/issues/9#issuecomment-208063040) is frustrating to say the least.
 
Given this reasoning, why install Signal? The main reason is because I have yet to convince anyone to remove Google Apps from their phone and Signal, right now, represents a dramatic improvement over most people's current communications habit. And, when it comes down to it, I need to run what I recommend.
 
I'm still running both [conversations](https://conversations.im/) and [Antox](https://tox.chat/) which are far better alternatives in the long run. However until they gain more widespread adoption, I'll be experimenting with Signal.
 
Technical Details
 
Oh, and by the way, installing Google Apps nearly bricked my phone.
 
Cyanogenmod's web site conveniently [provides instructions](https://wiki.cyanogenmod.org/w/Google_Apps) for installing Google Apps. However, confusingly, they provide two different links to choose from, one is from [OpenGapps](http://opengapps.org) which provided my a link to download the zip file to flash on a non-https page (the link itself was over https). The other link was to an https-enabled page [on androidfilehost.com](https://www.androidfilehost.com/) that offered a non-https download (but did provide a md5 checksum). I am now sure why people offering software downloads don't enable https from start to finish (well, maybe I do - I haven't yet enabled https on this site...).
 
However, more confusing is that both links were to different files. The OpenGapps one seemed to be a daily build and the androidfilehost was to a file with the date 20140606 in it's name, suggesting it was built nearly 2 years ago.
 
I went with the daily build.
 
When I restarted, I got the error "Unfortunately, Android Keyboard (Aosp) Has Stopped." If you search the web for this error you will see loads of people getting it. However, none of them seem to be using an encrypted disk. Yes, that is a bigger problem since you can't enter your encrypted disk passphrase if your keyboard app has crashed and you can't boot your phone if you can't even hit enter at the passphrase prompt. If you can't boot, you can't clear the keyboard app cache or most of the suggestions. In fact, when you press and hold the power key you don't even get the option to reboot into recovery mode. And, if you connect your device to your USB cable and run the `adb` tool on your computer, the tool reports that you are not authorized to connect your device.
 
Oh damn. Did I just brick my phone?
 
Fortunately, you can still boot into recovery mode on a Samsung S4 by powering it off. Then, press and hold the up volume button while turning it on.
 
In recovery mode, I as able to convince the adb tool to connect to my device and I copied over the other Gapps zip file from androidfilehost.com and flashing that one seems to have fixed the problem.
 
Once I booted, I ran Google Play and opted to create a new Google Account. I chose the option to not sync my data. Then, I checked in Settings -> Accounts I saw that a Google Account was there and was synchronizing. Great. What was it synchronizing? I clicked the account, then clicked "Accounts and Privacy" and ensured that everything was turned off. Let's hope that works.

[Update]

Signal's option to take over as your default SMS client and send un-encrypted normal SMS messages while sending encrypted messages to other Signal users is a very good way to smooth adoption. Unfortunately I had some problems with MMS message for which I [found a work-around](https://github.com/WhisperSystems/Signal-Android/issues/4878#issuecomment-221449778). But sheesh, [lots of MMS problems](https://github.com/WhisperSystems/Signal-Android/issues?q=is%3Aissue+is%3Aopen+mms+label%3Amms) at the moment. 
