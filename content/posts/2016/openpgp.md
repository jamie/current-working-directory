---
title: "Should we be pushing OpenPGP?"
tags: ["debian","security","openpgp","email"]
date: 2016-12-15T08:53:16.640955
---

Bjarni Rúnar, the author of [Mailpile](https://www.mailpile.is) released a [blog about recent blogs disparaging OpenPGP](https://www.mailpile.is/blog/2016-12-13_Too_Cool_for_PGP.html). It's a good read.

There's one reason to support OpenPGP missing from the blog: OpenPGP protects you if your mail server is hacked. I'm sure that [Debbie Wasserman Schultz wishes she had been using OpenPGP](https://en.wikipedia.org/wiki/2016_Democratic_National_Committee_email_leak).

Having said all of this... OpenPGP didn't make [my recent list of security tips](https://network.progressivetech.org/online-protection). That ommission is for two reasons:

 * I've never trusted my phone enough to store my OpenPGP keys on it. However, now that I am encrypting my data partition on the phone, should I re-consider? I use the K-9 email client which has had OpenPGP support for years, should I recommend that other people use K-9 and upload their keys to their phones? Suggesting that people use OpenPGP without the ability to use it on your phone seems like an empty suggestion. What about OpenPGP on the iPhone?
 * I'm waiting for Mailiple 1.0 to be released so I have a viable suggestion for how people can start using encryption now on their desktops. The complexity of using Thunderbird with Enigmail (and the [uncertain future of Thunderbird](http://kb.mozillazine.org/Future_of_Thunderbird)) make it a hard sell. Should I re-consider? What about [Mailvelope](https://www.mailvelope.com/)? Should I be encouraging people to use Mailvelope with their Gmail, etc. accounts?



