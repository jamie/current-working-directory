---
title: "Signal and Mobile XMPP Update"
tags: ["debian","signal","mobile", "security"]
date: 2016-06-04T23:03:27.832074
---

First, many thanks to [Planet Debian](http://planet.debian.org) readers for your thoughtful and constructive feedback to my [Signal](/posts/2016/signal/) and [Mobile Instant Messaging](/posts/2016/mobile-instant-messaging/) blogs. I learned a lot.

Particularly useful was the comment directing me to [Daniel Gultsch's The State of Mobile in 2016](https://gultsch.de/xmpp_2016.html) post.

I had previously listed the outstanding technical challenges as: 

 * Implement end-to-end encryption
 * Receive messages the moment they are sent without draining the battery

I am now fairly convined that both problems are well-solved on Android via the [Conversations](https://conversations.im/) app and a [well-tuned XMPP server](https://support.mayfirst.org/ticket/11822) (I had no idea how easy it was to [install your own Prosody modulues](https://prosody.im/doc/installing_modules) -- the client state indicator module is only 22 lines of lua code!)

I think the current technical challenges could be better summarized as: adding iOS (iPhone) support. Both end-to-end encryption and receiving messages consistently seem to be hurdles. However, it seems that Chris Ballinger and the Chat Secure team are well on their way toward solving the [push issue](https://chatsecure.org/blog/chatsecure-v32-push/) and facing [funder skittishness](https://github.com/ChatSecure/ChatSecure-iOS/issues/376#issuecomment-218902284) on the encryption front. Nonetheless, but *seem* to be progressing.

With the obvious technical hurdles in progress, we have the luxury of talking about the less obvious ones - particularly the ones requiring trade-offs.

In particular: Signal replaces your SMS client. It looks and feels like an SMS client and automatically sends un-encrypted messages to everyone your address book that is not on signal and sends encrypted messages to those that are on signal.

The significance of this feature is hard to over-state. It differentiates tools by and for technically minded people and those designed for a mass audience. 

When I convince people to use Conversations, in contrast, I have to teach them to:

 * Create an entirely new address book by entering addresses for your friends that you don't already have
 * Use a new and different app for sending encrypted messages

For most people who don't (yet) have their friends XMPP addresses or for people who don't have any friends who use XMPP, it means that they will install it, send me a few messages and then never use it again.

The price Signal pays for this convenience is steep: Signal seems to synchronize your entire address book to their servers so they can keep a map of cell phone numbers to signal users. It's not only creepy (I get a text message everytime someone in my address book joins Signal) but it's flies in the face of expectations for a privacy-minded application.

How could we take advantage of this feature, without the privacy problems?

What if...

 * Our app could send both XMPP messages and SMS messages
 * Everytime you added a new XMPP contact, it added the contact to your address book with a new XMPP field
 * Anytime you send a message to a contact with an XMPP field filled in, it would send via XMPP and otherwise it would send a normal SMS message

The main downside (which Signal faces as well) is that you have to contend with the complexities of sending SMS messages on top of the work needed to write a well-functioning XMPP client. As I mentioned in my [Signal](/posts/2016/signal/) blog, there are no shortage of [MMS bugs against Signal](https://github.com/WhisperSystems/Signal-Android/issues?q=is%3Aissue+is%3Aopen+mms+label%3Amms). Nobody wants that head-ache. 

Additinally, we would still lose one Signal feature: with Signal, when a user joins, everyone automatically sends them encrypted messages. With this proposed app, each user would have to manually add the XMPP address and have no way of knowing when one of their friends gets an XMPP address. 

Any other ideas?





