---
title: "Nice Work Apertium"
tags: ["debian","language-justice"]
date: 2016-08-17T10:07:36.813455
---

For the last few years I have been periodically testing out [apertium](https://www.apertium.org/) and today I did again and was pleasantly surprised with the quality of the english-spanish and spanish-english translations (and also their nifty web site translator).

So, I dusted off some of my geeky code to make it easier to use and continue testing.

For starters...

		sudo apt-get install apertium-en-es xclip coreutils

Then, I added the following to my .muttrc file:

		macro pager <F2> "<enter-command>set pipe_decode<enter><pipe-entry> sed '1,/^$/d' | apertium es-en | less<enter><enter-command>unset pipe_decode<enter>" "translate from spanish"

If you press F2 while reading a message in spanish it will print out the English translation.

If you use vim, you can create ~/.vim/plugins/apertium.vim with:

		function s:Translate()
			silent !clear
			execute "! apertium en-es " . bufname("%") . " | tee >(xclip)"
		endfunction
		command Translate :call <SID>Translate()

Then, you can type the command:

:Translate

And it will display the English to Spanish translation of the file you are editing and copy the translation into your clip board so you can paste it into your document.

