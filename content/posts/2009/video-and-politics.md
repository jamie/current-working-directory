---
tags: ["video"]
title: "The politics of video distribution on the Internet"
date: 2009-01-08T09:32:44.185780
---

Technology, in particular the Internet, has revolutionized the use of
video among progressive activists over the last 15 years. During this
short span of time, the production and distribution of progressive video
has moved from the domain of a small number of individuals and
organizations with specialized skills and equipment to being within
reach of nearly every movement activist or organization.

Despite the enormous contribution of the left toward democratizing media
in general, and the influence of the left on the Internet itself, the
first wave of this revolution on the Internet has corporate roots. In
2009, the ubiquity and ease with which anyone can distribute video to a
large audience boils down to two develpments:

 * Common file format. After years of struggle and confusion, the flash
	 video format (.flv) has emerged as the dominant standard for
	 delivering video on the Internet. The emergence of a dominant file
	 format has lead to the wide adoption of flash video enabled web
	 browsers, relieving video producers of the need to save their videos
	 in multiple and often confusing formats in order to ensure that their
	 work is accessible to everyone. Flash video is a foramt created and
	 controlled by the Adobe corporation.

 * Internet server infrastructure to distribute videos. Despite Free
	 Speech Video's initial foray into hosting video (which was well ahead
	 of its time), YouTube led the way for the masses to upload and share
	 their videos. They've been followed by Blip TV and other corporations
	 who have used their capital to finance large scale, centralized and
	 tightly controlled spaces for activists to distribute their video
	 files.

The second wave of this revolution is yet to come. Given the economic
crisis, and the growing strength of the progressive movement both in the
US and internationally, now is a critical time for the left to define
and develop the next wave in video distribution.

# Topics #

## Accessibility ##

Video collectives have a long history and intimate knowledge around the
issue of accessibility to video. Having experienced the transitions from
3/4", to Hi-8 to MiniDV and other formats, and havig gone through the
expensive and time consuming process of archiving work for future
generations, video collectives have a lot to contribute toward the
debate over how we should be saving our work online. Who should define
the video formats we use? How do we ensure they will stay free and open?

## Ownership/Control ##

In an era where exact duplicates of video works can easily be made, how
should we define ownership of our work? How does Internet distribution
affect ownership? Under what conditions should we be distributing our
work?

## Infrastructure ##

Who owns the airwaves becomes who owns the servers? How do we assert
control over the means of distribution? How do we communicate this
principle to our allies?

## Collaboration ##

Groups like Paper Tiger and Indy Media have redefined the way we can
make television by introducing a collaborative approach. How can we
extend this idea from a small group of producers to the Internet? How
can Internet distribution contribute to collaboration?

## Sustainability ##

The corporate Internet is a graveyard of failed ideas. How can we build
a distribution system that won't fail when it runs out of venture
capital?

## What's happening now? ##

There are a number of promising developments in the area of
Internet/video that are relevant in this discussion.

[Internet Archive](http://www.archive.org/). The Internet archive is a
nonprofit building a library of online artifacts (including video).

[Miro](http://www.getmiro.com/). Miro TV is an effort to combine a free media
player with free software that aggregates existing video on the
Internet, providing a "TV Guide" for leftist video on the Internet.

[Engage Media](http://www.engagemedia.org/). Engage Media is an activist video
sharing site focused on Asia Pacific (based on Australia). They are an example
of a YouTube alternative for activists.

[Transmission.cc](http://transmission.cc/). The Transmission Network is an
international coalition of groups working on online video distribution tools
for social justice and media democracy.

[Open Video Alliance](http://openvideoalliance.org/). The Open Video Alliance
seems similar in scope to the Transmission Network. They are having a
conference in July in NYC.




