---
tags: ["security","openpgp"]
title: "Key transition"
date: 2009-01-08T09:32:44.185780
---

The time has finally come.

After putting it off, I've finally started transitioning my gpg key.

I've [published instructions](/pages/key-transition-2009-05-10.txt) for how to
find (and sign) my new key. Thanks to dkg and micah for the help in getting
this out.

If you have a gpg key, you may want to [check to see if your key should be transitioned](http://www.debian-administration.org/users/dkg/weblog/48) as well.
