---
tags: [ "sysadmin" ]
title: "Installing Debian via qemu"
date: 2009-01-08T09:32:44.185780
---

I've been experimenting with kvm and qemu and run into some conceptual and
practical problems. Here are some of my discoveries and short cuts.

I started by downloading the netinst Debian install image as
debian-503-amd64-netinst.iso. Next I created an empty image:

		qemu-img create -f raw default.img 3GB 

This file will hold the virtual server.

Next, I launched the virtual server, booting from the debian installer, passing
the file I just created as the first hard drive:

		 qemu-system-x86_64 -boot d -cdrom debian-503-amd64-netinst.iso -hda default.img -curses

This command is like booting a server with a single hard drive and the Debian
install CD in the CD ROM drive (and configuring your bios to boot from the CD).

I pass the -curses option to tell qemu to not try to use the graphics card
(since I'm doing this over ssh).

And here's the real trick: Since the Debian installer tries to use frame
buffers, I just got "640 x 480 Graphic Mode" on my screen. Following the Debian
manual, I hit "Esc" on my keyboard and was magically dropped into text mode.
Next I typed: 

		install fb=false 
		
to launch the installer and ensure that frame buffer mode was disabled. From
that point on I was taken through a text-based installation.

Everything went smoothly until the Grub install where I seemed to be hit with a [qemu/grub 1 bug](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=425648).

I killed the qemu process and made a bootable grub iso:

		mkdir -p grub/boot/grub
		cp /usr/lib/grub/x86_64-pc/stage2_eltorito grub/boot/grub/
		mkisofs -R -b grub/boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -o grub.iso grub

Those steps created a file called grub.iso. I then booted with that file:

		 qemu-system-x86_64 -boot d -cdrom grub.iso -hda default.img -curses

And was dropped into a grub shell. I typed:

		root (hd0,0) 
		kernel /boot/vmlinuz-2.6.26-2-amd64 root=/dev/hda1 ro
		initrd /boot/initrd.img-2.6.26-2-amd64

And finally typed: boot and behold the installation booted!

Now the problem is... how do get grub properly installed??

Well... the solutions seems to be to install grub2. Before I could do that, I had to figure out how to get networking properly setup. Thanks to some [help](http://oldwiki.openwrt.org/RunningKamikazeOnQEMUHowTo.html) I changed the /etc/network/interfaces file on the host to:

		auto br0
		iface br0 inet static
			address 209.234.253.26
			netmask 255.255.255.224
			gateway 209.234.253.1
			bridge_ports eth0

And edited /etc/qemu-up to:

		sudo -p "Password for $0:" /sbin/ifconfig $1 0.0.0.0 promisc up
		sudo /usr/sbin/brctl addif br0 $1

Then, I restarted the virtual server with:

		 qemu-system-x86_64 -boot d -cdrom grub.iso -hda default.img -curses -net nic -net tap

I went through the same grub business. Then, from inside the virtual host, I configured /etc/network/interfaces with a real IP address.

With network access, I installed grub2 and then ran:

		grub-intall /dev/hda
		udpate-grub
		shutdown -h now

And then restarted the virtual server with:
		qemu-system-x86_64 -hda default.img -curses -net nic -net tap

grub2 is graphical - so I can't access it during the bootup (there's probably a text mode command for grub2 somehwere).

At this point I finally got around to reading the README.Debian file in the qemu docs directory and learned about the Debian command qemu-make-debian-root. Wow. That's makes things a lot easier.

According to the docs you can run:

		qemu-make-debian-root 3000 lenny http://ftp.debian.org/ debian.img

To use deboostrap to create a debian image. Then, launch with:

		qemu debian.img -kernel /vmlinuz

Haven't gotten this to work though...

And another thing - I realized that I didn't have kqemu installed (check for /dev/kqemu). I installed it with:

		aptitude install kqemu-sources

And then ran:

		m-a

To build the module.

Followed by:

		modprobe -v kqemu

To load the module.


