---
title: "Ikiwiki upgrading and disabled comments"
tags: [ "ikiwiki", "cms" ]
date: 2009-01-08T09:32:44.185780
---

Although not nearly as hairy as [helping David Swanson
upgrade](https://support.mayfirst.org/ticket/2100) upgrade [his Drupal
blog](http://davidswanson.org), the ikiwiki upgrade from version 2 to version 3
has been a bit hairy.

My biggest difficulty is that my published version of the blog is running
Debian Etch (and pulling ikwiki from Debian Lenny) while my laptop is running
Debian Squeeze. Hence, my laptop pulled in ikiwiki 3, which as prompted me to upgrade my blog, whereas my published wiki is still running 2.53. 

My temporary solution to this problem is to disable commenting and make my
published blog a plain rsync'ed version of the version on my laptop.  Sorry!
Commenting will return soon.


