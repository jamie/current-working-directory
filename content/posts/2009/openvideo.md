---
tags: ["video"]
title: "Open Video Alliance 2009"
date: 2009-01-08T09:32:44.185780
---

Here are some of my notes from attending the second day of the [Open Video
Conference](http://openvideoconference.org) in June 2009.

##Opening Remarks##

Jonathan Zittrain gave the opening address. Striking was the prediction (from the
conference organizers) that by 2013 90% of all video traffic will be video.  We
can expect bandwidth costs for providers will go through the roof. Jonathan
point's is the the big content providers ultimately sign contracts with the big
Internet service providers. The basis is: the content providers suddenly, once
they become big enough, can approach the ISPs and say: your subscribers want
our content - let's strike a deal. And the deal is that they don't have to have
their costs sky rocket as their bandwidth goes up.

Where does that leave small providers? It's an impossible to sustain growth
model.

Makes me think a lot more about varnish - a web proxy server that allows you to
create a network of servers proxying static content (like video) across many
different servers on different providers. As DSL and cable providers are
rolling out very high bandwidth personal packages - it could provide an
opportunity for our members to contribute their own home bandwidth to the
organization for distributing our video bandwidth.

##Free Editing Software##

A new one!! [pitivi](http://www.pitivi.org/wiki/Main_Page). Still [doesn't work
for me](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=533839) - but the demo
makes it look like the uber simple video editing software of my dreams.

##DMCA Take downs##

[YouTomb](http://www.pitivi.org/wiki/Main_Page) reports that it seems like the
number of YouTube videos being pulled down is going down, but in fact the truth
is that they are coming down so quickly after going up that they can't track
it. The average is 8 days.

In one case, a YouTube user's video was taken down after a big company used his
video. YouTube assumed the big company's work was the original.

We were also entertained by Scott Smitelli's [experiment with YouTube's
fingerprinter](http://www.csh.rit.edu/~parallax/). YouTube maintains a digital
fingerprint database of copyright works and automatically scans uploads against
this database and automatically takes them down.

Some of the conclusions about how you can fool the fingerprinter: 

 * Don't bother changing metadata, title, description etc.
 * Altering pitch or speed works (as little as 3% slowdown or 4% speedup)
 * Volume changes don't work 
 * Removing parts of the song works in most cases.

As for video:

 * Inverting colors works
 * Removing initial a few seconds works
 * Speedup/slow down works

[Chilling Effects](http://www.chillingeffects.org/) was mentioned as an
important resource in fighting DMCA take downs.

A striking point made on a few occasions is that copyright law is causing
increasing numbers of people to build private networks - a terrible trend for a
public network.


## HTML 5 and the video tag ##

Yes - it's coming! Support is coming soon in Opera, Safari, and of course
Firefox. Soon, we'll be able to include streaming video in our web page by
using the &lt;video&gt; tag just like we use the &lt;img&gt; tag for an image
file.  And, it looks like there's broad support for ogg/theora as the default
encoding.

The problem, of course, is that Internet Explorer doesn't support it. How are we going to make this transition given their (still) majority share of the browser sphere?

And one last tidbid on the topic of ogg: [A web site that will convert your
video to ogg](http://www.firefogg.org/make/index.html).

## Bittorrent and pirate bay ##

The closing drew a comparison between the seizure of [Elite
Torrents](http://en.wikipedia.org/wiki/Operation_D-Elite) in the United States,
in which network admins went to jail with little or no notice among the rest of
the country. In comparison, when [Pirate
Bay](http://en.wikipedia.org/wiki/Pirate_bay) came under pressure, there were
demonstrations and even a political party that jumped in to support the freedom
of sharing.


