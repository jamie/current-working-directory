---
tags: ["oppression", "security"]
title: "Internet security and oppression"
date: 2009-01-08T09:32:44.185780
---

Oppression - particularly race and gender based oppression - has a long sordid
history. One important aspect of oppression is exclusion. It doesn't matter
whether it's officially acknowledged (segregation) or hidden (old boys
networks) or something in between. It also doesn't matter whether it's
explicitly based on race or gender or if that exclusion happens in a de facto
way. Exclusion, in all forms, is a central tenant of oppression.

Tech culture within the Internet is one of the most gender and race-based
exclusive cultures in the mainstream today. Sadly, the radical tech sub-culture
of the Internet rarely challenges this exclusion and often furthers it.

Consider security. Before getting into the tech aspects of security, the
general concept itself is fraught with gender and racial overtones. For
example, there's the aggressive macho approach to security, or the racialized
circle the wagons to defend against the &quot;outsiders.&quot; 

Our impression (and reaction) to security can vary wildly depending on our
experience with oppression. It frequently comes as a surprise to one person
that an idea about security could be interpreted as oppressive.  However, to an
entire group of people who have experienced a pattern of exclusion resembling
that approach to security, it's profoundly racist or sexist.

## Security and the Internet: default closed ##

With regard to Internet, there are an many ways to interpret security. However,
within radical tech culture, most of the approaches involve exclusion. 

Encrypted email, for example, requires that every participant have a special
key, a working and configured program that can use the key, and access to this
working installation for all email communications. If you don't have that, you
can't participate.

Key-based shell access to a server is another example. A common and secure way
to provide access to servers is to use an approach requiring every user to have
a public key - rather than a password. You then need to connect from a computer
that has the private key corresponding to your public key. More secure - yes,
however it has the effect of excluding everyone without a key or the knowledge
to generate one. 

However, the example that is the most striking is the way we treat the concept
of privacy. In radical tech culture, privacy is often the default. We decide to
be private and then, maybe later, decide what we want to make public. Email
list archives become private, email has to be encrypted, wikis have to be
password-protected and the list goes on. 

This approach requires a pre-defined group of people who are in. Nobody gets
access to what this group produces unless the group decide later to make it
public. And - people can only join if someone in the group can vouch for them.

Furthermore, even within this group, the overhead involved in communication is
immense since every channel has to be secured before you can begin.

The result is an exclusive enclave within a network that is internationally
famous for being decentralized and open. A country club on the Internet built
by people who identify with left politics. And a very difficult environment for
including new people.

And when a group like this is predominantly run by white men - or any other
grouping of people with privilege - it effectively creates an environment that
is not only exclusive but throws huge structural obstacles to challenging
racism and sexism by removing itself from a bigger movement for liberation. 

## A "default open" approach to security ##

There is, of course, a need for security when organizing. However, "default
closed" is not the only way to organize and, from both an anti-oppression and
pro-security perspective, is not necessarily the best way.

A different approach is to consider a default open approach, and then figure
out where to make exceptions. In other words:

* Organize most activities in such a way that all newcomers can be safely
treated as part of the group until they demonstrate otherwise.

* Organize most meetings so that they are public and open to all (if it's an
online - you don't need a GPG key or an SSH key to participate - although you
will be encouraged to get one moving forward).

* Publish as much information about what you are doing publicly so new people
can decide whether they agree with what you are doing before deciding to
participate.

This approach probably sounds familiar to veteran organizers, since it's an old
method for movement building. It also may sound familiar to techies used to
Unix - since it's the approach taken with the file system (all files are
readable by everyone, except the small handful that should not be public).

In an organizing context, certain information and certain discussions will need
to be exclusive to a smaller number of people who are trusted with the group -
however, this division and explanation can be public without divulging the
private information. And this distinction should be a healthy debate within the
group.

This approach doesn't guarantee an oppression-free organizing environment.
However, it does provide an environment in which challenging oppression can
take place.

In addition, this approach makes security significantly easier. Rather than
trying to secure everything, we only have to focus on a securing a smaller
subset of information and communications. And, we reduce our risks of divulging
private information due to a technical compromise by operating mostly in the
public. 

## Deciding on an approach to security ##

Ultimately, a group's approach to security will depend on the group's
priorities.  There are organizing campaigns now (and from our history) in which
individuals face serious persecution for having their identities divulged or
activities public. Sometimes that happens when we are working in a regime with
laws we do not respect, other times our politics dictate acting in a way that
makes us unacceptably vulnerable. And there are a many other reasons why
staying under the radar is critical for one's organizing objectives.

How a group of people decides to organize will be a result of the individuals
involved and their collective politics.  Ultimately, we have to consider the
risks involved, the sacrifices we are willing to make, and what our political
priorities are. While the decision will be different for every group, there are
few areas of work more in need of attention to exclusion and oppression than
the Internet. 
