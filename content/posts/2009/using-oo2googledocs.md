---
tags: ["security"]
title: "Google Docs and politically responsible computing"
date: 2009-01-08T09:32:44.185780
---

I just saw an announcement on a politically smart list saying that a
collaborative document was just made available on Google Docs (a web
application provided by Google that allows multiple people to collaboratively
edit a word processing document).

Many people are concerned about privacy in the realm of corporate services like
Google Docs (Google, and anyone with legal access to Google, can read
everything you post). 

While I agree, I'm even more concerned about reliability. I don't believe we
should count on services provided by corporations (or corporations themselves)
being around for the long haul and we certainly can't count on them to
gracefully close. When capitalism is doing well, corporations are purchased,
merged, and re-organized with unpredictable changes in the quality or future of
services. In times like the present, corporations slash services or just go
belly up. And regardless of the economic climate, network access to corporate
services can be turned off in an instant in response to any number of spurious
copyright complaints or politically motivated criminal investigations.

What happens to our library of documents when this happens? Can we count on a
warning that would allow us to retrieve our data?

You could argue that Google is one of the most financially powerful
institutions on the Internet and is therefore highly unlikely to be bought or
go belly up. Yet, do we really want to hitch our political movements to the
financial success of a corporation whose intent is to dominate the Internet?
Do we want to support a practice that will, over time, make us increasingly
interdependent on Google?

Fortunately, there are alternatives. "Alternative" in this sense doesn't mean a
nonprofit or other entity offering the same services as Google Docs (yet
another unreliable data silo), but instead alternative in the real sense of the
word. 

The project is called [oo2gd](http://code.google.com/p/ooo2gd/) (OpenOffice to
Google Docs). It allows you to synchronize your documents from
[OpenOffice](http://openoffice.org), a free office suite, to Google Docs (and
several other network platforms). With oo2gd you can use Google Docs while
always preserving, off line on your own computer, copies of your work.


