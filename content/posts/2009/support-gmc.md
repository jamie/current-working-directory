---
tags: ["mayfirst","gmc"]
title: "Support the an important NYC conference: The Grassroots Media Conference"
date: 2009-01-08T09:32:44.185780
---

One of the best conferences in New York City is looking for fresh volunteers!
Anyone who feels strongly not just about grassroots media but about broad based
progressive organizing should consider helping out. For more information about the project (if you are not already familiar with it) - you can checkout their [website](http://nycgrassrootsmedia.org/).

Here's the call for the volunteers:

	CALL FOR PARTICIPATION: Sixth Annual NYC Grassroots Media Conference!

	Host: NYC Grassroots Media Coalition
	Date:Wednesday, January 14, 2009
	Time:7:00pm - 8:00pm
	Location:NACLA office
	Street:38 Greene Street, 4th floor (corner of Grand Street), SoHo

	Come out and help organize the most important media gathering in NYC! Get to
	know grassroots media makers and social justice organizers from around the
	city while working to change our city's media landscape. Network, learn
	about media, and make friends!

	We're looking for people to join our core organizing team. Starting in
	January, you'll work closely with staff and other organizers to make this
	the most diverse and exciting GMC yet. We particularly need people with
	event organizing, design, and web skills, but we welcome everyone. So come &
	learn about the organizing process and meet the rest of the group.
	The NYC Grassroots Media Conference organizing committee actively seeks
	participation from different ethnic and racial backgrounds, sexual
	orientations, classes, and physical abilities.

	Phone: 8023098146
	Email: jbatten517@gmail.com

