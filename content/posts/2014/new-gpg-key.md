---
title: "New OpenPGP key"
tags: ["debian","security","openpgp"]
date: 2014-01-09T09:09:11.498814
---

After playing around with [How secure is my password](https://howsecureismypassword.net/), I dediced to start changing the handful of passwords I have committed to memory to being very long phrases rather than shorter, concentrated collections of random-seeming characters (another interesting blog post would be to understand the algorythm behind that site but for now I've just taken it at face value).

Despite my usual practice of introducing new passphrases by changing my laptop login (which can always be reset if necessary), I went ahead and changed by GPG key instead to a new 50 character phrase. Fifteen minutes passed before I needed access to the key and... I couldn't repeat the passphrase.

I tried to be clever. I opened a text file and typed in this passphrase over and over again, ultimately generating 49 unique typo-ridden versions of the passphrase.

Then, I wrote a simple script that took each of these versions and generated additional versions by:

 * Changing one character (from a preselected character list of letters, numbers and punctuation within key range of the actual letters in the passphrase).
 * Deleting one character
 * Adding one character
 * Transposing all characters

In the end I had about 350,000 versions of my passphrase. 

None of them worked.

What in the world did I type??

Could I just ask the NSA?  

In any event, my new OpenPGP key fingerprint is: 

    1F9C30CB3CFC5DA9987FA035A014C05A607B7535

Please note: I've changed the "real name" part of my User Id from "Jamie McClelland" to "James McClelland" becuase James is on my government issued id.

I'll be working on collecting new signatures for the rest of my life.

Some interesting information on how this made my life difficult:

 * I use the [Monkeysphere](http://monkeysphere.info/) for accessing the servers I maintain using by OpenPGP key. Fortunately, the key was loaded into my ssh agent, so I have still been able to access the servers. If my computer crashes or gets restarted I'll be locked out until I get my new key in place. I'll need to ask another [May First/People Link](https://mayfirst.org/) administrator to sign a puppet release with my new fingerprint.
 * May First/People Link uses [keyringer](https://keyringer.pw/) to store shared passwords, encrypted to a small collection of administrator OpenPGP keys, including my old one. I'll need to ask another admin to update the list of keys.
 * I have hundreds of (mostly) web-site passwords stored in [assword](http://finestructure.net/assword/) and encrypted to my old key. They are all gone. I'll be doing lots of password resets now.
 * I OpenPGP sign my emails. That has been turned off between the time I lost the passphrase and generated a new key.
 * I am subscribed to a few email lists run via [Schleuder](http://schleuder2.nadir.org/). I can't access those emails now. I'll need to send my new key to the list.
 * I've lost access to every encrypted message that has been sent to my old key.

------

Postscript: Thanks to Jordan Uggla, I was directed to [passe-partout](http://c0decstuff.blogspot.com/2011/01/in-memory-extraction-of-ssl-private.html), a tool that can extract an RSA private key out of memory when ssh-agent is running with the key loaded. Since I am using the [Monkeysphere](http://monkeysphere.info/), my OpenPGP key has an authentication-capable subkey, which is fed to an ssh-agent and used to grant me access to remote computers using ssh. After losing my passphrase, I was not able to access that subkey.  With the help of pass-partout, and the fact that the subkey was loaded into ssh-agent before I lost the passphrase, I was able to extract the private RSA key from my ssh-agent and save it to a file. Then, I could restart my ssh-agent with my ''new'' authentication-capable OpenPGP subkey, thus allowing me to access servers that have granted me access via my new key and the handful of servers that still only grant me access via my old key.
