---
title: "Getting to know systemd"
tags: ["debian","sysadmin"]
date: 2014-08-20T10:17:39.513403
---

Update 2014-08-20: apcid needs tweaking. See update section below.

Somehow both my regular work laptop and home entertainment computers (both running Debian Jessie) were switched to [systemd](https://en.wikipedia.org/wiki/Systemd) without me noticing. Judging from by dpkg.log it may have happened quite a while ago. I'm pretty sure that's a compliment to the backwards compatibility efforts made by the systemd developers and a criticism of me (I should be paying closer attention to what's happening on my own machines!).

In any event, I've started trying to pay more attention now - particularly learning how to take advantage of this new software. I'll try to keep this blog updated as I learn. For now, I have made a few changes and discoveries.

First - I have a convenient bash wrapper I use that both starts my OpenVPN client to a samba server and then mounts the drive. I only connect when I need to and rarely do I properly disconnect (the OpenVPN connection does not start automatically). So, I've written the script to carefully check if my openvpn connection is present and either restart or start depending on the results.

I had something like this:

    if ps -eFH | egrep [o]penvpn; then
      sudo /etc/init.d/openvpn restart
    else
      sudo /etc/init.d/openvpn start
    fi

One of the advertised advantages of systemd is the ability to more accurately detect if a service is running. So, first I changed this to:

    if systemctl -q is-active openvpn.service; then
      sudo systemctl restart openvpn.service
    else
      sudo systemctl start openvpn.service
    fi

However, after reviewing the man page I realized I can shorten if further to simply:

      sudo systemctl restart openvpn.service

According to the man page, restart means:

    Restart one or more units specified on the command line. If the units are not
    running yet, they will be started.

After discovering this meaning for "restart" in systemd, I tested and realized that "restart" works the same way for openvpn using the old sysv style init system. Oh well. At least there's a man page and a stronger guarantee that it will work with *all* services, not just the ones that happen to respect that convention in their init.d scripts.

The next step was to disable openvpn on start up. I confess, I never bothered to really learn update-rc.d. Everytime I read the manpage I ended up throwing up my hands and renaming symlinks by hand. In the case of openvpn I had previously edited /etc/default/openvpn to indicate that "none" of the virtual private networks should be started.

Now, I've returned that file to the default configuration and instead I ran:

    systemctl disable openvpn.service

UPDATES

2014-08-20: I've recently noticed strange behavior when I wake my laptop. Seems to sometimes go right back to sleep. After doing some digging I traced the problem to some customizations I have made to my laptop's acpid behavior combined with systemd taking over some apci events.

Up to now, I have created my own /etc/acpi files so I have full control over the acpi events. In particular, I don't want my laptop to suspend when I close the lid. I only want it to suspend when I press the suspend key. And, when it does suspend, I want it to run my own personal suspend script so I can do things like lock the screen and restart tint2.

I've found that systemd launches it's own acpi event monitoring that ignores my /etc/acpid rules (the systemd "unit" that monitors events is called acpid.socket which exists in addition to acpid.service). The systemd reaction to events is controlled by the systemd-logind.service which has a configuration file: /etc/systemd/logind.conf. By default, systemd-logind.service will put my laptop to sleep when the lid is closed and when the suspend button is pushed. systemd seems to get the signal first, putting the laptop to sleep. After I wake it up, acpid gets the signal - so it goes right back to sleep.

Reading `man logind.conf` helps. I was able to restore my desired behavior by adding these lines to /etc/systemd/logind.conf:

    HandleSuspendKey=ignore
    HandleLidSwitch=ignore

Then: `sudo systemctl restart systemd-logind.service`.
