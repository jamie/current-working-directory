---
title: "Certain Days"
date: 2018-02-21T09:23:28.187530
---

[<img border="0" align="left" hspace="10" vspace="10" src="http://certaindays.org/sites/certaindays.org/files/images/cover2006.jpg" >](http://www.certaindays.org)
For those of you thumbing through the last few pages of your 2005 calendar, wondering if your uncle is going to send you another Sierra Club calendar with chipmunks and pine trees, do yourself a favor and get a copy of the Certain Days 2006 Freedom for Policial Prisoners Calendar. It looks better. And it actually supports groups doing political prisoner work.

[http://www.certaindays.org](http://www.certaindays.org)
