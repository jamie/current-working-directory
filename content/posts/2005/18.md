---
title: "May First Moves into New Cabinet"
date: 2005-01-08T09:32:44.109780
---

Last week we moved all of our servers from the clutches of the Internet Channel (recently aquired by thorn.net) into our very own cabinet located at 25 Broadway in downtown Manhattan.

This move happened after months of being saddled with astronomical and questionably calculated overage charges by the Internet Channel (not to mention ridiculous termination fees and, at the last minute, $300/hour charge - 3 hour minimm - to hire a technician to unplug and hand over our servers when we moved).

What does this mean? It means that May First now has direct control over all of our servers. It means we have a stable-priced cabinet that can hold about 20 more servers, enabling us to easily expand in the future. And it means we have loads more bandwidth for high capacity sites, streaming video and more.
