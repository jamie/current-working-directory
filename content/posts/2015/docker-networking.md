---
title: "Docker networking... private range or not?"
tags: ["debian","docker"]
date: 2015-05-19T12:07:23.532656
---

Am I missing something? 

I installed docker and noticed that it created a virtual interface named docker0 with the IP address 172.17.42.1. This behavior is consistent with the [Docker networking documentation](https://docs.docker.com/articles/networking/). However, I was confused by this statement:

    It randomly chooses an address and subnet from the private range defined by RFC 1918 that are not in use on the host machine, and assigns it to docker0. Docker made the choice 172.17.42.1/16 when I started it a few minutes ago...

It seems like [RFC 1918](https://tools.ietf.org/html/rfc1918) defines:

 * 10.0.0.0        -   10.255.255.255  (10/8 prefix)
 * 172.16.0.0      -   172.31.255.255  (172.16/12 prefix)
 * 192.168.0.0     -   192.168.255.255 (192.168/16 prefix)

How is 172.17.42.1/16 from the private ranges listed above? Is 172.17.42.1 a potentially public IP address? 

