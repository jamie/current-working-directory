---
title: "Agents, the kinds that work for us."
tags: ["debian","security","desktop","openpgp"]
date: 2015-08-03T10:37:36.288725
---

I recently decided I wanted to adjust how long my gpg password cache lasts and realized I had no idea how either my ssh-agent or gpg-agent was being launched or even which programs were being used as the agent for which protocol. While I appreciate that everthing just works based on blind faith, I decided to investigate more closely.

I started by reading the man pages for ssh-agent and gpg-agent and discovered that gpg-agent could be used for providing ssh-agent. 

Since I run openbox, I decided to dig through my ~/.config/openbox/autostart file and discovered that I was also launching gnome-keyring-daemon, which provides both an ssh-agent and a gpg-agent. This is getting confusing.

I then examined the output of `ps -eFH` and found this: 

    jamie    28499 28489  0  1082  1516   1 10:06 tty1     00:00:00     /bin/sh /usr/bin/startx
    jamie    28542 28499  0  3987  1912   1 10:06 tty1     00:00:00       xinit /etc/X11/xinit/xinitrc -- /etc/X11/xinit/xserverrc :0 vt1 -auth /tmp/serverauth.gn3C5pYbPc
    root     28543 28542  0 58065 25036   3 10:06 tty1     00:00:05         /usr/bin/X -nolisten tcp :0 vt1 -auth /tmp/serverauth.gn3C5pYbPc
    jamie    28548 28542  0 69563 19980   0 10:06 tty1     00:00:00         /usr/bin/openbox --startup /usr/lib/x86_64-linux-gnu/openbox-autostart OPENBOX
    jamie    28604 28548  0  2774   348   1 10:06 ?        00:00:00           /usr/bin/ssh-agent /usr/bin/gpg-agent --daemon --sh --write-env-file=/home/jamie/.gnupg/gpg-agent-info-animal /usr/bin/dbus-launch --exit-with-session /usr/bin/monkeysphere-validation-agent /usr/bin/im-launch /home/jamie/.xsession
    jamie    28605 28548  0  4915   248   0 10:06 ?        00:00:00           /usr/bin/gpg-agent --daemon --sh --write-env-file=/home/jamie/.gnupg/gpg-agent-info-animal /usr/bin/dbus-launch --exit-with-session /usr/bin/monkeysphere-validation-agent /usr/bin/im-launch /home/jamie/.xsession
    jamie    28610 28548  0 22402 34456   0 10:06 tty1     00:00:00           /usr/bin/perl -wT /usr/bin/monkeysphere-validation-agent /usr/bin/im-launch /home/jamie/.xsession

Further down (and not nested under the xinit process), I found my gnome-keyring-daemon process.

I started by removing my own call to gnome-keyring-daemon and realized that everything still worked fine (both gpg and ssh). I must have added that line before things were configured system-wide to run automatically. 

Then, I saw that the gpg-agent was being launched without the `--enable-ssh-support` option (and ssh-agent was running), so looks like ssh is handled by ssh-agent and gpg is handled by gpg-agent. So far so good.

Now, how do they get launched??

I start my graphical session by logging in at a console and running `exec startx`. In my home directory, I have ~/.xsession which contains the line `exec /usr/bin/openbox-session`. Therefore, I started by scouring all things openbox on my system and couldn't find any reference to these agents. 

Next, I turned my attention to X.  Based on my `ps -eFH` output, I started with /etc/X11/xinit/xinitrc, which led me to /etc/X11/Xsession which led me to /etc/X11/Xsession.d and presto, I found my answer: a series of shell scripts that modify a global variable STARTUP which is executed by X.

Thanks Debian developers for making it all work out of the box!
