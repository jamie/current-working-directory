---
title: "So long email, it's been good to know yuh"
tags: ["debian","email","desktop"]
date: 2015-04-30T10:00:52.616553
---

Yesterday I permanently deleted 15 years of email.

It wasn't because I didn't have enough hard disk space to store it. It's because I decided, after 15 years, that the benefits of keeping all this email did not outweigh the risks. Although I have never had my email subpoenaed, I have had many [legal interactions](https://support.mayfirst.org/wiki/legal) due to my involvement with [May First/People Link](https://mayfirst.org), some of which were about finding the real identities of May First/People Link members. I'd rather not risk compromising anyone or needlessly exposing my networks. Now I have an Inbox, Sent Box, Trash Box and Detected Spam Box. The Inbox I empty manually and the other boxes are automatically purged on a scheduled basis.

In this age of surveillance it's sad to see data evaluated based on risk of exposure.



