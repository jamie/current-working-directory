---
title: "Deleting an app won't bring back Roe v Wade"
date: 2022-06-25T18:27:10-04:00
tags: [ "debian", "oppression", "another-internet" ]
---

In some ways it feels like 2016 all over again. 

I'm seeing panic-stricken calls for everyone to delete their period apps, close
their Facebook accounts, de-Google their cell phones and, generally speaking,
turn their entire online lives upside down to avoid the techno-surveillance
dragnet unleashed by the overturning of Roe v. Wade.

I'm sympathetic and generally agree that many of us should do most of those
things on any given day. But, there is a serious problem with this cycle of
repression and panic: it's very bad for organizing.

**In our rush to give people concrete steps they can take to feel safer, we're
fueling a frenzy of panic and fear, which seriously inhibits activism.**

Now is the time to remind people that, over the last 20 years, *a growing
movement of organizers and technologists have been building user-driven,
privacy-respecting, consentful technology platforms as well as organizations
and communities to develop them.*

We have an entire eco system of:

 * [technology platform cooperatives](https://platform.coop/), 
 * movement aligned Internet providers that pre-date the founding of Twitter
   and are still going strong ([May First](https://mayfirst.coop),
   [Riseup](https://riseup.net), and [Autistici](https://autistici.org/) just
   to name a few), 
 * the [fediverse](https://fediverse.party/en/fediverse/), a well developed,
   de-centralized alterntiave to corporate social media (try
   [Social.coop](https://wiki.social.coop/home.html) if you want to get started),
 * powerful open source, privacy respecting software geared for organizing and movement providers
   hosting it (see [CiviCRM](https://civicrm.org/) and [Progressive Technology
   Project](https://progressivetech.org)), 
 * multi-year campaigns targeting poor tech practices of corporate technology
   giants (see [Mijente's](https://mijente.org) [No Tech for
   ICE](https://notechforice.com/)),
 * so many more examples, far too numerous to name.

All of these projects need our love and support over the long haul.  Please
help spread the word - rather then just deleting an app, let's encourage people
to join an organziation or try out a new kind of technology that will serve us
down the road when we may need it even more then today.
