---
title: "A very liberal spam assassin rule"
date: 2022-06-20T08:27:10-04:00
tags: [ "debian", "sysadmin", "email" ]
---

I just sent myself a test message via [Powerbase](https://ourpowerbase.net/) (a
hosted [CiviCRM](https://civicrm.org) project for community organizers) and it
didn't arrive. Wait, nope, there it is in my junk folder with a spam score of
6!

```
X-Spam-Status: Yes, score=6.093 tagged_above=-999 required=5
	tests=[BAYES_00=-1.9, DKIM_SIGNED=0.1, DKIM_VALID=-0.1,
	DKIM_VALID_AU=-0.1, DKIM_VALID_EF=-0.1, DMARC_MISSING=0.1,
	HTML_MESSAGE=0.001, KAM_WEBINAR=3.5, KAM_WEBINAR2=3.5,
	NO_DNS_FOR_FROM=0.001, SPF_HELO_NONE=0.001, ST_KGM_DEALS_SUB_11=1.1,
	T_SCC_BODY_TEXT_LINE=-0.01] autolearn=no autolearn_force=no
```

What just happened?

A careful look at the scores suggest that the `KAM_WEBINAR` and `KAM_WEBINAR2`
rules killed me. I've never heard of them (this email came through a system I'm
not administering). So, I did some searching and [found a page with the
rules](https://github.com/NethServer/nethserver-mail-filter/blob/master/root/etc/mail/spamassassin/KAM.cf):

```
# SEMINARS AND WORKSHOPS SPAM
header   __KAM_WEBINAR1 From =~ /education|career|manage|learning|webinar|project|efolder/i
header   __KAM_WEBINAR2 Subject =~ /last chance|increase productivity|workplace morale|payroll dept|trauma.training|case.study|issues|follow.up|service.desk|vip.(lunch|breakfast)|manage.your|private.business|professional.checklist|customers.safer|great.timesaver|prep.course|crash.course|hunger.to.learn|(keys|tips).(to|for).smarter/i
header   __KAM_WEBINAR3 Subject =~ /webinar|strateg|seminar|owners.meeting|webcast|our.\d.new|sales.video/i
body     __KAM_WEBINAR4 /executive.education|contactid|register now|\d+.minute webinar|management.position|supervising.skills|discover.tips|register.early|take.control|marketing.capabilit|drive.more.sales|leveraging.cloud|solution.provider|have.a.handle|plan.to.divest|being.informed|upcoming.webinar|spearfishing.email|increase.revenue|industry.podcast|\d+.in.depth.tips|early.bird.offer|pmp.certified|lunch.briefing/i

meta     KAM_WEBINAR (__KAM_WEBINAR1 + __KAM_WEBINAR2 + __KAM_WEBINAR3 + __KAM_WEBINAR4 >= 3)
describe KAM_WEBINAR Spam for webinars
score    KAM_WEBINAR 3.5

meta     KAM_WEBINAR2 (__KAM_WEBINAR1 + __KAM_WEBINAR2 + __KAM_WEBINAR3 + __KAM_WEBINAR4 >= 4)
describe KAM_WEBINAR2 Spam for webinars
score    KAM_WEBINAR2 3.5
```

For those of you who don't care to parse those regular expressions, here's a summary:

 * There are four tests. If you fail 3 or more, you get 3.5 points, if you fail
   4 you get another 3.5 points (my email failed all 4).
 * Here is how I failed them:
   * The *from address* can't have a bunch of words, including "*project.*" My from address includes my organization's name: The Progressive Technology Project.
   * The *subject line* cannot include a number of strings, including "*last chance*." My subject line was "Last change to register for our webinar."
   * The *subject line* cannot include a number of other strings, including "*webinar*" (and also webcast and even strategy). My subject line was "Last chance to register for our webinar."
   * The *body* of the message cannot include a bunch of strings, including "*register now.*" Well, you won't be suprised to know that my email contained the string "Register now."

Hm. I'm glad I can now fix our email, but this doesn't work so well for people
with a name that includes "project" that like to organize webinars for which you
have to register.
