---
title: "Fine tuning Thunderbird's end-to-end encryption"
date: 2022-08-04T18:27:10-04:00
tags: [ "debian", "email", "openpgp" ]
---

I love that Thunderbird really tackled OpenPGP head on and incorporated it
directly into the client. I know it's been a bit rough for some users, but I
think it's a good long term investment.

And to demonstrate I'll now complain about a minor issue :).

I replied to an encrypted message but couldn't send the response using
encryption. I got an error message indicating that "End-to-end encryption
requires resolving certificate issues for" and it listed the recipient
email address.

![Screen shot of error message saying: End-to-end encryption requires resolving certificate issues for](posts/2022/thunderbird-and-openpgp/resolving-certificate-issues.png)

I spent an enormous amount of time examining the recipient's OpenPGP key. I
made sure it was not expired. I made sure it was actually in my Thunderbird key
store not just in my OpenPGP keychain. I made sure I had indicated that I trust it
enough to use. I re-downloaded it.

I eventually gave up and didn't send the email. Then I responded to another
encrypted email and it worked. What!?!?

I spent more time comparing the recipients before I realized the problem was
the sending address, not the recipient address.

I have an OpenPGP key that lists several identities. I have a Thunderbird
Account that uses the Identities feature to add several from addresses. And, it
turns out that in Thunderbird, you need to indicate which OpenPGP key to use
for your main account... but also for each identity. When you drill down to
Manage Identities for your account, you are able to indicate which OpenPGP key
you want to use for each identity. Once I indicated that each identity should
use my OpenPGP key, the issue was resolved.

And here's my [Thunderbird bug asking for an error message pointing to the
sender address, not the recipient
address](https://bugzilla.mozilla.org/show_bug.cgi?id=1783424).

