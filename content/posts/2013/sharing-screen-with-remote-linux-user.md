---
title: "Sharing screen with remote Linux user behind a firewall"
tags: ["debian","sysadmin","desktop"]
date: 2013-01-08T09:32:44.189780
---

Intentionally helping someone poke a hole through their firewall to allow any users on a remote machine access to their computer is generally a bad idea... unless they want you to help them with their computer.

In that case, I find it really useful. 

Toward this end, I've setup a dedicated user account (jamie-share@chavez.mayfirst.org in this example). I granted myself shell access to this user and generated an ssh key pair:

    ssh-keygen -t rsa

Then, as a convenience I move my public key into the home directory:

    cp ~/.ssh/id_rsa.pub ~/

Next, I give the user I want to help ssh access to this shell account on an Internet connected computer (in this example: jamie-share@chavez.mayfirst.org). Either I share the password with them or if they have an ssh key or monkeysphere identity I use that.

Then, I ask them to grant teh jamie-share user access to their user account by running (which downloads the key I just created):

    mkdir -p ~/.ssh
    ssh jamie-share@chavez.mayfirst.org "cat id_rsa.pub" >> ~/.ssh/authorized_keys

Next, ask them to install openssh-server on their local computer:

    sudo apt-get install openssh-server screen

Next, they run the following command on their local computer:

    ssh -R 2222:localhost:22 jamie-share@chavez.mayfirst.org

This command says: forward port 2222 on chavez.mayfirst.org to port 22 on your local computer.

Lastly, I log in to jamie-share@chavez.mayfirst.org and run, replacing <their-username> with their local username. 

    ssh <their-username>localhost -p 2222

You can do whatever you want now, but running screen is a good way to share a session so the person you are working with can see what you are doing as you do it.

p.s. Thanks Ross for the tips!
