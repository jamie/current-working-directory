---
title: "Administering CUPS from the command line"
tags: ["debian","sysadmin","desktop"]
date: 2013-01-08T09:32:44.189780
---

I usually try to avoid administering printers whenever possible. As a result I end of flailing around the CUPS web interface before I figure out how to re-enable a printer. And, when I get a call to help debug a printer, I can't easily tell people what to do.

When I try to do what I need via the command line, I end up spending at least 10 or 15 minutes re-reading man pages before I piece together the steps.

Here's my attempt to document the steps so I don't have to re-read man pages.

## Setup ##


The cups commands in these examples can be run as a non-root user if that user is in the lpadmin group.

Type:

    groups

To see if lpadmin is listed. If not:

    sudo adduser <your-user-name> lpadmin

Then, to gain access to the new group without logging out and logging in again:

    newgrp lpadmin

## Finding printers.

In these examples, the printer name in question is: `stability` and it is a network printer, with local DNS that properly resolves the hostname stability to an IP address.

You can list all detected printers with: `lpstat -v`

Note that the resolved IP address is listed if it's a network/wifi connected printer.

## Network access ##

First, try to ping the printer:

    ping stability

If this fails, restart the printer and/or check network cables. No point in doing anything else until it responds to pings.

## Can't submit new jobs to the printer ##

Next, if the problem is that the printer is greyed out when you try to print a document or your application tells you that the printer is rejecting jobs, confirm this status with:

    lpstat -a stability

It will either output:

    stability accepting requests since Mon 20 May 2013 10:28:57 AM EDT

Or

    stability not accepting requests since Mon 20 May 2013 10:28:57 AM EDT -
      Rejecting Jobs

If it is rejecting jobs, try:

    /usr/sbin/cupsaccept stability

## Accepts new jobs, but just doesn't print ##

On the other hand, if the printer is accepting jobs, but the jobs are not printing, find out if the printer is enabled with:

    lpstat -p stability

You should get either:

    printer stability is idle.  enabled since Mon 20 May 2013 10:28:57 AM EDT

Or:

    printer stability disabled since Mon 20 May 2013 10:35:10 AM EDT -
      Paused

If it is disabled, you should first see what queued jobs there are:

    sudo rlpq -P <printer> -H <printer>

(If this fails, try replacing the `-H` option with the IP of the printer - remember, run `lpstat -v` to see the IP address of your printer.)

If you have a list of duplicate pending jobs, be sure to delete the duplicates to avoid having your print job come out multiple times.

To delete a queued job, type the following (n should be the number in the Job column of the lpq output):

    cancel <n>

After you have deleted duplicate jobs, try "enabling" it:

    /usr/sbin/cupsenable stability

Then, re-rerun the lpq command and see if it's now "ready." At this point, the jobs should start printing.

## Submit a job via the command line

    lp -h <printer-host> /path/to/file
    lp -d <printer-name> /path/to/file

If a pdf doesn't print, try "fixing" the PDF with:

    mutool clean input.pdf output.pdf

## Review of concepts ##

For review... a few important concepts:

  * cupsaccept/cupsreject: controls whether a printer will accept or reject *new* jobs. It doesn't matter whether the printer is enabled or disabled.
  * cupsenable/cupsdisable: controls whether a printer will print existing jobs. It doesn't matter whether the print is accepting or rejecting new jobs.
