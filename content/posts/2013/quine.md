---
title: "If you don't know what a quine is, consider yourself lucky"
tags: ["debian","programming"]
date: 2013-01-08T09:32:44.189780
---

rhatto just asked me to approve a change in [keygringer's](https://keyringer.sarava.org/) license from AGPLv3+ to GPLv3+ citing a [discussion about a Berkeley DB's switch to AGPLv3](https://lists.debian.org/debian-devel/2013/07/msg00031.html). For some reason, a reference to a [quine](https://lists.debian.org/debian-devel/2013/07/msg00057.html) caught my eye.

If you don't know what a quine is, I suggest you remain ignorant. I won't even provide the Wikipedia link. 

A quick web search suggests that it's quite possible to write one in bash. I hope to be productive again some day. 
