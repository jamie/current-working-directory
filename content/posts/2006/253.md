---
title: "May First/People Blog blogged published in Social Policy Magazine"
date: 2006-01-08T09:32:44.121780
---

Alfredo Lopez's blog on Spam and Censorship was published in the current issue of [Social Policy Magazine](http://socialpolicy.org/index.php?id=1122). We're excited to see the debate about the serious political implications of controlling spam reach an activist audience, rather than remaining soley an issue for techies.

To read the original blog and see more information about the May First/People Link Spam campaign, please see our <a href="spamcampaign">Spam Campaign Page</a>. Please check it out and then post a comment to let us know what you think.
