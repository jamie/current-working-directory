---
title: "Yahoo to Internet: Drop Dead"
date: 2006-01-08T09:32:44.121780
---

When you're a giant on the Internet, like Yahoo, you really can make a difference. You can afford to show leadership, develop new ideas, and build and instracture that improves the Internet for everyone.

Or, you tell the world to drop dead.

That's effectively what Yahoo starting doing some time last October. The web-o-sphere is full of complaints from people trying to send email to Yahoo that Yahoo refuses to accept, saying that it is being deferred (for the geeks: 421 Message from (209.51.172.11) temporarily deferred - 4.16.50). It continues to defer these messages, randomly letting them through according to the phases of the moon, the velocity of butterflies flapping their wings over the Sea of Japan or God knows what else.

In any event, we're devoting yet more scarce resources ensuring that mail sent via our mail servers get delivered to Yahoo. The best we can hope for is that Yahoo's backward ideas on how to ensure all mail gets delivered to their users will generate interesting conversations about how to choose an organization to entrust with your email.

For an interesting thread on the topic, check out [AHFX's blog](http://www.ahfx.net/weblog.php?article=107).

