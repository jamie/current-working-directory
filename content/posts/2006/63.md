---
title: "Spam is out of control!"
date: 2006-01-08T09:32:44.121780
---

Especially the last few weeks we've seen an signficant rise in spam on the May First/People Link servers. We've started taking more aggressive evasive action, so you should see more spam identified with the ****SPAM**** subject line or rejected all together over the next few weeks. Thanks for your patience!

Keep in mind tips you can use to reduce spam. A good article on the subject from Tech Soup is [available here](http://www.techsoup.org/howto/articles/internet/page1586.cfm).