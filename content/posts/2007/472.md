---
title: "Macintosh + Firefox  + WMV + AutoStart false = AutoStart true"
date: 2007-01-08T09:32:44.133780
---

Coding for multiple browsers and operating systems is always painful. However, adding wmv to the mix makes it even worse.

After loads of testing, we managed to get this code to work on Firefox for Windows, IE6 and IE7 for Windows, and Firefox/Iceweasel on Linux.

Now, we've discovered that with Firefox (and Safari) on a Mac, the AutoStart=false is ignored - the video starts playing automatically.


	&lt;object id="MediaPlayer"
	classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
	standby="Loading Microsoft Windows Media Player components..."
	type="application/x-oleobject"&gt;
	&lt;param name="FileName" value =
	"http://www.afterdowningstreet.org/downloads/norman.wmv"&gt;
	&lt;param name="AutoStart" value="0"&gt;
	&lt;param name="ShowControls" value="true"&gt;
	&lt;param name="ShowStatusBar" value="true"&gt;
	&lt;embed type="application/x-mplayer2" Name="MediaPlayer"
	src="http://www.afterdowningstreet.org/downloads/norman.wmv"
	AutoStart="false" autoplay="false" ShowStatusBar="1"
	ShowControls="1" volume="-1"&gt;&lt;/embed&gt;
	&lt;/object&gt;


Looking at this <a href="http://www.jakeludington.com/windows_media/20061113_embed_a_mac_compatible_wmv.html
">post</a> I get the impression that autostart is not supported on a mac.