---
title: "HTML Email"
date: 2007-01-08T09:32:44.125780
---

I've never been a big fan of HTML email, however, we have received so many requests about how to do it that I've been re-considering. There's a good site [here](http://www.birdhouse.org/etc/evilmail.html) that includes those reasons (it also includes a note from the author about how he is beginning to change his mind). If you are considering sending html email, please read that page first!

I asked my radical techie friends what they though and got a variety of answers, mostly relating to how to do it if you're going to do it. Here's a brief summary.


 * Keep it simple. Don't add fancy tables and such.
 * Test in multiple browsers. Check out this [page](http://www.thinkvitamin.com/features/design/html-emails) to get a sense of what can go wrong.
 * [Thunderbird](http://www.mozilla.com/en-US/thunderbird/) is the most recommended email program for creating standards compliant email. Another suggestion was to compose the email in [Nvu](http://www.nvu.com/).
 * Don't expect people to see your images!! Be sure to include Alt text and use designs that won't fall apart if the images are not viewable. Also, images as headers are not a good idea for these same reasons.
 * People spend on average 50 seconds reading organizational email
 * Sending images embedded into the email will make them more likely to be viewed (as opposed to sending links to the images). However, it will bloat your email.



