---
title: "People for Internet Responsibility"
date: 2007-01-08T09:32:44.137780
---

Thanks Michael for the tip on [People for Internet Responsibility](http://www.pfir.org).

They have started an interesting <a href="http://forums.pfir.org/>set of forums</a> where issues like the definition of spam are being debated. Definitely worth the read.
