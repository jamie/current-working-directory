---
title: "And now munson gets Etch"
date: 2007-01-08T09:32:44.125780
---

Learning from my peewee experience...

After editing /etc/apt/sources.list I started with:

<code>
mayfirst@munson:~$ sudo apt-get install initrd-tools
</code>

Which gave me:


	mayfirst@munson:~$ sudo apt-get install initrd-tools
	Reading Package Lists... Done
	Building Dependency Tree... Done
	The following extra packages will be installed:
	libc6 libc6-dev libdevmapper1.02 libselinux1 libsepol1 locales tzdata
	Suggested packages:
	glibc-doc
	The following packages will be REMOVED:
	base-config
	The following NEW packages will be installed:
	libdevmapper1.02 libselinux1 libsepol1 tzdata
	The following packages will be upgraded:
	initrd-tools libc6 libc6-dev locales
	4 upgraded, 4 newly installed, 1 to remove and 194 not upgraded.
	Need to get 12.0MB of archives.
	After unpacking 1810kB of additional disk space will be used.
	Do you want to continue? [Y/n]


So far so good..

Then I tried:

<code>
apt-get install --purge linux-image-2.6-686
</code>

The purge part is because I want to purge hotplug. Which produced:


	mayfirst@munson:~$ sudo apt-get install linux-image-2.6-686
	Password:
	Reading Package Lists... Done
	Building Dependency Tree... Done
	The following extra packages will be installed:
	busybox initramfs-tools klibc-utils libklibc libvolume-id0
	linux-image-2.6.18-4-686 lsb-base makedev module-init-tools udev
	Suggested packages:
	linux-doc-2.6.18
	Recommended packages:
	libc6-i686
	The following packages will be REMOVED:
	hotplug*
	The following NEW packages will be installed:
	busybox initramfs-tools klibc-utils libklibc libvolume-id0
	linux-image-2.6-686 linux-image-2.6.18-4-686 lsb-base udev
	The following packages will be upgraded:
	makedev module-init-tools
	2 upgraded, 9 newly installed, 1 to remove and 191 not upgraded.
	Need to get 17.3MB of archives.
	After unpacking 52.0MB of additional disk space will be used.
	Do you want to continue? [Y/n]


Next comes the reboot

And then:

<code>
apt-get dist-upgrade
</code>

And away we go.