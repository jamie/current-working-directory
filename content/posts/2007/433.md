---
title: "Failure to mount root partition after major lenny dist-upgrade"
date: 2007-01-08T09:32:44.125780
---

I've been very busy the last two months working on the [US Social Forum](https://www.ussf2007.org). As a result - I've neglected liberace, my laptop. I went a full two months without even running apt-get update. Since liberace is running Lenny, the testing version of Debian, when I finally updating and then ran apt-get dist-upgrade, I had over 300 packages to upgrade, including a kernel upgrade.

I should know better, but in the interest of time I simply pulled them all in together.

When I rebooted, I got an error message:


	/bin/sh: can't access tty; job control turned off


Looking up the screen, I discovered the error message that was causing the problem:


	mount: Mounting /dev/hda6 on /root failed: No such device


I was fortunately dropped into an Ash shell, giving me some tools to play with.

I tried:

<code>
cat /proc/partitions
</code>

And saw all my normal partitions, including /dev/hda6, which is my root partition.

I then tried:

<code>
mount /dev/hda6 /root
</code>

And I received the message:


	mount: Mounting /dev/hda6 on /root failed: Invalid argument


For kicks I tried:

<code>
mount -t ext3 /dev/hda6 /root
</code>

And presto it worked. I then manually ran:

<code>
mount /sys /root/sys
mount /proc /root/proc
</code>

Since those two commands failed before I was dropped into the ash shell.

Then, I typed:

<code>
exit
</code>

And liberace booted up without a hitch.

In an effort to trouble shoot, I re-booted liberace (and got the same  results). I also tried using previous kernels - also with the same results (so it doesn't seem to be related to the kernel upgrade).

At one point, after getting dropped into the Ash shell, I tried:

<code>
mount -t ext3 /dev/hda6 /root
umount /root
mount /dev/hda6 /root
</code>

And that worked.

Ash doesn't have fsck, so I re-booted into [Recovery Is Possible](http://www.tux.org/pub/people/kent-robotti/looplinux/rip/) and ran fsck on /dev/hda6. No problems.

So... I can boot if I run those manually commands. However, I have no idea why the kernel can't mount the root partition.