---
title: "Get lucky on Friday the 13th with the Rude Mechanical Orchestra!"
date: 2007-01-08T09:32:44.125780
---

The RMO contributes to the left on a near weekly basis, playing benefits, demonstrations and rallies: now we need to support them! Friday, July 13th,  2007: Caliper Studios, 67 Metropolitan Avenue, 2nd floor (between Kent and Wythe) Williamsburg, Brooklyn

-----------------------

Help us on our mission to cast away the bad luck of the past 8 years!!

*Friday, July 13th, 2007

*2 sets by the Rude Mechanical Orchestra, at 8 and 11pm.
*More music by Phil Not Bombs, Bovine Homecoming, and other RMO friends.
*Music for dancing by two great DJs
*Our debut CDs for sale, and other RMO goodies and surprises!
*$0-20 sliding scale, $20 gets you a free CD

*Caliper Studios, 67 Metropolitan Avenue, 2nd floor (between Kent and
Wythe) Williamsburg, Brooklyn


The Rude Mechanical Orchestra is proud to announce the release of their debut CD, featuring studio recordings of radical marching band tunes and a live performance video. This limited edition enhanced CD was created to spread the love and noise, and to raise funds for our travel goals for 2008.  Established in 2004 for the March for Women's Lives and to protest the Republican National Convention in NYC, we hope to continue using our music and energy to support local struggles and also travel to events like the 2008 RNC (St. Paul, MN), swing-state voter registration, and brass band/movement convergences in the states and internationally.

We've loved supporting you over the years, and now we'd love your support!

SEE YOU IN THE STREETS!

love & noise,
the Rude Mechanical Orchestra

For the cool flyer, check out:
[http://rudemechanicalorchestra.org/benefitflyerweb.jpg](http://rudemechanicalorchestra.org/benefitflyerweb.jpg)