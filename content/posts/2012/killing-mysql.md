---
title: "Killing MySQL"
tags: ["debian","sysadmin","mysql"]
date: 2012-01-08T09:32:44.189780
---

MySQL is supposed to allow one + the max_connections from a super user so you can stop/restartmysql even when it is reporting too many connections. Unfortunately, for reasons I'm not entirely sure of, sometimes that connection seems to be in use.

I've encountered this problem with web apps and now follow these steps:

 * Shutdown apache to prevent new connections
 * Search for and kill any backup processes (this can cause table locking which can be
   the cause of the problem)
 * Try a clean shutdown again
 * search for and kill mysqld_safe safe process on host. This step should kill one
   connection allowing you to cleanly stop mysql with:

  /etc/init.d//mysql stop

