---
title: "Posting to identi.ca via the command line"
tags: ["debian","social","desktop"]
date: 2012-01-08T09:32:44.189780
---

I set aside 15 minutes to find a tool that would allow me to easily post to my [identi.ca account](https://identi.ca/jamiemcclelland) via the command line. I'm two hours in and I finally sent my first remote post. Hopefully nobody else will have to spend this much time!

There are a lot of tools to help you interact with Twitter via the command line and identi.ca supports the twitter API, however, convincing these tools to use identi.ca was harder than I expected and is woefully under-documented. This hardship is largely self-inflicted since I chose to authenticate via oauth, however, it's not clear to me if identi.ca supports basic auth and if so for how much longer. In any event, oauth seems like a much preferable authetication approach because I don't have to store my identi.ca password in plain text.

I decided to use [tweepy](http://packages.debian.org/search?keywords=tweepy) since it's packaged for Debian and it's written in python. 

If you are not familiar with [OAuth](https://en.wikipedia.org/wiki/Oauth), it's worth reading up on. In short:

* register your application via identi.ca web interface
* retrieve consumer token and consumer secret from identi.ca (via the web)
* using this token and secret, request from identi.ca an application key and secret (you must provide your identi.ca user login credentials before you get these strings)
* and now, configure your application to use the applicaiton key and secret everytime it connects

tweepy has [documentation on using setting up oauth](http://packages.python.org/tweepy/html/auth_tutorial.html#oauth-authentication), however, I found it hard to follow, especially since it goes back and forth between using it for a web app and using it for a desktop app. Since I'm interested in a desktop app (shell), the web app business was just clutter.

I wrote the script below to initialize my app and retrieve the application key and secret. If you are writing a desktop app, this needs to be run once for each user of the app:

    #!/usr/bin/python

    """ 
    This script should be used once to initialize your desktop/command line
    application with a oauth access key and access secret. 

    Your first step is to login, via the web, to your identi.ca account
    and authorize your application. Click Edit next to your username
    to edit your profile settings. Then click Connections on the left
    side bar. Then, in the right sidebar, click 
    "Register an OAuth client application".

    When you are done, you will see a page listing your token and secret.
    Fill in the consumer_token and consumer_secret variables below with those
    values.
    """
    """ Fill in these values with values provided in the identi.ca web app when you register your app! """
    consumer_token = ""
    consumer_secret = ""

    import tweepy

    host = 'identi.ca'
    api_root = '/api/'
    oauth_root = api_root + 'oauth/'

    auth = tweepy.OAuthHandler(consumer_token, consumer_secret, 'oob')

    auth.OAUTH_HOST = host
    auth.OAUTH_ROOT = oauth_root
    auth.secure = True

    try:
      redirect_url = auth.get_authorization_url()
    except tweepy.TweepError:
      print 'Error! Failed to get request token.'
      quit()

    req_key = auth.request_token.key
    req_secret = auth.request_token.secret

    print "you don't need these values... just fyi..."
    print "auth request key is: " + req_key
    print "auth request secret is: " + req_secret

    print "Go to this URL for verify code: " + redirect_url

    print "enter the verify code from the URL above"
    verifier = raw_input('Verify code: ')

    auth = tweepy.OAuthHandler(consumer_token, consumer_secret, 'oob')
    auth.set_request_token(req_key, req_secret)

    auth.OAUTH_HOST = host
    auth.OAUTH_ROOT = oauth_root
    auth.secure = True

    try:
      auth.get_access_token(verifier)
    except tweepy.TweepError:
      print 'Error! Failed to get access token.'

    print "Store these values in your application. You will re-use them"
    print "auth access key is: " + auth.access_token.key 
    print "auth access secret is: " + auth.access_token.secret 
    print "done with initialization"

Once you have your key and secret, the following simpler application will post for you:

    #!/usr/bin/python
    import tweepy

    post = "I microblog from bash"
    consumer_token = ""
    consumer_secret = ""
    access_key = ""
    access_secret = ""

    host = 'identi.ca'
    api_root = '/api/'
    oauth_root = api_root + 'oauth/'

    auth = tweepy.OAuthHandler(consumer_token, consumer_secret, 'oob')
    auth.OAUTH_HOST = host 
    auth.OAUTH_ROOT = oauth_root 
    auth.secure = True 
    auth.set_access_token(access_key, access_secret)

    api = tweepy.API(auth, host = host, api_root = api_root)
    api.update_status(post)

