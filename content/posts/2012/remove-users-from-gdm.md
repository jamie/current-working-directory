---
title: "Removing the user list from gdm"
tags: ["debian","sysadmin","desktop"]
date: 2012-01-08T09:32:44.189780
---

UPDATE

Nothing lasts for ever. I just restarted gdm3 (July 29, 2012) and my list of users again popped up.

None of the tips in my original blog or the ones suggested in the comments made any difference.

With help from [dkg](http://www.debian-administration.org/users/dkg)'s grepping ... we found in /etc/gdm3/greeter.gsettings:

    #disable-user-list=true

If you don't have that setting, you can add it under the [org.gnome.login-screen] section. If you do have it, just un-comment it.

No need to dpkg-reconfigure, just restart gdm3.

Also, don't be fooled by IncludeAll or Include in /etc/gdm3/daemon.conf. Those settings don't seem to have any affect.

INFO BELOW IS OUTDATED

I don't want to advertise the list of available logins when gdm starts and presents a login screen on my laptop.

Seems harder to figure out how to do this than it should...

Thanks to [a helpful blog](http://www.hackido.com/2010/03/quick-tip-configure-gdm-to-hide.html) I've figured out two ways to accomplish the task:

    sudo gconftool-2 --direct --config-source xml:readwrite:/etc/gconf/gconf.xml.mandatory --type Boolean --set /apps/gdm/simple-greeter/disable_user_list True

or edit:

/etc/gconf/gconf.xml.mandatory/%gconf-tree.xml

And add:

    <?xml version="1.0"?>
    <gconf>
      <dir name="apps">
        <dir name="gdm">
          <dir name="simple-greeter">
            <entry name="disable_user_list" mtime="1338752251" type="bool" value="true"/>
          </dir>
        </dir>
      </dir>
    </gconf>
