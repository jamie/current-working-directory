---
title: "Property is Theft"
tags: ["debian","coops"]
date: 2012-01-08T09:32:44.189780
---

I've recently been making my way through the Proudhon Reader, a collection of writings by Pierre-Joseph Proudhon recently published by AK Press. Proudhon, known as the father of Anarchism is most famous for his declaration: "Property is theft."

The simple declaration is based on pages of explanation which, simplistically, can be boiled down to the idea that workers should all equally share in the fruits of their labor. While Proudhon's influence on Marx is hotly debated, this idea is fundamental to Marx's idea around the alienation of labor. If we are working to produce a product that is then owned by the proprietor, rather than the worker, we are alienated from our labor.

Many May First/People Link members, such as [Palante Tech](http://palantetech.org), [Openflows](http://openflows.com), [Union Web Services](http://unionweb.org), and [Agaric Design](http://agaricdesign.com/) are all committed to fighting this alienation by organizing as worker-run cooperatives. Nobody is the boss, all workers share in all profits.

However, Proudhon got me thinking about free software shops that aren't worker-run cooperatives. If you are employed by a for-profit corporation as a worker and your only job is to write code that is released under a free software license, are you alienated from your labor in the Marxist sense? The fruits of your labor might be **owned** by your company, but they are freely licensed to the world (which, of course, includes you).

There is still surplus value accrued to your company (if you do good work for your company, you will be contributing to their reputation from which they will profit). However, if you are primarily writing code, the vast majority of your labor **is** producing a product that you are fundamentally not alienated from.

Proudhon, while sprinkling the word revolution throughout his writings, was notably not particularly revolutionary in his calls for action. He was reformist. Rather than calling for a direct confrontation with capitalism, he called for worker-run collectives to be formed throughout the world to make capitalism irrelevant. While many people point to corporate use of free software as an argument for why free software and capitalism are perfectly compatible, the truth may be more complicated. Fundamentally, and in a non-confrontational way, free software seems to undermine one of the basic tenants of capitalist: worker alienation. 

Now we just need to work on all the other ways corporate work places are alienating...


