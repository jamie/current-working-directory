---
title: "Servers4All... unless someone complains"
tags: ["sysadmin","debian","spam"]
date: 2012-01-08T09:32:44.189780
---

On Wednesday, February 1, a new virtual server [May First/People Link](https://mayfirst.org/) recently rented went offline.  We contracted the virtual server through [Server4All](http://server4all.org) because we need their un-metered 100Mbit connection to help us handle the bandwidth for [Sahara Reporters](http://saharareporters.com/), one of the most important independent news sources for Saharan Africa. With the server offline, the web site was down as well.

We scrambled to setup alternative caching servers to handle the bandwidth.

When I logged into our control panel, I saw the message: This virtual server has been suspended by the administrator. Please contact support.

I immediately contacted support and then received the message:

    Hello

    We have received the following complaint associated with your server/service.

    IP: 76.73.121.164

    To prevent any further abuse we have suspended this service. In order to
    resume, we request you to cooperate with our investigation as promptly as
    possible. Please respond to us with the following details:

    (1) What has caused the complaint
    (2) What is the server used for. Purpose?
    (3) How can you resolve the complaint and make sure it will not be repeated.

    Depending on the nature of the complaint and your response, we will put back
    the server online.  Please note, this has violated our Terms Of Service. We
    expect your response within 24 hours, otherwise your account will be
    terminated permanently.  Thank you

What?? What complaint?? I followed up but had to wait til the next day to get the response.

    Here is the full log,

    An email advertizing the Domain Name: saharareporters.com
    has been sent to the blacklist.woody.ch spamtrap.

    This Domain does resolve to IP addresses one of which your are responsible:
    76.73.121.164

    Please investigate why this Domain has been advertized.

    Attached you find the headers and reports in ARF for automatic processing.
    Feedback is appreciated.

    Actual listing periods:

    Bounce: 1 Hour in DNS.
    Whitelisted IP: Not lised in DNS.
    Spam: 24 hours in DNS.

    Every Hit: 14 days in evidence DB.

    For any questions or Feedback, contact abuse@woody.ch

    From: is intentionally set to a bit-bucket.

    Kind regards
    -Benoit Panizzon- 

There is no attachment. I went to woody.ch and it was in German. Then tried blacklist.woody.ch, but no luck. Finally I found the [Woody's World Blacklist Page](http://blacklist.woody.ch/rblcheck.php3). I plugged in our IP address into their checker and I got:

    Output from the Check, if empty the IP is not listed.
    164.121.73.76.[name of the blacklist] being tested.

    Host 164.121.73.76.blacklist.woody.ch not found: 3(NXDOMAIN)
    Host 164.121.73.76.blacklist.woody.ch not found: 3(NXDOMAIN)

    Host 164.121.73.76.rbl.maps.vix.com not found: 3(NXDOMAIN)
    Host 164.121.73.76.rbl.maps.vix.com not found: 3(NXDOMAIN)

    Host 164.121.73.76.relays.mail-abuse.org not found: 3(NXDOMAIN)
    Host 164.121.73.76.relays.mail-abuse.org not found: 3(NXDOMAIN)

    ;; connection timed out; no servers could be reached
    ;; connection timed out; no servers could be reached

    ;; connection timed out; no servers could be reached
    ;; connection timed out; no servers could be reached

    Host 164.121.73.76.relays.ordb.org not found: 3(NXDOMAIN)
    Host 164.121.73.76.relays.ordb.org not found: 3(NXDOMAIN)

    Host 164.121.73.76.dev.null.dk not found: 3(NXDOMAIN)
    Host 164.121.73.76.dev.null.dk not found: 3(NXDOMAIN)

    Host 164.121.73.76.blackholes.five-ten-sg.com not found: 3(NXDOMAIN)
    Host 164.121.73.76.blackholes.five-ten-sg.com not found: 3(NXDOMAIN)

    Host 164.121.73.76.bl.spamcop.net not found: 3(NXDOMAIN)
    Host 164.121.73.76.bl.spamcop.net not found: 3(NXDOMAIN)

    Host 164.121.73.76.relays.visi.com not found: 3(NXDOMAIN)
    Host 164.121.73.76.relays.visi.com not found: 3(NXDOMAIN)

    164.121.73.76.blacklist.spambag.org has address 208.91.197.182
    164.121.73.76.blacklist.spambag.org descriptive text "v=spf1 -all"

So, Woody's World thinks we are listed in spambag.org. I went to [spambag.org](http://spambag.org) and learned that the domain is for sale. I then tried [blacklist.spambag.org](http://blacklist.spambag.org) and got the same page. This page has many links all pointing to advertisements. The "RBL List" link takes me to a page advertising "5 foods you must not eat."

Next, out of curiousity, I tried [Mxtoolbox](http://www.mxtoolbox.com/SuperTool.aspx). I got one hit from Barricuda. Barricuda says the reputation of the IP address is "poor". Why? According to Barricuda, the reasons could be:

 * Your email server contains a virus and has been sending out spam.
 * Your email server may be improperly configured.
 * Your PC may be infected with a virus or botnet software program.
 * Someone in your organization may have an infected PC with a virus or botnet program.
 * You may be using a dynamic IP address which was previously used by a known spammer.
 * Your marketing department may be sending out bulk emails that do not comply with the CAN-SPAM Act.
 * You may have an insecure wireless network which is allowing unknown users to use your network to send spam.
 * In some rare cases, your recipient's Barracuda Spam Firewall may be improperly configured.

Keep in mind, this IP address is not used for sending email. It's just the web site.

I then took a step back and re-read the complaint and noticed that it says that the domain name saharareporters.com was listed in a spam email. Hm. More concerted searching for the terms "woody spamtrap blacklist" and I found a pattern in URLs that suggested I plug in the following:

http://news.scoutnet.org/rblhostlist.php?id=saharareporters.com.uri 

And sure enough, there was a result. In short, it was a classic Nigerian Oil scam in which the person claims to be "JAMES IBORI ex-governor of DELTA STATE oil city." The scammer acknowledges that he has been arrested, but promises lots of cash to the person who can help him. In an effort to boost their credibility, the scammer included a link to a Sahara Reporters article about the real [James Ibori](http://saharareporters.com/news-page/uk-money-laundering-trial-james-ibori-suffers-various-reverses).

And that, my friends, is enough to have one of the most prominent independent African news organization taken offline. 

<del>But, Sahara Reporters shouldn't necessarily feel singled out.</del> Sahara Reporters should feel singled out. They exist to illuminate news from Africa. They were taken offline because a series of individuals don't know the difference between a prominent independent African news service and a criminal scammer. To most of the Internet, Nigeria and email scams are synonymous. That has to change.

Although Sahara Reporters is particularly vulnerable, any site hosted with Server4All can potentially be taken down. All you have to do is write a fake spam/scam email, including a link to the web site you want to be taken offline, and then send that email to: listme@blacklist.woody.ch.

I'm currently following up with both Woody's World and Server4All. However, once this particular issue is resolved, we're left with a much bigger and ominous problem. If your hosting provider (or their upstream provider) takes your site offline when it receives a complaint first and then asks questions second, you have a big problem. 

All of our legal fights over our rights to keep content online are moot if our providers, without any legal pressure to do so, still take down our services based on spurious complaints. 



