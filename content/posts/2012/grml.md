---
title: "Creating virtual machines"
tags: ["debian","sysadmin"]
date: 2012-01-08T09:32:44.189780
---

I recently setup an Alix board from [PC Engines](http://pcengines.ch). I installed Debian onto a Compact Flash card using grml for the first time.

After using parted to create a single partition, I ran:

    grml-debootstrap --arch i386 --filesystem ext4 --grub /dev/sdb --hostname yaqeen --mirror http://http.us.debian.org/debian \ 
    --nopassword  --release squeeze --target /dev/sdb1

I had to manually mount the partition and add console redirection for grub (and /etc/inittab).

And then, when booting, I had to manually edit the grub configuration because it was trying to find the root filesystem on /dev/sdb instead of /dev/sda. I'm sure there is some tweaking I can do via /etc/deboostrap/scripts to fix those needs.

Overall, I was very happy with the results. Thanks grml developers!

Update 2012-06-19

When installing a server I prefer keeping things a bit more manual.

I start by doing a pxeboot into a [debirf image](http://cmrg.fifthhorseman.net/wiki/debirf). Then, I run through all the [disk partitioning/encrypting/etc steps listed on the May First/People Link installation page](https://support.mayfirst.org/wiki/install_debian#DrivepartioningDisksetup).

Then, I mount each partition in /mnt and run debootstrap:

    debootstrap squeeze /mnt

Copy over any non-free firmware packages, e.g.:

    cp /media/usb/firmware-bnx2_0.28+squeeze1_all.deb /mnt/root/

Next, mount the system directories and chroot:

    for dir in sys dev proc; do mkdir -p /mnt/$dir; mount -o bind /$dir /mnt/$dir; done
    chroot /mnt

Install non-free firmware packages, e.g.:

    dpkg -i /root/firmware-bnx2_0.28+squeeze1_all.deb

Add /etc/fstab, /etc/network/interfaces, /etc/crypttab files

Edit /etc/inittab to enable output to the serial console

Set the root password.

Set the hostname (edit /etc/hostname)

Install necessary packages that debootstrap won't install for you:

    apt-get update
    apt-get install linux-image-2.6-amd64 lvm2 cryptsetup mdadm grub-pc

When prompted by grub for Linux command line options, enter:

    console=ttyS0,115200n8

Edit /etc/default/grub adding to the bottom:

    GRUB_TERMINAL=serial
    GRUB_SERIAL_COMMAND="serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"

Edit /boot/grub/device.map to remove any USB devices

  And run update-grub
 
