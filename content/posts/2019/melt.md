---
title: "Editing video without a GUI? Really?"
tags: ["debian", "video"]
date: 2019-10-08T09:21:39.067533
---

It seems counter intuitive - if ever there was a program in need of a graphical
user interface, it's a non-linear video editing program.

However, as part of the [May First](https://mayfirst.coop/) board elections, I
discovered otherwise.

We asked each board candidate to submit a 1 - 2 minute video introduction about
why they want to be on the board.  My job was to connect them all into a single
video. 

I had an unrealistic thought that I could find some simple tool that could
concatenate them all together (like `mkvmerge`) but I soon realized that this
approach requires that everyone use the exact same format, codec, bit rate,
sample rate and blah blah blah.

I soon realized that I needed to actually *make* a video, not compile one. I
create videos so infrequently, that I often forget the name of the video
editing software I used last time so it takes some searching. This time I found
that I had [openshot-qt](https://tracker.debian.org/pkg/openshot-qt) installed
but when I tried to run it, I got a back trace (which [someone else has already
reported](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=940839)).

I considered looking for another GUI editor, but I wasn't that interested in
learning what might be a complicated user interface when what I need is so
simple.

So I kept searching and found [melt](https://tracker.debian.org/pkg/mlt). Wow.

I ran:

    melt originals/* -consumer avformat:all.webm acodec=libopus vcodec=libvpx

And a while later I had a video. Impressive. It handled people who submitted
their videos in portrait mode on their cell phones in mp4 as well as web cam
submissions using webm/vp9 on landscape mode.

Thank you melt developers!
