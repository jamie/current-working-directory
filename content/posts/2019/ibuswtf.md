---
title: "I didn't know what ibus was one day ago, now I love it"
tags: ["debian", "desktop", "language-justice"]
date: 2019-02-11T15:50:56.210555
---

[See update below.]

After over a decade using mutt as my email client, I finally gave up pretending
I didn't want to see pretty pictures in my email and switched to Thunderbird.

Since I don't write email in spanish often, I didn't immediately notice that my
old [dead key](https://en.wikipedia.org/wiki/Dead_key) trick for typing spanish
accent characters didn't work in Thunderbird like it does in vim or any
terminal program.

I learned many years ago that I could set a special key via my openbox
autostart script with `xmodmap -e 'keysym Alt_R = Multi_key'`. Then, if I
wanted to type é, I would press and hold my right alt key while I press the
apostrophe key, let go, and press the e key. I could get an ñ using the same
trick but press the tilde key instead of the apostrophe key. Pretty easy.

When I tried that trick in Thunderbird I got an upside down e. WTF.

I spent about 30 minutes clawing my way through search results on several
occassions over the course of many months before I finally found someone say:
"I installed the ibus package, rebooted and it all worked." (Sorry Internet, I
can't find that page now!)  

ibus? `apt-cache show ibus` states:

    IBus is an Intelligent Input Bus. It is a new input framework for the Linux
    OS. It provides full featured and user friendly input method user
    interface. It also may help developers to develop input method easily.

Well, that's succinct. I still had no idea what ibus was, but it sounded like
it might work. I followed those directions and suddenly in my system tray area,
there was a new icon. If I clicked on it, it listed by default my English
keyboard.

I could right click, hit preferences and add a new keyboard:

English - English (US, International with dead keys)

Now, when I select that new option, I simply press my right alt key and e (no
need for the apostrophe) and I get my é. Same with ñ. Hooray!

My only complaint is that while using this keyboard, I can't using regular
apostrophes or ~'s. Not sure why, but it's not that hard to switch.

As far as I can tell,
[ibus](https://en.wikipedia.org/wiki/Intelligent_Input_Bus) tries to abstract
some of the difficulties around input methods so it's easier on GUI developers.

**Update 2019-02-11**

Thanks, Internet, particularly for the comment from Alex about how I was
choosing the wrong International keyboard. Of course, my keyboard does not have
dead keys, so I need to choose the one called "English (intl., with AltGr dead
keys)."

Now everything works perfectly. No need for ibus at all. I can get é with my
right alt key followed by e. It works in my unicode terminal, thunderbird, and
everywhere else that I've tried.
