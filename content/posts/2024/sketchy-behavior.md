---
title: "Sketchy Behavior"
date: 2024-09-12T08:27:10-04:00
tags: [ "debian", "sysadmin", ]
draft: true
---

Web compromises are a pain. Anyone who manages an initial compromise has some many options for keeping the door open.

Recently, we noticed a site with the following processes identified by ps:

```
usernam+  8576     1  0  2970  7340   1 Sep11 ?        00:00:00   /usr/sbin/client
usernam+  8577     1  0  2969  7304   0 Sep11 ?        00:00:00   /usr/sbin/client
usernam+  8651     1  0  2958  7368   2 Sep11 ?        00:00:00   /usr/sbin/client
usernam+  8652     1  0  2938  7388   1 Sep11 ?        00:00:01   /usr/sbin/client
```

There isn o `/usr/sbin/client` on the system and `lsof -p 8577` reveals:

```
COMMAND    PID          USER   FD   TYPE             DEVICE SIZE/OFF      NODE NAME
/usr/sbin 8577 username  cwd    DIR              253,0     4096         2 /
/usr/sbin 8577 username  rtd    DIR              253,0     4096         2 /
/usr/sbin 8577 username  txt    REG              253,0  3201864     66960 /usr/bin/perl
/usr/sbin 8577 username  mem    REG              253,0    47480    320331 /usr/lib/x86_64-linux-gnu/perl/5.28.1/auto/Socket/Socket.so
/usr/sbin 8577 username  mem    REG              253,0    43328     16492 /lib/x86_64-linux-gnu/libcrypt-2.28.so
/usr/sbin 8577 username  mem    REG              253,0  1820400     16427 /lib/x86_64-linux-gnu/libc-2.28.so
/usr/sbin 8577 username  mem    REG              253,0   146968     16951 /lib/x86_64-linux-gnu/libpthread-2.28.so
/usr/sbin 8577 username  mem    REG              253,0  1579448     16534 /lib/x86_64-linux-gnu/libm-2.28.so
/usr/sbin 8577 username  mem    REG              253,0    14592     16527 /lib/x86_64-linux-gnu/libdl-2.28.so
/usr/sbin 8577 username  mem    REG              253,0    22800    320317 /usr/lib/x86_64-linux-gnu/perl/5.28.1/auto/IO/IO.so
/usr/sbin 8577 username  mem    REG              253,0   165632     16355 /lib/x86_64-linux-gnu/ld-2.28.so
/usr/sbin 8577 username    0r  FIFO               0,12      0t0 674219666 pipe
/usr/sbin 8577 username    1w  FIFO               0,12      0t0 674218799 pipe
/usr/sbin 8577 username    2u   CHR                1,3      0t0      1028 /dev/null
/usr/sbin 8577 username    3u  IPv4          675551210      0t0       TCP gaspar.mayfirst.org:36548->167.71.55.227:ircd (SYN_SENT)
/usr/sbin 8577 username    4u  unix 0x000000000e5cbb19      0t0 674218798 /var/run/php/7.4-site100236.sock type=STREAM
/usr/sbin 8577 username   63u  unix 0x000000006330398a      0t0 670636451 /var/run/php/7.4-site100236.sock type=STREAM
```

So, it's really a perl program. It was presumably launched via (and is still holding open) a php fpm process.

I tried `ls -ld /proc/8577` and got a date, but grepping the web log for that date and nothing turns up. There are a few hits a few seconds before but I don't see anything suspicious.

There are no cron jobs and no suspicious ssh logins.

I try grepping the web directory for "perl" and ended up (quite coincindentally)
finding [Tiny File Manager](https://github.com/prasathmani/tinyfilemanager) in
places it doesn't belong (it happens to have the string "perl" in the code).

So yeah, this is one way they have gotten in, but grepping the logs for this
filename in the last week turns up nothing. This site has been compromised for
so long that some of the backdoors have been forgotten?

Since the suspicious tiny file manager was in the tmp directory, what else is there that ends in .php?

Lots of legit looking files thanks to the template engine, but plenty of suspicious ones, including one with:

```
         <title>MARIJUANA</title>

                        <link rel="icon" href="//0x5a455553.github.io/MARIJUANA/icon.png" />
                        <link rel="stylesheet" href="//0x5a455553.github.io/MARIJUANA/main.css" type="text/css">

                        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script src="//cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
                </head>

                <body>
                        <header>
                                <div class="y x">
                                        <a class="ajx" href="<?php echo basename($_SERVER['PHP_SELF']);?>">
                                                MARIJuANA
                                        </a>
                                </div>

                                <div class="q x w">
                                        &#8212; DIOS &#8212; NO &#8212; CREA &#8212; NADA &#8212; EN &#8212; VANO &#8212;
                                </div>

```

A marijuana store with a quote: "God creates nothing in vain." Nice.

But nothing in the logs matching these files.

I also grepped the database dump for "bin/perl" without luck.







