---
title: "Gmail vs Tor vs Privacy"
date: 2024-09-18T08:27:10-04:00
tags: [ "debian", "sysadmin", "email" ]
---

A legit email went to spam. Here are the redacted, relevant headers:

```
[redacted]
X-Spam-Flag: YES
X-Spam-Level: ******
X-Spam-Status: Yes, score=6.3 required=5.0 tests=DKIM_SIGNED,DKIM_VALID,
[redacted]
	*  1.0 RCVD_IN_XBL RBL: Received via a relay in Spamhaus XBL
	*      [185.220.101.64 listed in xxxxxxxxxxxxx.zen.dq.spamhaus.net]
	*  3.0 RCVD_IN_SBL_CSS Received via a relay in Spamhaus SBL-CSS
	*  2.5 RCVD_IN_AUTHBL Received via a relay in Spamhaus AuthBL
	*  0.0 RCVD_IN_PBL Received via a relay in Spamhaus PBL
[redacted]
[very first received line follows...]
Received: from [10.137.0.13] ([185.220.101.64])
        by smtp.gmail.com with ESMTPSA id ffacd0b85a97d-378956d2ee6sm12487760f8f.83.2024.09.11.15.05.52
        for <xxxxx@mayfirst.org>
        (version=TLS1_3 cipher=TLS_AES_128_GCM_SHA256 bits=128/128);
        Wed, 11 Sep 2024 15:05:53 -0700 (PDT)
```

At first I though a Gmail IP address was listed in spamhaus - I even opened a
ticket. But then I realized it wasn't the last hop that Spamaus is complaining
about, it's the first hop, specifically the ip `185.220.101.64` which appears
to be a Tor exit node.

The sender is using their own client to relay email directly to Gmail. Like any
sane person, they don't trust Gmail to protect their privacy, so they are
sending via Tor. But WTF, Gmail is not stripping the sending IP address from
the header.

I'm a big fan of harm reduction and have always considered using your own
client to relay email with Gmail as a nice way to avoid some of the
surveillance tax Google imposes.

However, it *seems* that if you pursue this option you have two unpleasant
choices:

 * Embed your IP address in every email message or
 * Use Tor and have your email messages go to spam

I supposed you could also use a VPN, but I doubt the IP reputation of most VPN
exit nodes are going to be more reliable than Tor.
