---
title: "Beach Day"
tags: ["dakar2011"]
date: 2011-01-08T09:32:44.185780
---

We took the afternoon off today. A 15 minute cab ride followed by a short 5 minute boat ride took us to N'gore Island. 

![Joseph on the Boat to N'gore Island](/posts/2011/dakar-beach/joseph.on.the.boat.to.n.gore.island.jpg "Joseph on the Boat to N'gore Island")  
*Joseph on the Boat to N'gore Island*

![Abondoned structure on N'gore Island](/posts/2011/dakar-beach/abondoned.structure.on.n.gore.island.jpg "Abondoned structure on N'gore Island")  
*Abondoned structure on N'gore Island*

![Joseph, Ross and Mallory in the ocean off N'gore Island](/posts/2011/dakar-beach/in.the.ocean.jpg "Joseph, Ross and Mallory in the ocean off N'gore Island")  
*Joseph, Ross and Mallory in the ocean off N'gore Island*

