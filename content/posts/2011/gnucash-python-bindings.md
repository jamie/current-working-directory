---
title: "GnuCash with python bindings"
tags: ["debian","gnucash"]
date: 2015-05-06T09:13:58.693546
---

Hats off to the [GnuCash](http://www.gnucash.org/) developers and the [Parit Worker Collective](http://parit.ca/) for the python bindings to GnuCash. If there is any doubt that free software makes life easier it's being able to write your own accounting import scripts while reading the original source code.

It's 2015-05-06 and I'm updating this blog post that I wrote back in 2011 since you no longer have to rebuild gnucash on Debian to get the python bindings. Thanks Debian!

I've also published May First/People Link's python import scripts via git: 
    git clone git://git.mayfirst.org/mfpl/gnucash-import.git

I'm happy to report that the import script now does the following:

 * Imports [May First/People Link](https://mayfirst.org/) members as GnuCash "Customers"
 * Imports membership dues invoices as GnuCash Invoices
 * Imports payments and applies them against the Invoices
 * Imports deleted invoices and puts them in our Unrecoverable A/R account

At this point we have eliminated all double data entry between our membership database and our accounting system. I can't say that I enjoy bookkeeping yet, but it's a lot better than before.



