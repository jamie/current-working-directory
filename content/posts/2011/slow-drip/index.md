---
title: "Slow Drip and other tales from Dakar"
tags: ["dakar2011"]
date: 2011-01-08T09:32:44.189780
---

We're slowly adjusting to daily power outages. We've only had one that last 24
hours... most are just 1 - 2 hours. 

![Slow Drip](/posts/2011/slow-drip/slow.drip.jpg "Slow Drip")  
*Joseph earns the nickname Slow Drip making coffee in the dark kitchen*

Senegal is not exactly Tunisia or Egypt, but the increasingly frequent and prolonged power outages have caused some tire-burning in the streets.

![Fire in the streets](/posts/2011/slow-drip/fire.in.the.streets.jpg "Fire in the streets")  
*Fire in the streets*

If you are curious about food... 

![Breakfast in Dakar](/posts/2011/slow-drip/breakfast.in.dakar.jpg "Breakfast in Dakar")  
*Breakfast in Dakar*

I took a break from working to pose for this picture. The writing says: Le
Culte du Travail (The cult of work). We found this in the University.

![Cult du travail](/posts/2011/slow-drip/cult.du.travail.jpg "Le Culte du Travail (The cult of work)")  
*Le Culte du Travail*

In other news, the convergence center house has been rented. It's a great house
- huge, 2 floors, 5 bedrooms and, somehow, 5 bathrooms. Joseph and I just
walked the 15 minutses to the University (another 15 minutes to the library
where the media center is). Wow.  I've never attended a social forum within
walking distance to where I was staying! We will be fitting about 30 of us
into the house (and the two apartments down the streets). We will move into
the house on February 2.

