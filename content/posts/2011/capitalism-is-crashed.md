---
title: "Capitalism Is Crashed"
tags: ["debian","mayfirst"]
date: 2011-01-08T09:32:44.185780
---

The Occupy Wall Street protests taking place around the world are incredibly moving. 

And, at [May First/People Link](https://mayfirst.org/), the joint us jumping with new projects. [Occupy Together](http://occupytogether.org) was the first to join, followed by the [Occupy Wall Street Journal](http://occupiedmedia.com). We then [launched boggs.mayfirst.org as our dedicated Occupy Wall Street shared server](https://mayfirst.org/lowdown/action/mfpl-offering-free-hosting-occupy-movement-organizations) and the radical tech work keeps on coming ([OccupyTechnoloyg](http://occupytechnology.org/), [OccupyProvidence](http://occupyprovidence.com), [OccupyColumbus GA](http://occupycolumbusga.org/) just to name a few).

It's also great to see free software and [responsible software as a service](https://mayfirst.org/lowdown/guest-article/progressive-technology-principles-and-software-service) making it's way into the conversation. I've heard many reports about the politics of technology taking it's rightful place in the realm of political decisions being made.

I don't have the context for this photo from the blog [non ti stavo cercando](http://www.nontistavocercando.it/), but it sure sums up the connection :

![Capitalism is Crashed! Install news system?](http://www.nontistavocercando.it/wp-content/uploads/2011/10/DSC_0137r-500x332.jpg "YES! apt-get install anarchism")



