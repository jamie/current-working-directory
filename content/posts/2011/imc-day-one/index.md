---
title: "IMC Convergence Center Day One"
tags: ["dakar2011"]
date: 2011-01-08T09:32:44.185780
---

Fabian, from London and Molefi from South African, arrived early monday morning - and were instrumental in our last day preparations for the IMC Africa
convergence Center.

![Advance Team](/posts/2011/imc-day-one/advance.team.jpg "Advance Team")  
*Advance Team pictured on the 2nd floor terrace of the House*

Turns out we got the numbers wrong - 30 delegates arrived on the day we
negotiated access to the first two bedroom apartment. Yikes. Fortunately, the
house was ready a day early and our landlord was willing to let us move in a
day early. 

The "mattresses" are one inch foam pads, but everyone is very flexible and in
good spirits. 

On day 1 (Wednesday), we met in the morning and formed four committees:
finance, program, welfare, and mediation.

![Preparing our first lunch](/posts/2011/imc-day-one/preparing.our.first.lunch.jpg "Preparing our first lunch")  
*Preparing our first lunch*

After the committees met, we had down time while we waited for lunch.

![In the backyard](/posts/2011/imc-day-one/in.the.backyard.jpg "In the backyard")  
*In the backyard*

