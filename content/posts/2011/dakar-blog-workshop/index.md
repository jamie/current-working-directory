---
title: "Dakar Blog Workshop"
tags: ["dakar2011"]
date: 2011-01-08T09:32:44.185780
---

May First/People Link collaborated with Mousa from Rwanda to teach our first workshop at the convergence center. Mousa handled the french speaking participants while Ross, Joseph and Mallory trained the english speakers. The [resulting web site](http://imc-africa.mayfirst.org/) brought tears to my eyes.

![workshop1](/posts/2011/dakar-blog-workshop/workshop1.jpg "workshop1")  

![workshop2](/posts/2011/dakar-blog-workshop/workshop2.jpg "workshop2")  

![workshop3](/posts/2011/dakar-blog-workshop/workshop3.jpg "workshop3")  

![workshop4](/posts/2011/dakar-blog-workshop/workshop4.jpg "workshop4")  

![workshop5](/posts/2011/dakar-blog-workshop/workshop5.jpg "workshop5")  

![workshop6](/posts/2011/dakar-blog-workshop/workshop6.jpg "workshop6")  

![workshop7](/posts/2011/dakar-blog-workshop/workshop7.jpg "workshop7")  

![workshop8](/posts/2011/dakar-blog-workshop/workshop8.jpg "workshop8")  

![workshop9](/posts/2011/dakar-blog-workshop/workshop9.jpg "workshop9")  

![workshop10](/posts/2011/dakar-blog-workshop/workshop10.jpg "workshop10")  

![workshop11](/posts/2011/dakar-blog-workshop/workshop11.jpg "workshop11")  

![workshop12](/posts/2011/dakar-blog-workshop/workshop12.jpg "workshop12")  

![workshop13](/posts/2011/dakar-blog-workshop/workshop13.jpg "workshop13")  

![workshop14](/posts/2011/dakar-blog-workshop/workshop14.jpg "workshop14")  

![workshop15](/posts/2011/dakar-blog-workshop/workshop15.jpg "workshop15")  

![workshop16](/posts/2011/dakar-blog-workshop/workshop16.jpg "workshop16")  

![workshop17](/posts/2011/dakar-blog-workshop/workshop17.jpg "workshop17")  

![workshop18](/posts/2011/dakar-blog-workshop/workshop18.jpg "workshop18")  

![workshop19](/posts/2011/dakar-blog-workshop/workshop19.jpg "workshop19")  

![workshop20](/posts/2011/dakar-blog-workshop/workshop20.jpg "workshop20")  

![workshop21](/posts/2011/dakar-blog-workshop/workshop21.jpg "workshop21")  

![workshop22](/posts/2011/dakar-blog-workshop/workshop22.jpg "workshop22")  

