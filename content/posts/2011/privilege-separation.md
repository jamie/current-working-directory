---
title: "Privilege Separation"
tags: ["debian","sysadmin","security"]
date: 2011-01-08T09:32:44.189780
---

What are the biggest security threats to my laptop? Almost all the software I install is vetted and signed by a member of the Debian team. I run reliable virus software on our mail server. My disk is encrypted and xscreensaver locks my screen after a few minutes of inactivity. What else? 

The two biggest threats I've recently considered are: web browsing and non-free software or software that doesn't come from Debian (I regularly have to use skype and zimbra, for example).

To mitigate these risks, I've configured these programs to run as their own users, thus adding a layer of separation between the programs and my primary user account, which has access to my gpg/ssh keys and email.

With a program like zimbra it's fairly easy. I created a zimbra user, added my primary account's public ssh key to /home/zimbra/.ssh/authorized_keys, and add the following stanza to ~/.ssh/config:

    Host zimbra 
    Hostname localhost 
    User zimbra 
    ForwardX11 yes

Now I can start zimbra with:

    ssh zimbra ./zdesktop-launch

Skype was a little harder, since the skype client has to access the audio system. With [pulseaudio](http://packages.debian.org/search?keywords=pulseaudio), though, it's a snap. I copied /etc/pulse/default.pa to ~/.pulse/ and added the line:

    load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1

Then, I added /home/skype/.pulse/client.conf with the contents:

    default-server = 127.0.0.1

[Note: Jamie Rollins and dkg have pointed out that this arrangement allows any user on my laptop to send arbitrary data to pulseaudio, running as my primay account. They suggested configuring pulseaudio to listen on a unix-domain socket, and then configuring pulseaudio to only permit access to users in a particular group.] 

iceweasel is the most complicated. In addition to the pulseaudio trick, I had to make two other allowances.

First, there are a lot of processes that launch a web browser in a number of different ways (sometimes asking for a new session, other times adding a tab to an existing one, sometimes passing an URL as an argument, etc). 

The one that I got the most stuck on was mutt. Sometimes I want to see how an HTML message looks in iceweasel. Via mailcap, mutt creates a temp file with the html content and then launches a web browser to view the file. As can be reasonably expected, this temp file is owned by my primary user, and only read-able by the owner. That means my iceweasel user can't read it.  

Eventually, I decided the easiest way to deal with these various scenarios was to write a simple bash script to launch my web browser (see below). I registered it via update-alternatives, so most reasonable programs that want to launch a web browser will use it.

The second issue is that I use the [monkeysphere xul plugin](http://packages.debian.org/search?keywords=xul-ext-monkeysphere) to verify TLS certificates, which requires iceweasel to communicate with my monkeysphere validation agent. My agent runs as my primary user and by default only responds to queries from my primary user.

Fortunately, monkeysphere is well-designed and can handle this situation. As you can see from my web launcher script, I pass MONKEYSPHERE_VALIDATION_AGENT_SOCKET=$MONKEYSPHERE_VALIDATION_AGENT_SOCKET when calling iceweasel. In addition, I added the following before I exec monkeysphere-validation-agent:

    export MSVA_ALLOWED_USERS="iceweasel jamie"

With this simple infrastructure setup, it's possible to easily isolate future programs as well.

Lastly... here's the script for launching iceweasel.

    #!/bin/bash

    stdin=
    new_session=no
    url=

    for arg in "$@"; do
      if [[ "$arg" =~ ^-- ]]; then 
        if [ "$arg" = "--new-session" ]; then
          new_session=yes
        elif [ "$arg" = "--from-stdin" ]; then
          stdin=yes
        fi
      else
        url="$1"
      fi
    done

    if [ "$stdin" = "yes" ]; then
      temp=$(mktemp)
      while read line; do
        echo "$line" >> "$temp"
      done
      # it must be readable by the iceweasel user
      chmod 755 "$temp"
      url="file:///$temp"
    fi

    args=
    if [ "$new_session" = "yes" ]; then
      args="--no-remote -ProfileManager"
    fi

    if [ -n "$url" ]; then
      args="$args '$url'"
    fi

    ssh iceweasel "MONKEYSPHERE_VALIDATION_AGENT_SOCKET=$MONKEYSPHERE_VALIDATION_AGENT_SOCKET iceweasel $args" &

    [ -f "$temp" ] && sleep 5 && rm "$temp"

Comments by email...

Hi,

http://current.workingdirectory.net/posts/2011/privilege-separation/ has good intentions but afaik it does not improve security much. X applications can sniff your passwords and inject commands to your terminal emulators.

I personally use xpra to get a similar solution without the hazards of X. I've been using it for two months now both at work and home. There are still bugs but svn version is getting better all the time.  

See for example my reply to

[https://grepular.com/Protecting_a_Laptop_from_Simple_and_Sophisticated_Attacks](https://grepular.com/Protecting_a_Laptop_from_Simple_and_Sophisticated_Attacks)


Timo
