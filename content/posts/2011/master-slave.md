---
title: "We need better metaphors"
tags: ["debian","oppression","mysql"]
date: 2011-01-08T09:32:44.185780
---

Most of us developers use metaphors to help convey an understanding of how our software works. Sadly, many developers choose the "master" and "slave" metaphor. 
Why would we voluntarily choose such an ugly human human interaction with a real and present history in many of our lives to describe software that we've written?

I wouldn't rule out the possibility that someone will (or already has) invented software for which the master/slave metaphor aptly conveys its function, however, I'd prefer not to use it. Also, many people **intentionally** play with master/slave relationships because of the history and connotations it provokes.

However, use of this metaphor in software like [MySQL databases](http://dev.mysql.com/doc/refman/5.0/en/replication.html) or [Bind Name servers](http://www.zytrax.com/books/dns/ch4/#master)  seems pointless, offensive and mis-leading.

I've never seen the use of the master/slave metaphor in software that conveyed more than the idea that one piece of the software essentially controls, wholly or in part, another piece of software. So, why master/slave? There are many metaphors to choose from - parent/infant, boss/worker, guard/prisoner, king/subject, landowner/serf, top/bottom, etc.

What does master/slave convey that the others don't convey? 

Or, maybe they all convey the same thing, so it doesn't make a difference which one you choose (after all, it's just a metaphor). In that case, how about replacing master/slave with the metaphor husband/wife? Whoops. Now that's offensive (if you don't think that's offensive you can stop reading here and please don't post any comments - I won't change your mind). 

But, that's different right? At one point in history, the role of the wife was to do as her husband says, but it's 2011, and we've evolved beyond that, so by using husband/wife we'd be re-enforcing that ugly, sexist stereotype.

However, how is that different from master/slave? It **is** 2011 after all. Isn't the role of the master to abdicate and the role of the slave to revolt? 

Maybe that's why Bind has so many security problems...


