---
title: "Arriving in Dakar"
tags: ["dakar2011"]
date: 2011-01-08T09:32:44.185780
---

We've safely arrived. Joseph, Ross and I have joined Mallory in Dakar for the
[World Social Forum](http://fsm2011.org). It's the first time May First/People
Link has had an opportunity to travel with four technologists.

Joseph and I met up with Ross in Washington...

![Joseph and Ross before departing from Washington, DC](/posts/2011/dakar-arrival/joseph.and.ross.iad.jpg "Joseph and Ross before departing from Washington, DC")  
*Joseph and Ross before departing from Washington, DC*

The flight was smooth, landing in Dakar at 6:00 am. 

![First Stop: Bakery](/posts/2011/dakar-arrival/first.stop.bakery.jpg "First Stop: Bakery")  
*First Stop: Bakery*

Mallory, once again, found an amazing space for us to stay, complete with Internet access and art work. This place is way more posh than our lovely techie house in Detroit.

![Dakar Mona Lisa](/posts/2011/dakar-arrival/dakar.mona.lisa.jpg "Dakar Mona Lisa")  
*Dakar Mona Lisa*

And, after going some insane number of hours without sleep, we had dinner with several other members of the international.

![International Team](/posts/2011/dakar-arrival/international.team.jpg "International Team")  
*International Team*

More to come!
