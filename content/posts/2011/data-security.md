---
title: "Data Security"
tags: ["mayfirst","security"]
date: 2011-01-08T09:32:44.185780
---

The way we evaluate Internet technology is changing ... quickly.

Seems like you can't read the news these days without hearing about a [security breach leaking hundreds of thousands of email addresses](https://www.nytimes.com/2011/04/05/business/05hack.html), a ["redundant" cloud service being down for over two days](http://bits.blogs.nytimes.com/2011/04/21/amazon-cloud-failure-takes-down-web-sites/), or a mobile device maker [accidentally tracking your movements](https://www.nytimes.com/2011/04/21/business/21data.html).

There was a time when most movement activists picked technology based on functionality and intuitive and well-designed user interfaces. 

However, now that we're more dependent on more technology for things more important to our lives, we're finding that we need to add a deeper foundational integrity to our list of criteria.

Sadly, as May First/People Link members learned earlier this month, data breaches don't just happen to profiteering corporations looking to cut corners to make a quick buck. In the worst data loss of our organization's history, we were unable to restore databases for about 20 of our members after a server had to be re-build from the backup. The failure, which was a calamity for the members involved, was the result of a series of mistakes on our part.

While we were not able to bring back the data, we did our best to model an honest and productive response, starting with a straight forward [report of the problem](https://lists.mayfirst.org/pipermail/service-advisories/2011-April/000245.html), personal contact to all affected members (we missed a few due to in-correct contact information), a [member-wide email invitation to a meeting to address the situation](https://lists.mayfirst.org/pipermail/lowdown/2011-April/000080.html), a [set of proposals to ensure it won't happen again](https://support.mayfirst.org/wiki/proposals/2011/new-data-protection-procedures), and an ad-hoc committee to see those proposals through.

The importance of our data and software will only grow in the future. There's no question we will need to continuously react to breaches affecting us, whether it's on May First/People Link or any one of the corporate providers our movement is still dependent on. Let's also remember to be pro-active - to conciously discuss and act to build a network we can trust. 




