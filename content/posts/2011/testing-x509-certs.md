---
title: "Testing x509 Certificates"
tags: ["debian","sysadmin","security"]
date: 2011-01-08T09:32:44.189780
---

Navigating the openssl suite of subcommands is time consuming.

Here's my list of frequently used commands for easy reference:

Examining the certificates being offered by a host (replace imap/https with any service, -starttls imap can be eliminated if you are checking a service that provides tls from the start):

    openssl s_client -starttls imap -showcerts -connect chavez.mayfirst.org:imap
    openssl s_client -showcerts -connect chavez.mayfirst.org:https

Examine a certificate signing request:

    openssl req -text -verify -noout -in /path/to/file.csr 

Examine a x509 certificate:

    openssl x509  -noout -text -purpose -in path/to/file.crt
