---
title: "Programming with 6 year olds"
tags: ["debian", "family", "programming"]
date: 2020-03-27T15:45:31.909199
---

I know there is a pedagogically appropriate way to teach 6 year olds how to program and this is not it.

But, it was still fun. And maybe we programmers could use the bark sound more often?

    #!/usr/bin/python3

    import random
    import time
    import subprocess

    while 1:
        answers = [
                "What is your question?",
                "There is no answer.",
                "53",
                "The answer to your question lies within your ear.",
                "Blue berries!",
                "The grasshopper is 20 feet tall.",
                "Elephants are the size of bacteria.",
                "True",
                "False",
                "Yes, no, well, maybe.",
                "I have no idea.",
                "I do not know WHAT you are talking about.",
                "That's rude.",
                "Ask your dad.",
                "I'm sick, please ask someone else.",
                "Maybe you should ask yourself that question.",
                "Could you ask a different question?",
                "You could probably find out by asking the Internet.",
                "Is that question really so important to you?",
                "Can I have a turn asking a question?",
                "How come you get to ask all the questions?",
                "Absolutely not.",
                "You are asking so many questions and haven't even introduced yourself to me.",
                "What other question do you have?",
                "What are you talking about?",
                "I couldn't possibly answer that question.",
                "Can we play a different game?",
                "That makes my brain hurt.",
                "Why are you asking me a question you know the answer to?",
                "Ha ha ha ha ha ha ha ha.",
                "Boo boo butt",
                "Really, ask someone else, I am sick!",
                "Computer, stop talking - you are interrupting me",
                "You are really bad at typing",
                "Why do you keep asking me questions?",
                "Ask someone else already.",
                "The answer is.",
                "You have a bug in your programming."
        ]
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        question = "What is your question? "
        subprocess.call(["espeak", question])
        user_question = input(question)
        subprocess.call(["espeak", "Your question is ... " + user_question])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])

        answer = random.choice(answers)

        print("And the answer is...")
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        subprocess.call(["espeak", "And the answer is"])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])

        time.sleep(2)

        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        print("... the answer is....")
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        subprocess.call(["espeak", "the answer is"])

        time.sleep(2)

        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])
        print(answer)
        subprocess.call(["espeak", answer])
        subprocess.call(["paplay", "/usr/share/sounds/gnome/default/alerts/bark.ogg"])

