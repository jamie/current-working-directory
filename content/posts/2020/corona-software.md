---
title: "Free software in the age of Corona Virus"
tags: ["debian", "covid", "floss"]
date: 2020-03-27T15:51:00.665884
---

There are the printed remarks I made during the [May
First](https://mayfirst.coop/) webinar on free software during the Corona Virus
pandemic, which can be [heard via the
recording](https://mayfirst.coop/en/audio/free-software-during-corona-virus-pandemic/).

Hi everyone, so glad to be here with all of you during these unprecedented times.

I'd like to first make clear that I'm proud of the tools and services that May
First and our movement in general [has made
available](https://mayfirst.coop/en/post/2020/content-alternatives-live-meetings/)
to everyone seeking a way to continue our important organizing work online. And
everyone on this call is a testament to our movement's ability to choose to
build online meeting software that respects our privacy and invests in movement
owned technology.

However, I'd like to address something we can't avoid: capitalism has created
online, full video Internet conferences with as many people as you want where
you can see everyone's faces and share any screen.

Our movement has not figured out how to provide that service in a way that
protects our privacy and ensures such a vital tool remains in the control of
the movement and not in the hands of the corporate world we are fighting so
hard to stop.

As a result of this shortcoming, our movement is, as we speak, investing
heavily - both in terms of money and in terms of learning new tools - to ensure
that these corporate solutions become the default and entrenched methods for
meeting online.

I'd like to address a couple questions: How did we get here? And what can we do
about it?

We could fill an entire conference on the techno/political reasons why video on
the Internet is hard, so I'll just skim the surface: The success of the
Internet is largely due to it's open nature: there is a standard for how
information can be shared, and anyone who builds a program that uses that
standard can inter-operate with everyone else.

From the birth of the corporate Internet, capitalists have been trying to
control the Internet in a way that ensures they will profit the most from it.
And video, which had no standards defined for it when things really took off
back in 1994, has been one of the biggest prizes. As a result, the last 20
years have witnessed battles over what standards will govern how video is
transmitted over the Internet and, despite some recent victories, the ones
promoting free and open tools have largely lost (those of you old enough to
remember that abomination called "flash" will know what I'm talking about).

The result is a fractured Internet when it comes to video. While things have
been recently changing, for most of the last 20 years it has been impossible to
come up with a way to transmit video that will work on all computers and all
cell phones. As a result, only organizations with a lot of capital have been
able to afford the engineering teams needed to write the software for all the
devices.

However there's also a second reason, one that might be even more important:
the movement has not prioritized it. Why? We certainly know how to prioritize
Internet development.

In the late 1980's the left wing Internet organization IGC organized a
left-wing forum system for earlier Internet users.  The Zapatistas prioritized
the use of the Internet in the mid 1990's before anyone had figured it out. The
Global Justice movement build the global Indymedia Network when nobody knew how
to publish our own news. In the early 2000's CiviCRM created a database system
that the movement needed and PTP has further developed it for the Movement as
Powerbase.

However, when it comes to video conferencing we seem to have ceded this work to
the corporatate world.

The next question is: how do we turn this around? That's for us to figure out
together, but I'll start with some ideas:

1. Let's design campaigns that demonstrate to the movement why this is
   important. [No Tech for Ice](https://notechforice.com/) is a great example -
   it shows how big tech has a real and direct impact on our issues. Same with
   anti-surveillance campaigns.

2. Let's remember our values: it's not just about video, it's about ensuring
   that everyone has access, even in low bandwidth environments, where people
   who can't make an international phone call can still connect, where people
   can speak the language they want and get interpreted in real time.

3. Let's use our imaginations to change our culture: we can't just move an "in
   person" meeting to an online format. In fact, maybe our in person formats
   weren't working so well anyway! How do we build a movement culture that
   supports unifying our disperate issue focused groups into a powerful force
   for systemic change? And now, how does the Internet support that agenda?


