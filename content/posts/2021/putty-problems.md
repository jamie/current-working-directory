---
title: "Putty Problems"
date: 2021-09-20T08:27:10-04:00
tags: [ "debian", "sysadmin" ]
---

I upgraded my first servers from buster to bullseye over the weekend and it
went very smoothly, so *big* thank you to all the debian developers who
contributed your labor to the bullseye release!

This morning, however, I hit a snag when the first windows users tried to login.
It seems like a putty bug (see update below).

First, the user received an error related to algorithm selection. I didn't
record the exact error and simply suggested that the user upgrade.

Once the user was running the latest version of putty (0.76), they received a new error:

    Server refused public-key signature despite accepting key!

I turned up debugging on the server and recorded:

    Sep 20 13:10:32 container001 sshd[1647842]: Accepted key RSA SHA256:t3DVS5wZmO7DVwqFc41AvwgS5gx1jDWnR89apGmFpf4 found at /home/XXXXXXXXX/.ssh/authorized_keys:6
    Sep 20 13:10:32 container001 sshd[1647842]: debug1: restore_uid: 0/0
    Sep 20 13:10:32 container001 sshd[1647842]: Postponed publickey for XXXXXXXXX from xxx.xxx.xxx.xxx port 63579 ssh2 [preauth]
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: userauth-request for user XXXXXXXXX service ssh-connection method publickey [preauth]
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: attempt 2 failures 0 [preauth]
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: temporarily_use_uid: 1000/1000 (e=0/0)
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: trying public key file /home/XXXXXXXXX/.ssh/authorized_keys
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: fd 5 clearing O_NONBLOCK
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: /home/XXXXXXXXX/.ssh/authorized_keys:6: matching key found: RSA SHA256:t3DVS5wZmO7DVwqFc41AvwgS5gx1jDWnR89apGmFpf4
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: /home/XXXXXXXXX/.ssh/authorized_keys:6: key options: agent-forwarding port-forwarding pty user-rc x11-forwarding
    Sep 20 13:10:33 container001 sshd[1647842]: Accepted key RSA SHA256:t3DVS5wZmO7DVwqFc41AvwgS5gx1jDWnR89apGmFpf4 found at /home/XXXXXXXXX/.ssh/authorized_keys:6
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: restore_uid: 0/0
    Sep 20 13:10:33 container001 sshd[1647842]: debug1: auth_activate_options: setting new authentication options
    Sep 20 13:10:33 container001 sshd[1647842]: Failed publickey for XXXXXXXXX from xxx.xxx.xxx.xxx port 63579 ssh2: RSA SHA256:t3DVS5wZmO7DVwqFc41AvwgS5gx1jDWnR89apGmFpf4
    Sep 20 13:10:39 container001 sshd[1647514]: debug1: Forked child 1648153.
    Sep 20 13:10:39 container001 sshd[1648153]: debug1: Set /proc/self/oom_score_adj to 0
    Sep 20 13:10:39 container001 sshd[1648153]: debug1: rexec start in 5 out 5 newsock 5 pipe 8 sock 9
    Sep 20 13:10:39 container001 sshd[1648153]: debug1: inetd sockets after dupping: 4, 4

The server log seems to agree with the client returned message: first the key
was accepted, then it was refused.

We re-generated a new key. We turned off the windows firewall. We deleted all
the putty settings via the windows registry and re-set them from scratch.

Nothing seemed to work. Then, another windows user reported no problem (and
that user was running putty version 0.74). So the first user downgraded to 0.74
and everything worked fine.

## Update

Wow, very impressed with the responsiveness of putty devs!

And, who knew that putty is available in debian??

Long story short: putty version 0.76 works on linux and, from what I can tell,
works for everyone except my one user. Maybe it's their provider doing some
filtering?  Maybe a nuance to their version of Windows?
