---
title: "From openbox to sway"
tags: ["debian", "desktop"]
date: 2021-05-11T08:33:26.677701
---

I've been running the [Openbox window
manager](http://openbox.org/wiki/Main_Page) since 2005. That's longer then I've
lived in any one apartment in my entire life!

However, over the years I've been bracing for a change. 

It seems clear the Wayland is the future, although when that future is supposed
to begin is much more hazy.

Really, I've felt a bit like a ping pong ball, from panicking over whether
[Xorg is
abandoned](https://www.phoronix.com/scan.php?page=news_item&px=XServer-Abandonware) (with a follow up from a [X server maintainer](https://ajaxnwnk.blogspot.com/2020/10/on-abandoning-x-server.html))
to anxiously wondering if [literally everything will break the moment I switch
to
Wayland](https://gist.github.com/probonopd/9feb7c20257af5dd915e3a9f2d1f2277).

In fact, I started this blog post over a year ago when I first decided to
switch from the Openbox to [Sway](https://swaywm.org/).

This is my third major attempt to make the change and I *think* it will finally
stick this time.

In retrospect, it would have been more sensible to first switch from openbox to
[i3](https://i3wm.org/) (which is a huge transition) and then from i3 to sway,
but I decided to dive into the deep end with both changes. Note: thanks to a
helpful comment on this blog, I learned that there is
[waybox](https://github.com/wizbright/waybox), an openbox clone for wayland,
which would have been another version of a less drastic change.

So... I'm on debian bullseye so I installed sway and friends (from sid).

Then I copied /etc/sway/config to ~/.config/sway/config.

I start openbox after logging in with `exec startx` so after rebooting, I ran
`exec sway` and to my astonishment sway started. Hooray!

However, I found that `ssh-agent` wasn't running so I couldn't `ssh`  into
any servers. That's kinda a problem.

Launching `ssh-agent` under openbox was buried deep in
`/etc/X11/Xsession.d/90x11-common_ssh-agent` and clearly was not going to
happen via wayland.

Since programs using `ssh-agent` depend on the environment variables
`SSH_AUTH_SOCK` and `SSH_AGENT_PID` being globally available I thought I could
simply run `$(eval ssh-agent)` via my tty terminal before running `exec sway`.

And, that *would have worked*. Except... I like to add my keys via `ssh-add -c`
so that everytime my key is being used I get a ssh-askpass prompt to confirm
the use.

It seems that since `ssh-add` is started before a window manager is running, it
can't run the prompt.

Ok, we can fix this. After searching the web, I came upon a solution of running
ssh-agent via `systemctl --user`:

    # This service myst be started manually after sway
    # starts.
    [Unit]

    Description=OpenSSH private key agent
    IgnoreOnIsolate=true

    [Service]
    Type=forking
    Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
    ExecStart=/usr/bin/ssh-agent -a $SSH_AUTH_SOCK

Then, in my `~/.bashrc` file I have:

    if [ -n WAYLAND_DISPLAY ]; then
      export SSH_AUTH_SOCK=/run/user/1000/ssh-agent.socket
    fi

I think `$SSH_AGENT_PID` is only used by `ssh-agent` to kill itself. Now that
is running via `systemd` - killing it should be do-able without a global
environment variable.
Done? Hardly.

I've been using `impass` (nee `assword`) happily for years but alas it is
tightly integrated with `xdo` and `xclip`.

So... I've switched to `keepassxc` which works out of the box with wayland.

My next challenge was the status bar. Farewell faithful
[tint2](https://gitlab.com/o9000/tint2). One of the reasons I failed on my
first two attempts to switch to Sway was the difficulty of getting the swaybar
to work how I wanted, particularly with nm-applet. Two things allowed me to move forward:

 * [waybar](https://packages.debian.org/bullseye/waybar) was added to Debian.
   Thank you Debian waybar maintainers!
 * I gave up on having nm-applet work the way I'm used to working and resigned
   myself to using `nmtui`. Sigh.

Next up: the waybar clock module doesn't work, but that is [easy enough to work
around](https://github.com/Alexays/Waybar/issues/977).

Replacing my uses of `xclip` with
[wl-clipboard](https://github.com/bugaevc/wl-clipboard) was a little tedious
but really not that difficult.

Getting my screen shot and screen recorder functionality was a bit harder. I
did a lot of searching before I finally found and compiled both [swappy, screen
shot](https://github.com/jtheoof/swappy) and
[wf-recorder](https://github.com/ammen99/wf-recorder).

In the course of all my adventures, I came across the following helpful tips:

 * [awesome wayland](https://github.com/natpen/awesome-wayland#screencasting)
 * [autodidacts blog](https://www.autodidacts.io/switching-to-sway-wayland-from-i3-x11-ubuntu/)
 * [wayland in 20201](https://shibumi.dev/posts/wayland-in-2021/)
 * [Jacobo's experiences (in spanish)](https://www.jacobo.org/sway-en-debian-bullseye/)

---- 

Updates

1. I've installed `libreoffice-gtk3` to ensure libre office runs under wayland
1. I've installed the [latest Gimp via flatpak to get proper wayland support](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/). Update: as of 2022-03-29, it seems that Gimp 2.10.30 in Debian Bullseye works fine with Sway.
1. I've exported `MOZ_ENABLE_WAYLAND` to ensure firefox works properly.
1. I've found that passing -c to my `ssh-add` command to ensure I am prompted for each use of my key seems to cause sway to crash intermittently. Update: This is no longer the case. Not sure why.
1. I learned about a [work around to get screen sharing to work in zoom](https://hugo.barrera.io/journal/2020/06/14/zoom-screensharing-on-archlinux/). Somehow this actually works. Amazing. Unfortunately, though, sharing your screen in the context of a Zoom meeting pins your screen for all participants. So, sharing your desktop through your camera really doesn't cut it. I finally landed an an obvious work around: Install chromium (which runs under X11); Install the [the chrome zoom redirector extension](https://chrome.google.com/webstore/detail/zoom-redirector/fmaeeiocbalinknpdkjjfogehkdcbkcd) (update: This extension is no longer available); Open zoom links in chromium; You can now share other chromium windows. Not the full desktop or any wayland window, but if you only need to share a web browser window, you are set; For the record, Zoom links normally are in the format: https://us04web.zoom.us/j/123456. If you want to force the use of the web client, just change the "j" to "wc": https://us04web.zoom.us/wc/123456.
1. Speaking of screen sharing - when using Firefox, I can only share Xwayland screens. Firefox is running under wayland so I can't share it. Chromium is running under xwayland, so I have to use Chromium when screen sharing.
1. Wait, scratch that about screen sharing in Firefox. I've installed [xdg-desktop-portal-wlr](https://github.com/emersion/xdg-desktop-portal-wlr), added `export XDG_CURRENT_DESKTOP=sway` and `export XDG_SESSION_TYPE=wayland` to my `.bashrc`, and after hours of frustration, realize that I needed to [configured firejail to allow it](https://github.com/netblue30/firejail/issues/3872) so that I can share my entire screen in Firefox. It doesn't yet support sharing a specific window, so I still have to keep chromium around for that (and Chromium can only share xwayland windows). Sigh. Oh, one more thing about Firefox: the option to choose what to share doesn't have "Entire Screen" as an option, you are just supposed to know that you should choose "Use operating system settings".
1. I still am getting weekly crashes. Some of them I've fixed by switching to wayland friendly versions (e.g. Libre Office and Gimp) but others I haven't yet tracked down. Update: No longer getting weekly crashes. Very stable.
1. My keyboard does not have an altgr key, so even though I have selected the "English (US) - English (intl., with AltGr dead keys)" I can't get accent marks. I went down a rabbit hole of trying to re-map the Alt key to the right of my space bar but it all seemed too complicated. So - I found a way easier approach. In my `~/.config/sway/config` file I have: `bindsym Mod4+e exec wtype "é"`. I have repeated that line for the main accent marks I need.
1. Due to a [Firefox Bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1590661), when I share my desktop or mic or camera, the sharing indicator expands like a normal tiling window instead of remaining a tiny little box on each desktop reminding me that I'm sharing something. I'd prefer to have it be a tiny little box, but since I can't figure that out, I've disabled it by typing `about:config` in the Firefox location window, searching for `privacy.webrtc.legacyGlobalIndicator` and setting it to **False**. The [reddit thread](https://www.reddit.com/r/swaywm/comments/iva00z/hi_help_needed_why_is_mic_indicator_appearing_as/) also suggested finding `privacy.webrtc.hideGlobalIndicator` and setting it to True, but that setting doesn't seem to be available and setting the first one alone seems to do the trick.
1. Oh, one more environment variable to set: `GDK_BACKEND=wayland,x11`. First I just set it to wayland to get gtk3 apps to use wayland (like gajim). But that broke electron apps (like signal) which notice that variable but don't have a way to display via wayland (at least [not yet](https://github.com/signalapp/Signal-Desktop/issues/3411)). Setting it to "wayland,x11" shows the priority. Thank you [ubuntu community](https://discourse.ubuntu.com/t/gtk-backend-selection-or-why-gtk-cannot-open-display-0/17657).
1. I've also finally consolidated where my environment variables go. I've added them all to `~/.config/sway/env`. That seems like an official sway place to put them, but sway doesn't pay any attention to them. So I start sway via my own bash script which sources that file via `[ -f "$HOME/.config/sway/env" ] && . "$HOME/.config/sway/env"` before `exec`'ing sway.


