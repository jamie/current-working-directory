---
title: "Google and Bitly"
date: 2021-07-18T13:25:59-04:00
tags: [ "debian", "sysadmin","email" ]
---

It seems I'm the only person on the Internet who didn't know [sending email to
Google with bit.ly links will tank your
deliverability](https://duckduckgo.com/?q=google+bit.ly+deliverability&t=h_&ia=web).
To my credit, I've been answering deliverability support questions for 16 years
and this has never come up. 

Until last week.

For some reason, at [May First](https://mayfirst.coop/) we suddenly had about
three percent of our email to Google deferred with the ominous sounding:

> "Our system has detected that this message is 421-4.7.0 suspicious due to the
> nature of the content and/or the links within."

The quantity of email that accounts for just three percent of mail to Google is
high, and caused all kinds of monitoring alarms to go off, putting us into a
bit of panic.

Eventually we realized all but one of the email messages had bit.ly links. 

I'm still not sure whether this issue was caused by a weird and coincidental
spike in users sending bit.ly links to Google. Or whether some subtle change in
the Google algorithm is responsible. Or some change in our IP address
reputation placed greater emphasis on bit.ly links. 

In the end it doesn't really matter - the real point is that until we disrupt
this growing monopoly we will all be at the mercy of Google and their
algorithms for email deliverability (and much, much more).



