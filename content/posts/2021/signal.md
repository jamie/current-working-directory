---
title: "So... is Signal good or bad?"
tags: ["debian", "im"]
date: 2021-01-29T15:37:11.056667
---

After [Facebook updated their Whatsapp privacy
policy](https://arstechnica.com/tech-policy/2021/01/whatsapp-users-must-share-their-data-with-facebook-or-stop-using-the-app/),
and a certain rich capitalist who doesn't like Facebook for reasons different
then mine told the world to use Signal, Signal's downloads [went up by
4,200%](https://www.businessinsider.com/whatsapp-facebook-data-signal-download-telegram-encrypted-messaging-2021-1?op=1).

As often happens when something becomes popular, the criticisms start to fly!

For the record, I currently think promoting [Signal](https://signal.org/) is
an important tactical strategy for the left. \[I also think we should promote
and install federated chat apps like [conversations](https://conversations.im/)
and [element](https://element.io/) and [delta chat](https://delta.chat/en/)
whereever it is possible.\]

Here are some of the main criticisms I hear that I think are a distraction:

 * **Signal forces you to use the Google Play store and Google Services**: This
   isn't true any more. You can download [the apk
   directly](https://signal.org/android/apk/) on a phone without any Google
   services and it works great. The app will alert you to new versions.

   Don't get me wrong: the Signal network still depends on Google services.
   And, we *should* be avoiding corporate technology and building our own
   infrastructure. However, in practice, Signal is an alternative to Whatsapp
   and Telegram - which not only use the same corporate services but are
   proprietary technology that is fully owned by powerful tech giants. Signal
   is still a non-profit organization with a vastly different mission.

 * **Signal's approach to privacy isn't perfect** (the most common variation on
   this theme is that a state actor could monitor your outgoing communications
   and the incoming communications of the person you are communicating with and
   prove that you are communicating with each other).

   This criticism missed what makes Signal so important. The beauty of Signal
   is that it addresses the "woops!" moment most privacy activists had when
   Snowden's data trove become public: it provides *mass* privacy to stop
   *mass* surveillance. Prior to 2013, most tech/privacy activists were focused
   on the "targeted" individual approach to privacy, working hard to make sure
   our tools were as absolutely perfect as possible for the tiny percentage of
   people who know they are under surveillance. Very little effort went into
   getting them adopted on a mass scale.

   Criticizing Signal for not providing perfect privacy misses that fact that
   these things often are trade offs.

   This trade-off also applies to the first point - dependency on Google
   services makes installation far easier for suporting millions of people.

Here are some criticisms that I think are nuanced:

 * **Signal is a centralized app**: this criticism often includes examples of
   Moxie (Signal's founder) refusing and actively discouraging attempts by
   others to write software that interacts with Signal. 

   Signal is free software, which is a major improvement over most corporate
   technology. But since it's entirely controlled by one entity, it can be
   shutdown in a heart beat. And, if Signal changes direction, we cannot easily
   take the work we have all invested in learning signal and create our own
   version that reflects our values.

   This problem is in contrast to federated systems like email - where anyone
   can run their own email server and apply their own policies. If one email
   serveer is shutdown, you can move to another.

   I agree with this critique, but I think it's nuanced because of the trade
   offs. Having full control over the entire network and all the software
   provides a level of reliability and consistency that would not be possible
   with a federated protocol. And, we already have three different, fully
   viable federated chat protocols (see above). I'd rather have Signal be
   Signal and invest our energy on a federated chat system via the existing,
   well-developed alternatives.

   This opinion is tactical - and could change at any moment. I think there
   will come a time when we are going to tell the world to move from Signal to
   the best available federated protocol. But I'm not convinced we have a
   robust enough federated chat infrastructure to support that move.

 * **Signal forces you to use your phone number as an identifier**: You can't
   get a Signal account without a phone number. And you generally can't get a
   phone number without revealing some aspect of your identity. That makes
   staying anoymous very difficult. There are reports of a new Signal feature
   making it possible to avoid revealing your phone number when communicating
   with others, but you would still need a phone number to get an account
   because a SMS or phone call confirmation is required.

 * **Signal isn't getting ahead of the curve on abuse**: There's an interesting
   [piece informed by former Signal staff
   people](https://www.platformer.news/p/-the-battle-inside-signal) about the
   management's resistance to getting ahead of the curve when it comes to
   abuse. How would signal respond to reports of harrassment? What would signal
   do if it recognized facsists movements organizing on its platform? Any mass
   platform that is not planning for abuse is going to be in big trouble very
   soon.

These last two are not exactly two sides of the same coin, but they are
related. How Signal manages to balance privacy and protection from abuse will
be the real test as to whether promoting Signal continues to be a useful
strategy for the left.

