---
title: "The problem with Richard Stallman is not about free speech"
tags: ["debian", "rms", "oppression"]
date: 2021-03-29T08:50:40.937563
---

Free speech and censorship are critically important issues. And, using them to
defend Richard Stallman's return to the Free Software Foundation (FSF)
board is just plain wrong.

Richard Stallman resigned from the Board in 2019 after he sent an email in
defense of Marvin Minsky (Minsky is accused of raping one of Jeffreys Epstein's
victims). 

Stallman's fateful email, however, is just *one piece of the reason for why he
should not be on the board.* The [full
story](https://selamjie.medium.com/remove-richard-stallman-appendix-a-a7e41e784f88)
is about his history of abuse toward women and is extensive.

On March 21st, 2021, Stallman announced [he is back on the
board](https://www.theregister.com/2021/03/22/richard_stallman_back_on_fsf_board/).

There are profound reasons why any movement interested in equitable and open
participation would want to publicly distance themselves from Stallman.
However, the long form defenses of Stallman, including [a note from Nadine
Strossen, the former executive director of the ACLU, quoted in this
defense](https://www.wetheweb.org/post/cancel-we-the-web), persist.

Many of the arguments defending Richard Stallman (including the one from
Strossen) are grounded in a belief that Stallman is being punished for his
unpopular political views, which deserve to be defended on the grounds of
freedom of expression.

That's wrong. 

Stallman should be kicked off the board because he has a long history of
abusing his position to hit on women, which, when combined with his public
opinions on under-age sex and his defense of Minsky, send a strong signal that
the FSF does not care about the participation of women.

Being on a board of directors is a privilege, not a right. Being removed from a
board is not a punnishment. And being criticized and removed from a board
because your behavior and public statements are an obstacle to building an
inclusive and equitable movement is what every board should strive to do.

If we are going to make this an issue about free expression, it should be about
all the political expression lost to the free software movement because
Stallman's unequal behavior toward women excluded an enormous number of
talented individuals.
