---
title: "How to Meet Online with Simultaneous Interpretation"
tags: ["debian", "video", "language-justice", "mayfirst"]
date: 2021-06-22T09:14:50.537525
---

[May First Movement Technology](https://mayfirst.coop/) has been running a
public [Jitsi Meet](https://jitsi.org/jitsi-meet/) instance since well before
the pandemic to support Internet-based, video meetings for folks who don't want
to rely on corporate and proprietary infrastructure.

However (until this week - see below), we haven't been using it for our own
meetings for one main reason: simultaneous interpretation. We're an
international organization with roots in the US and Mexico and we are committed
to building a bi-national leadership with a movement strategy that recongizes
the symbolic and practical disaster of the US/Mexico border.

As a result, we simply can't hold a meeting without simultaneous interpretation
between english and spanish.

Up to now, we've worked out [a creative way to have mumble meetings with
simultaneous
interpretation](https://support.mayfirst.org/wiki/mumble-interpreter-setup). In
short: we have a room for interpretation. If you move into the interpretation
room, you hear the interpreter. If you move into the main room, you hear the
live voices of the participants. You can switch between rooms as needed. This
approach is rock solid, and we benefit from mumble's excellent performance in
low bandwidth situations and the availability of mumble clients on both Android
and iPhones.

However, there are limitations, which include:

 * You can't hear both the live voices and the interpretation at the same time:
   it's one or the other. If you are in a face-to-face meeting and receiving
   interpretation via headphones, you can see the person talking and even
   remove the headphones from one ear to get a sense of the tone and emotions
   of the speaker. Not with mumble. In fact, you can't even tell who is
   speaking.

 * Two chat rooms: If you chat to the live group, it's only seen by the live
   group. If you chat with the interpretation group, it's only seen by the
   interpretation group.

 * No video in mumble: well, some people consider this a positive. I'll leave
   it at that.

After years of reviewing the many dead-end threads and issue requests around
simultaneous interpretation on the Jitsi boards (and the Big Blue Button boards
for that matter) I finally came across [the
thread](https://community.jitsi.org/t/adjust-volume-levels-via-javascript/100701)
that led to the [pull request](https://github.com/jitsi/jitsi-meet/pull/9322)
that changed everything.

With the ability to control local volume via the Jitsi Meet API, I was able to
pull together a very small amount of code to produce [Jitsi Simultaneous
Interpretation (JSI)](https://gitlab.com/mfmt/jsi) - a way to run your Jitsi
Meet server with an interpretation slider at the top allowing you to set the
volume of the interpreter at any time during the meeting.

It's still not perfect - the main problem is that you can't use any of the
Jitsi Meet apps - so it runs well on most desktops, but when it comes to cell
phones, it only runs (in browser) on modern android phones.
