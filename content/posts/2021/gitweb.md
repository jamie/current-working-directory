---
title: "Anyone still using gitweb?"
date: 2021-08-18T08:27:10-04:00
tags: [ "debian", "git", "sysadmin" ]
---

It seems like the self-hosting git world has all moved to
[gitlab](https://about.gitlab.com/install/) or [gitea](https://gitea.com/). 

For a number of reasons not worth enumerating, I'm still running
[gitolite](https://packages.debian.org/search?keywords=gitolite3) and recently decided I
wanted to checkout my code via https using
[gitweb](https://packages.debian.org/search?keywords=gitweb).

I got through most of the installation and configuration without trouble (I
could browse via the web and see all my repositories). But, when I tried to
`git clone` using the https address I got a fatal "not found" error.

It seems that gitweb, out of the box, allows for easy web-browsing of git
repositories but needs some extra work if you want to clone over https.
Specifically, you need to use `git-http-backend`.

The `git-http-backend` man page is very useful, but assumes you are accessing
your repos via `https://example.org/git` instead of simply
`https://git.exmple.org`.

These lines are my variation to the suggested apache configuration lines provided by 
`man git-http-backend`.

They differ by:

 * allowing for web access without specifying a subdirectory 
 * using the debian `/usr/lib/git-core` path instead of `/usr/libexec/git-core`
 * removing `git-receive-pack` since I only plan to clone and don't plan to push
   back to this repo.

```
DocumentRoot /usr/share/gitweb
<Directory /usr/share/gitweb>
  Options +FollowSymLinks +ExecCGI
  AddHandler cgi-script .cgi
  Require all granted
</Directory>
<Directory /usr/lib/git-core>
  Require all granted
</Directory>
SetEnv GIT_PROJECT_ROOT /var/lib/git
AliasMatch ^/(.*/objects/[0-9a-f]{2}/[0-9a-f]{38})$          /var/lib/git/$1
AliasMatch ^/(.*/objects/pack/pack-[0-9a-f]{40}.(pack|idx))$ /var/lib/git/$1
Alias /static /usr/share/gitweb/static
ScriptAliasMatch \
  "(?x)^/(.*/(HEAD | \
    info/refs | \
    objects/info/[^/]+ | \
    git-upload-pack))$" \
  /usr/lib/git-core/git-http-backend/$1
ScriptAlias / /usr/share/gitweb/gitweb.cgi/
```

The main trick is to direct some requests to `apache2`, some requests to
`/usr/lib/git-core/git-http-backend`, and everything else to `gitweb.cgi`.
