---
title: "From Ikiwiki to Hugo"
date: 2021-07-16T08:27:10-04:00
tags: [ "debian", "cms" ]
---

Back in the days of Etch, I converted this blog from Drupal to
[ikiwiki](https://ikiwiki.info). I remember being very excited about this brand
new concept of static web sites derived from content stored in a version
control system.

And now over a decade later I've moved to [hugo](https://gohugo.io/). 

I feel some loyalty to ikiwiki and Joey Hess for opening my eyes to the static
web site concept. But ultimately I grew tired of splitting my time and energy
between learning ikiwiki and hugo, which has been my tool of choice for new
projects. When I started getting strange emails that I suspect had something to
do with spammers filling out ikiwiki's commenting registration system, I choose
to invest my time in switching to hugo over debugging and really understanding
how ikiwiki handles user registration.

I carefully reviewed [anarcat's blog on converting from ikiwiki to
hugo](https://anarc.at/services/wiki/ikiwiki-hugo-conversion/) and learned
about a lot of ikiwiki features I am not using. Wow, it's times like these that
I'm glad I keep it really simple. Based on the various ikiwiki2hugo python
scripts I studied, I eventually wrote a [far simpler
one](https://gitlab.com/jamie/current-working-directory/-/tree/main/tools)
tailored to my needs.

Also, in what could only be called a desperate act of procrastination combined
with a touch of self-hatred (it's been a rough week) I rejected all the
commenting options available to me and choose to [implement my own in
PHP](https://gitlab.com/jamie/commenter). 

What?!?!  Why would anyone do such a thing? 

I refer you to my previous sentence about desperate procrastination. And
also... I know it's fashionable to hate PHP, but honestly as the first
programming language I learned, there is something comforting and familiar
about it. And, on a more objective level, I can deploy it easily to just about
any hosting provider in the world. I don't have to maintain a unicorn service
or a nodejs service and make special configuration entries in my web
configuration. All I have to do is upload the php files and I'm done. 

Well, I'm sure I'll regret this decision.

Special thanks to [Alexander Bilz](https://github.com/lxndrblz) for the
[anatole hugo theme](https://github.com/lxndrblz/anatole/). I choose it via a
nearly random click to avoid the rabbit hole of choosing a theme. And, by luck,
it has turned out quite well. I only had to override the commento partial theme
page to hijack it for my own commenting system's use.
