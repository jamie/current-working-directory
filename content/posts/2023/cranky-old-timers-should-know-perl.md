---
title: "Cranky old timers should know perl"
date: 2023-05-17T08:27:10-04:00
tags: [ "debian", "sysadmin", "email" ]
---

I act like an old timer (I've been around linux for 25 years and I'm cranky
about new tech that is not easily maintained and upgraded) yet somehow I don't
know perl. How did that happen?

I discovered this state when I decided to move from the heroically packaged yet
[seemingly upstream
un-maintained](https://github.com/trusteddomainproject/OpenDMARC/issues/240)
[opendmarc](https://github.com/trusteddomainproject/OpenDMARC) package to
[authentication_milter](https://github.com/fastmail/authentication_milter/).

It's written in perl. And, alas, [not in
debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1036235).

How hard could this be?

The instructions for installing seemed pretty straight forward: `cpanm Mail::Milter::Authentication`.

Wah. I'm glad I tried this out on a test virtual machine. It took forever! It
ran tests! It compiled things! And, it installed a bunch of perl modules
already packaged in Debian.

I don't think I want to add this command to my ansible playbook.

Next I spent an inordinate amount of time trying to figure out how to list the
dependencies of a given CPAN module. I was looking for something like `cpanm
--list-dependencies Mail::Milter::Authentication` but eventually ended up [writing
a perl script](https://metacpan.org/pod/CPAN::FindDependencies) that output
perl code, inserting a "use " before each dependency and a semicolon and line
break after them. Then, I could execute that script on a clean debian
installation and see which perl modules I needed. For each error, I checked for
the module in Debian (and installed it) or kept a list of modules I would have
to build (and commented out the line).

Once I had a list of modules to build, I used the handy `cpan2deb` command. It
took some creative ordering but eventually I got it right. Since I will surely
forget how to do this when it's time to upgrade, I [wrote a
script](https://code.mayfirst.org/mfmt/seed/-/blob/master/roles/tests/files/build-authentication-milter-debian-packages).

In total it took me several days to figure this all out, so I once again find
myself very appreciative of all the debian packagers out there - particularly
the perl ones!!

And also... if I did this all wrong and there is an easier way I would love to
hear about it in the comments.



