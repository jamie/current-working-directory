---
title: "Enough about the AI Apocalypse Already"
date: 2023-06-01T08:27:10-04:00
tags: [ "debian", "oppression", "AI"  ]
---

After watching [Democracy Now's](https://democracynow.org) [segment on
artificial
intelligence](https://www.democracynow.org/2023/6/1/ai_bengio_petty_tegmark) I
started to wonder - am I out of step on this topic?

When people claim artificial intelligence will surpass human intelligence and
thus threaten humanity with extinction, they seem to be referring specifically
to advances made with large language models.

As I understand them, large language models are probability machines that have
ingested massive amounts of text scraped from the Internet. They answer
questions based on the probability of one series of words (their answer)
following another series of words (the question).

It seems like a stretch to call this intelligence, but if we accept that
definition then it follows that this kind of intelligence is nothing remotely
like human intelligence, which makes the claim that it will surpass human
intelligence confusing. Hasn't this kind of machine learning surpassed us
decades ago? 

Or when we say "surpass" does that simply refer to fooling people into thinking
an AI machine is a human via conversation? That is an important milestone, but
I'm not ready to accept the [turing
test](https://en.wikipedia.org/wiki/Turing_test) as proof of equal
intelligence.

Furthermore, large language models "hallucinate" and also reflect the biases of
their training data. The word "hallucinate" seems like a euphemism, as if it
could be corrected with the right medication when in fact it seems hard to
avoid when your strategy is to correlate words based on probability. But even
if you could solve the "here is a completely wrong answer presented with
sociopathic confidence" problem, reflecting the biases of your data sources
seems fairly intractable. *In what world would a system with built-in bias be
considered on the brink of surpassing human intelligence?*

The danger from LLMs seems to be their ability to convince people that their
answers are correct, including their patently wrong and/or biased answers.

Why do people think they are giving correct answers? Oh right... 
terrifying right wing billionaires (with [terrifying
agendas](https://www.longtermism-hub.com/) have been [claiming AI will exceed
human intelligence and threaten
humanity](https://futureoflife.org/open-letter/pause-giant-ai-experiments/) and
every time they [sign a hyperbolic
statement](https://www.safe.ai/statement-on-ai-risk#open-letter) they get front
page mainstream coverage. And even [progressive news outlets are spreading this
narrative](https://www.democracynow.org/2023/6/1/ai_bengio_petty_tegmark) with
minimal space for contrary opinions (thank you Tawana Petty from the
[Algorithmic Justice League](https://www.ajl.org/) for providing the only
glimpse of reason in the segment). 

The belief that artificial intelligence is or will soon become omnipotent has
real world harms today: specifically it creates the misperception that current
LLMs are accurate, which paves the way for greater adoption among police
forces, social service agencies, medical facilities and other places where
racial and economic biases have life and death consequences.

When the CEO of OpenAI calls the technology dangerous and in need of
regulation, he gets both free advertising promoting the power and supposed
accuracy of his product and the possibility of freezing further developments in
the field that might challenge OpenAI's current dominance. 

The real threat to humanity is not AI, it's massive inequality and the use of
tactics ranging from mundane bureaucracy to deadly force and incarceration to
segregate the affluent from the growing number of people unable to make ends
meet. We have spent decades training bureaucrats, judges and cops to
robotically follow biased laws to maintain this order without compassion or
empathy. Replacing them with AI would be make things worse and should be
stopped. But, let's be clear, the narrative that AI is poised to surpass human
intelligence and make humanity extinct is a dangerous distraction that runs
counter to [a much more important story about "the very real and very present
exploitative practices of the [companies building AI], who are rapidly
centralizing power and increasing social
inequities."](https://www.dair-institute.org/blog/letter-statement-March2023).

Maybe we should talk about *that* instead?
