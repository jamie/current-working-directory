---
title: "Electron doesn't like negative layout coordinates"
date: 2023-04-20T08:27:10-04:00
tags: [ "debian" ]
---

I got a second external monitor. Overkill? Probably, but I like having a
dedicated space to instant messaging (now left monitor) and also a dedicated
space for a web browser (right monitor).

But, when I moved signal-desktop to the left monitor, clicks stopped working. I
moved it back to my laptop screen, clicks started working. Other apps (like
gajim) worked fine. A real mystery.

I spent a lot of time on the wrong thing. I turned this monitor into portrait
mode. Maybe signal doesn't like portrait mode? Nope.

Maybe signal doesn't think it has enough horizontal space? Nope.

Maybe signal suddently doesn't like being on an external monitor? Nope.

Maybe signal will output something useful if I start it via the terminal? Nope
(but that was a real distraction).

Then, I discovered that mattermost desktop behaves the same way. A clue! So,
now I know it's an electron app limitation, not a signal limitation.

Finally I hit on the problem: Via my sway desktop, I set my laptop screen to
the x,y coordinates: 0,0 (since it's in the middle). I set the left monitor to
negative coordinates so it would appear on the left. Well, electron does not
like that, not sure why.

Now, my left monitor occupies 0,0 and the rest are adjusted to that center.

I originally used negative coordinates so when I unplugged my monitors, my
laptop would still be 0,0 and display all desktops properly. Fortunately, sway
magically figures that all out even when the "center" shifts by unplugging the
monitors. Hooray for sway.
