---
tags: ["fsa2008","mayfirst"]
title: "Americas Social Forum Inaugural Post"
date: 2008-01-08T09:32:44.149780
---

I'm in the LA airport awaiting my flight to Houston and from there on to
Guatemala City for the third annual [Americas Social
Forum](http://forosocialamericas.org). 

I can't think of a more political intense and overhelming time to be talking
politics in Latin America. We have the [crisis in
Bolivia](/posts/2008/support-for-bolivia/) and the [global economic
crisis](http://lists.portside.org/cgi-bin/listserv/wa?A2=ind0810a&L=PORTSIDE&P=2075).
We have country after country electing leftists governments - with El Salvador
poised to be the next early in 2009, taking the trend from South America into Central America. 

And, we have a burgeoning global communications network with an immense
potential for international organizing.

I'll be posting here over the next 10 days about my experiences volunteering, participating, and [organizing May First/People Link's own workshop contribution](http://mayfirst.org/fsa). 

Stay tuned!



