---
title: "SMTP Problems, old and new"
date: 2008-01-08T09:32:44.149780
---

I received 100 [back
scatter](http://en.wikipedia.org/wiki/Backscatter#Backscatter_of_email_spam)
messages in my inbox today. As someone who receives mail for dozens of system
email messages (like root@ and www-data@), this number is higher than usual,
but not something so odd that it concerns me. Upon closer examination, however,
I noticed two things different with these messages.

Normally, I get back scatter of two varieties: spammer emails joe@example.org.
Joe is a legit user with a auto responder - so example.org accepts the spam,
and then generates the auto response, which I receive. The second variety:
spammer emails announce@lists.example.org. Since announce is a legit list, it
is accepted by the mail server, handed off to the list server program, that
programs notices that I'm not allowed to post, and generates an email warning
me to that effect. These are some of the reasons SMTP is broken, but given the
current state of SMTP, I don't have any good ideas on how to prevent this type
of back scatter.

In contrast, the messages today were bounced with mailbox full and mailbox
unavailable messages. These type of messages should not generate back scatter.
It's 2008 and most mail servers are configured to reject messages for mailboxes
that don't exist or are over quota, relying on the sending server to generate
the bounce back. Since spammers usually email directly to the receiving
servers, they don't bother generating the bounce back.

So why did I get these types of bounce backs? I carefully examined three
messges and found one open relay (that's an old problem) and two MX servers
that accepted delivery to mailboxes and then turned around and generated an over quota message (in one case) and a generic delivery failed (in the other case).

The open relay problem is an age old problem - nothing new here. I suspect the
other problems are due to people setting up much more elaborate mail systems,
ironically, in an attemp to fight spam. The strategy is to setup a server
(which will act as the offical MX server that is the first to receive the
message) whose function is to just scan for spam and viruses and then pass the
message on to the server that will deliver the email message. The problem is
when that first line server does not know how to reject messages for mailboxes
that don't exist or are over quota. Instead, they accept all mail. If the
server setup to deliver the messages can't deliver the message - they have no
way of communicating this rejection to the original sending server - so they
either have to generate an email bounce, or the MX front line server has to
generate an email bounce.

The final element I noticed was that the original emails being sent had forged
Received lines.

Here's one example:

		Received: from GMSEXC03.CORP.DS.GEMPLUS.COM ([10.2.158.51]) by
						GMSEXC05.CORP.DS.GEMPLUS.COM with Microsoft SMTPSVC(6.0.3790.1830);  Mon,
						24 Mar 2008 21:41:15 +0100
		Received: from SINEXC01.CORP.DS.GEMPLUS.COM ([10.150.204.51]) by
						GMSEXC03.CORP.DS.GEMPLUS.COM with Microsoft SMTPSVC(6.0.3790.1830);  Mon,
						24 Mar 2008 21:41:14 +0100
		Received: from mars.gemplus.com ([203.126.114.26]) by
						SINEXC01.CORP.DS.GEMPLUS.COM with Microsoft SMTPSVC(6.0.3790.1830);  Tue,
						25 Mar 2008 04:41:14 +0800
		Received: from 83-70-211-36.b-ras1.chf.cork.eircom.net
						(83-70-211-36.b-ras1.chf.cork.eircom.net [83.70.211.36]) by
						mars.gemplus.com (Postfix) with ESMTP id 1E3B62EACDF; Mon, 24 Mar 2008
						20:41:05 +0000 (GMT)
		Received: from [83.70.211.36] by leslie.mayfirst.org; Mon, 24 Mar 2008 20:41:05
						+0000

You'll notice that the first received line (the one on the bottom) says that it
was received by leslie.mayfirst.org from the IP address 83.70.211.36. The
second recieved line says that it was received by
83-70-211-36.b-ras1.chf.cork.eircom.net *also* from the IP address
83.70.211.36. In other words, the first one, claiming to be recived by the
mayfirst.org server, is clearly forged since the next line claims to have
received the message from the same IP. All of this makes me very nervous about
spam detection systems that block based on the original sending IP. Are they
smart enough to see forgeries like this? This can be a very effective Denial of
Service attack - scattering spam throughout the world claiming to have been
originally received by the IP address of the mail system you want to target.
