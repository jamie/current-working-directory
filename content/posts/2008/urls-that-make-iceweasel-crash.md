---
title: "URLs That Make Iceweasel Crash"
date: 2008-01-08T09:32:44.149780
---

This is a work in progress. If you are running Iceweasel don't click on these
links unless you don't mind a browser crash.

		Iceweasel version: 2.0.0.12-1
		URL: http://howtoforge.com/ipp_based_print_server_cups_p2
		Date: 2008-03-19
