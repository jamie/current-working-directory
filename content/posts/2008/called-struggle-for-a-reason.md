---
tags: ["mayfirst"]
title: "It's called struggle for a reason"
date: 2008-01-08T09:32:44.137780
---

The NY Times ran an
[article](http://www.nytimes.com/2008/06/15/fashion/15green.html) last weekend
about just how darn difficult it is to be an environmentalist. You switch from
cardboard milk cartoons to recycled glass bottles to reduce waste and then,
before you know it, somebody is telling you that all that glass recycling
wastes energy. Sheesh!

The article is a nice illustration of the obstacles to building a political
movement around a topic that has become marketable. Once environmentalism
became an option at mainstream stores, something significant changed. We
stopped seeing environmentalism as a complex problem requiring lots of
discussion and, more often than not, messy solutions requiring difficult
compromises. Suddenly, environmentalism became easy choice: buy product A
instead of product B. 

The good news is that a concept of environmentalism reached a huge new audience
because it was made easy. The bad news is that reaching a new audience didn't
solve the underlying problems. Additionally, the transformative impact of
engaging in environmental struggle, the most important and difficult aspect of
any political movement, was lost. Now the environmental justice movement has to
fight both environmental destruction and a mainstream perception of what
environmentalism is.

Technology is on a similar trajectory. How do we engage people in a difficult
discussion about the politics of our work on the Internet when even the left
sees technology as choosing product A versus product B? 

Consider some of the most pressing political issues facing the Internet: using
free software and using OpenPGP and the web of trust to sign and encrypt our
messages.

Making a commitment to free software *is* hard. It's not just hard because you
may need to learn how to use a new program. It's hard because you have a new
relationship to software development - one that has higher expectations of you.
One that expects you to engage with the developers by posting bug reports,
upgrading so developers can spend less time supporting old versions, and
contributing answers to other users. It's much harder for developers as well -
demanding a much more engaged relationship with potentially thousands of users.
The transformative potential of free software isn't just to change the licenses
of the software we use, but to transform the relationships of the developers
and users of the software - from a capitalist exchange to a real community of
collaboration.

Using OpenPGP to encrypt and sign our messages *does* make our lives more
difficult. Not only because you have to learn new buttons to push in order to
read and write email, but because good security requires us to fundamentally
understand what we are doing. Security culture demands both adherence to
protocol and, more importantly, a fundamental understanding of the protocol.

The mainstream ([x509](http://en.wikipedia.org/wiki/X509)) protocol for
security is significantly easier to understand: here's a list of entities (in
practice mostly corporations) that you should trust because we tell you to.
Easy. You just trust them. The OpenPGP model allows you to build a network of
trusted individuals. Much more complicated.

These things don't come easy. They never will. The real question is: why do we
expect them to be easy?

