---
title: "Welcome to my new blogging software"
date: 2008-01-08T09:32:44.149780
---

Times are a-changing and so is my blog software. May First/People Link is about
to launch its new blog site, which means we'll no longer being blogging on the
main [www.mayfirst.org](http://www.mayfirst.org) site.

I will be re-publishing my blog on the about to be launched [May First/People
Link blogging](http://blogs.dev.mayfirst.org) site. However, I decided to take
advantage of this opportunity to play around with
[ikiwiki](http://ikiwiki.info). It's particularly interesting because it stores
my blogs in [git](http://git.or.cz) - a revision control system often used for
software projects. That means I get to write my blogs in a normal text editor
rather than a web form.

I did my best to bring over my old blogs - including comments! The order is a
bit off - if you look at the [[posts]] page they are not listed in
chronological order. I decided I could live with it. I hope you can too!

