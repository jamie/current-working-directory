---
tags: ["mayfirst","amc2008"]
title: "Media Sanctuary on the future of censorship"
date: 2008-01-08T09:32:44.149780
---

Sunday morning at 10:00 am in Room J you'll find me at Steve Pierce's workshop
on censorship.

UPDATE: I can't believe I missed this workshop :(. Too many amazing workshops at the same time!

Be sure to checkout the [Media Sanctuary](http://mediasanctuary.org/) and the
story about [Wafaa Bilal's "Virtual
Jihadi"](http://mediasanctuary.org/node/120).

