---
title: "Ikiwiki + Git + Debian Etch"
date: 2008-01-08T09:32:44.149780
---

After a lot of head scratching, I seem to have [ikiwiki](http://ikiwiki.info)
up and running using a [git](http://git.or.cz) repository - all running on
[Debian](http://debian.org) Etch.

I'm using the version of ikiwiki that ships with Etch (1.33.3), which doesn't
seem to have some of the nice features available in the current version
(2.32.3) such as the ikiwiki-makerepo command. In addition, I am using git
1.4.4.4-2 (also on Etch).

Nevertheless, I was able to make it work. I did the following somewhat
cumbersome steps. Note: if you are using a more recent version of ikiwiki, you
probably shouldn't follow these steps.

## Creating the git repositories ##

1. Create a bare repository:

        mkdir iw-repo
        cd iw-repo
        git --bare init-db

2. Copy the sample blogs directory into a temporary directory

        cp -rp /usr/share/doc/ikiwiki/examples/blog/ temp

3. Initialize the temp directory as a git repository

        cd temp
        git init-db

4. Check in the files

        git add *
        git commit -a

5. Push to the bare repo

        git push ../iw-repo

6. Delete your temp directory

        cd ..
        rm -rf temp

7. Create at working directory from the bare repo

        git clone iw-repo iw-working

## Setting up ikiwiki ##

1. Copy the sample setup file to your home directory

        cp /usr/share/doc/ikiwiki/html/ikiwiki.setup ~/

2. Edit the file. A big concept I missed the first time arround: the "wrappers"
section refers to executable files that ikiwiki will create for you. There are two that you may be interested in:

    * The ikiwiki.cgi wrapper is used by the web server to execute commands based on comments or posts added via the web browser.

    * The post-update wrapper is executed every time you update the git repository - it triggers the wiki to be re-freshed.

The "wrapper" variable is the file location you want ikiwiki to create the
wrapper file for you. 

## Setting up your local repository ##

1. Clone the repository:

        git clone user@host:/path/to/iw-repo local-ikiwiki

2. Now - make changes to any of the files.

3. Finally - commit your changes and push them upstream:

        git add
        git commit
        git push

4. And voila, it should be showing up on your site.
