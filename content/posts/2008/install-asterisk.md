---
title: "Installing asterisk"
date: 2008-01-08T09:32:44.149780
---

This is attempt number two. Attempt number one I didn't properly document, so hopefully this attempt will be more successful just for that reason. Below is my final email on our previous attempt. The two differences with the current attempt are:

 * It's June 2008 as opposed to September 2007. Hopefully we have new software that might have fixed problems from before.

 * New server hardware. We're using a different computer.

Here's the email of the previous problem:

		I've started going through and trying to debug. I'm seriously at the point of
		recommending that we remove the card and send it back for a replacement if not
		full refund (and go for a different phone system).

		I've limited experience with the zaptel stuff before and I find it incredibly
		flakey. I don't know if it's this particular card or this particular card
		model (it's a newer version). Several years ago I worked with a different
		zaptel card and, once we got it working, it seemed to work pretty
		consistently.

		In any event... this was my experience this evening:

		I confirmed the familiar problem of calling into the system and not having the
		system respond to key presses. And once again I confirmed that by shutting
		down asterisk, unloading and reloading the modules and starting asterisk
		again, the problem goes away (for the time being).

		I created a script in /root called reset-asterisk that will take care of this
		in one fell swoop.

		I tested dialing out from both phones on 718, 917 and did not get the "You
		must dial a one" error. However, as I continued to test I did get the error
		and believe that it is intermittent. Sigh. Looking at the console, asterisk
		appears to be sending out the one with the number - maybe it's not waiting
		long enough for the dial tone so the one is not properly going through?

		I also found that sometimes I could pick up both internal phones and make two
		simoultaneous calls (at least once) but most times I would get that error or a
		busy signal.

		To make matters worse, at two points in my testing the entire server froze up
		(did not respond to pings) forcing me to hard reset the machine.

		My sense is that most if not all of these problems are related to the zaptel
		card and/or kernel modules. And what makes this so difficult is that all the
		problems seem to be intermitten!!

		I'm not sure what to do at this point, but am open to suggestions!

		jamie

		p.s. I made the following changes on the server:

		* Enabled serial console access (modified /etc/inittab and
		+/etc/boot/grub/menu.1st)

		* Uploaded "The Man in Black.mp3" into the /var/lib/asterisk/moh directory. On
		one server crash, asterisk was trying to access that directory (moh = music on
		hold). I'm not sure why, but thought I would throw a file into that directory
		to see if that was why it was failing.

		* changed extensions.conf to use a different programming logic to select the
		line to dial out with (based on:
		http://www.voip-info.org/wiki/index.php?page=Asterisk+cmd+ChanIsAvail).

We're starting with a Debian Lenny installation that is stripped down to run
just the basics. Then:

		aptitude install asterisk
		aptitude install zaptel zaptel-source

Then, based on /usr/share/doc/zaptel/README.Debian.gz, I ran:

		module-assistant a-i zaptel

Woops - got an error: 

		Bad luck, The kernel headers for the target kenrel version could not be
		found and you did not specify other valid kernel headers to use.

Tried again with:

		module-assistant prepare

And got:

		The following packages have unmet dependencies: linux-headers-2.6.22-3-686:
		Depends: linux-kbuild-2.6.22 but it is not installable E: Broken packages

Strange. Linux 2.6.24-1-686 is installed:

		0 zaius:/usr/share/doc/zaptel# aptitude search linux-image | grep ^i
		i   linux-image-2.6-686             - Linux 2.6 image on PPro/Celeron/PII/PIII/P
		i A linux-image-2.6.22-3-686        - Linux 2.6.22 image on PPro/Celeron/PII/PII
		i A linux-image-2.6.24-1-686        - Linux 2.6.24 image on PPro/Celeron/PII/PII
		0 zaius:/usr/share/doc/zaptel#

But is wasn't chosen when I rebooted:

		0 zaius:/usr/share/doc/zaptel# uname -a
		Linux zaius 2.6.22-3-686 #1 SMP Sun Feb 10 20:20:49 UTC 2008 i686 GNU/Linux
		0 zaius:/usr/share/doc/zaptel#

I ran update-grub:

		0 zaius:/usr/share/doc/zaptel# update-grub 
		Searching for GRUB installation directory ... found: /boot/grub
		Searching for default file ... found: /boot/grub/default
		Testing for an existing GRUB menu.lst file ... found: /boot/grub/menu.lst
		Searching for splash image ... none found, skipping ...
		Found kernel: /vmlinuz-2.6.24-1-686
		Found kernel: /vmlinuz-2.6.22-3-686
		Updating /boot/grub/menu.lst ... done
		0 zaius:/usr/share/doc/zaptel#
	
Checked /boot/grub/menu.lst and made sure default was set to 0 and the 2.6.24
kernel was the first listed and then rebooted.

Now we are using 2.6.24:

		0 zaius:~# uname -a
		Linux zaius 2.6.24-1-686 #1 SMP Thu May 8 02:16:39 UTC 2008 i686 GNU/Linux
		0 zaius:~# 

Strange. Not sure what happened before.

Now running module-assistant a-i zaptel returns without an error (it installed
cpp-4.1 gcc-4.1 gcc-4.1-base libmudflap0 libmudflap0-dev
linux-headers-2.6.24-1-common linux-kbuild-2.6.24) and then built the kernel
modules from the zaptel-source.

Now:

		0 zaius:~# lsmod | grep zap
		1 zaius:~# modprobe zaptel
		0 zaius:~# lsmod | grep zap
		zaptel                188548  0 
		crc_ccitt               2176  1 zaptel
		0 zaius:~#

Ok. So far so good. The zaptel device seems created:

		0 zaius:~# find /dev/zap
		/dev/zap
		/dev/zap/ctl
		/dev/zap/pseudo
		/dev/zap/channel
		/dev/zap/timer
		0 zaius:~#

Continuing with README.Debian.gz:

		/etc/zaptel.conf
		----------------
		A sample /etc/zaptel.conf is no longer installed by default. You should 
		generate it manually (or automatically with genzaptelconf) if and when
		you actually have zaptel hardware and installed a zaptel-modules package
		for your kernel version.

Ok.

		0 zaius:/usr/share/doc/zaptel# genzaptelconf
		0 zaius:/usr/share/doc/zaptel#

That was really anti-climatic. Nonetheless, it apparently worked:

		0 zaius:/usr/share/doc/zaptel# cat /etc/zaptel.conf 
		# Autogenerated by /usr/sbin/genzaptelconf -- do not hand edit
		# Zaptel Configuration File
		#
		# This file is parsed by the Zaptel Configurator, ztcfg
		#

		# It must be in the module loading order


		# Global data

		loadzone	= us
		defaultzone	= us

		0 zaius:/usr/share/doc/zaptel# cat /etc/asterisk/zapata-channels.conf 
		; Autogenerated by /usr/sbin/genzaptelconf -- do not hand edit
		; Zaptel Channels Configurations (zapata.conf)
		;
		; This is not intended to be a complete zapata.conf. Rather, it is intended 
		; to be #include-d by /etc/zapata.conf that will include the global settings
		;
		0 zaius:/usr/share/doc/zaptel#

Hm. I think it's supposed to list our FXO/FXS modules in /etc/asterisk/zapata-chanels.conf. From our previous install, we have 6 channels:

		0 zaius:~# cat asterisk.from.blackbolt/zapata-channels.conf
		; Autogenerated by /usr/sbin/genzaptelconf -- do not hand edit
		; Zaptel Channels Configurations (zapata.conf)
		;
		; This is not intended to be a complete zapata.conf. Rather, it is intended 
		; to be #include-d by /etc/zapata.conf that will include the global settings
		;

		; Span 1: WCTDM/0 "Wildcard TDM800P Board 1" 
		;;; line="1 WCTDM/0/0 FXOLS"
		signalling=fxo_ls
		callerid="Channel 1" <6001>
		mailbox=6001
		group=5
		context=from-internal
		channel => 1
		callerid=
		mailbox=
		group=
		context=default

		;;; line="2 WCTDM/0/1 FXOLS"
		signalling=fxo_ls
		callerid="Channel 2" <6002>
		mailbox=6002
		group=5
		context=from-internal
		channel => 2
		callerid=
		mailbox=
		group=
		context=default

		;;; line="3 WCTDM/0/2 FXOLS"
		signalling=fxo_ls
		callerid="Channel 3" <6003>
		mailbox=6003
		group=5
		context=from-internal
		channel => 3
		callerid=
		mailbox=
		group=
		context=default

		;;; line="4 WCTDM/0/3 FXOLS"
		signalling=fxo_ls
		callerid="Channel 4" <6004>
		mailbox=6004
		group=5
		context=from-internal
		channel => 4
		callerid=
		mailbox=
		group=
		context=default

		;;; line="5 WCTDM/0/4 FXSKS"
		signalling=fxs_ks
		callerid=asreceived
		group=0
		context=from-pstn
		channel => 5
		context=default

		;;; line="6 WCTDM/0/5 FXSKS"
		signalling=fxs_ks
		callerid=asreceived
		group=0
		context=from-pstn
		channel => 6
		context=default

		0 zaius:~#

Trying again with -v (verbose) and -d (hardware detection):

		0 zaius:~/asterisk.from.blackbolt# genzaptelconf -vd
		Unloading zaptel modules:

		Temporarily moving zaptel.conf aside to work around broken modprobe.conf
		Test Loading modules:
			- 	wct4xxp	
			- 	wcte12xp	
			- 	wcte11xp	
			- 	wct1xxp	
			- 	wanpipe	
			- 	tor2	
			- 	torisa	
			- 	qozap	
			- 	vzaphfc	
			- 	zaphfc	
			- 	ztgsm	
			ok	wctdm24xxp	
			- 	wctdm	
			- 	opvxa1200	
			- 	wcfxo	
			- 	pciradio	
			- 	wcusb	
			- 	xpp_usb	
		Generating '/etc/zaptel.conf and /etc/asterisk/zapata-channels.conf'
		Note: generated /etc/asterisk/zapata-channels.conf not included in zapata.conf
		To fix:  echo '#include zapata-channels.conf' >>/etc/asterisk/zapata.conf
		Reconfiguring identified channels

		Zaptel Version: 1.4.10.1
		Echo Canceller: MG2
		Configuration
		======================


		Channel map:

		Channel 05: FXS Kewlstart (Default) (Slaves: 05)
		Channel 06: FXS Kewlstart (Default) (Slaves: 06)

		2 channels to configure.

		0 zaius:~/asterisk.from.blackbolt#

Hm. Now it found 2 our of the 6 channels. That's an improvement. The channels it found are the FXS channels (the ones you plug a telephone line into, as opposed to FXO which plug a telephone into).

Ok. Enough for tonight...




