---
tags: ["mayfirst","amc2008"]
title: "Web 2 point... well you know"
date: 2008-01-08T09:32:44.149780
---

We're at the [Allied Media Conference](http://alliedmediaconference.org/) - day
1! Stay tuned for more blogs about what I'm seeing.

This is the first session on day one present by Geoff Hing.  Despite the name
of the session (ug - jargon hell!) - it was an interesting workshop. A few
thoughts:

 * Tags as non-hierarchical approach to information organizing. I've never
 really considered the political implications of free tagging versus
 hierarchical categorizing. It really pushes the power of creating meaning
 toward the users rather than the administrators. In the worst scenarios it
 means everyone is an independent agent defining their own reality - the worst
 aspect of liberalism.  On the other hand - that's where organizing comes in -
 to organize our own meaning (in this context that means collaboratively
 defining tags).

 * Use of device independent communication. Geoff's response to to the often
 stated problem "but not everyone has access to the Internet" is: then use
 other ways of communicating, such as cell phones (twitter) or even landlines
 (jot) to bridge the gap.

 * Use of API (application programmer interface) as means of decentralization. 

It was a real relief to go to a Web 2.0 presentation that analyzed the concepts
from a political perspective.

The political discussion and conclusions, however, are the same ones we've been
going over and over: how do we use the Internet when not everyone has access to
it or when it's too difficult for some people. I don't want to detract from
this problem - it's a real one. On the other hand - I think we tend to address
it too blindly in ways that lead to bad political decisions, such as using
corporate services because they're "easy" and "more accessible." I think
political work is struggle - and yes, we do need to struggle with this
technology.
