---
title: "The wonders of ps2pdf"
date: 2008-01-08T09:32:44.149780
---

I really don't like printing. Most things I print just end up in the recycle
bin or lost. However, there are times when it's nice to have a physical copy of
something (like making a card or a poster).

I just created a tabloid sized poster in the [GIMP](http://gimp.org). I needed
to convert it to a PDF so it could be printed. I first saved the file with a
.ps extension, so the GIMP saved it as a postscript file. Then, I ran:

		ps2pdf file.ps

And voila, courtesy of the wonders of free software, file.pdf was created.

Unfortunately, the pdf displayed the poster in letter size (meaning a good
portion was cut off).

Hm. How does this work? I fumbled around with my GIMP and evince settings
([evince](http://www.gnome.org/projects/evince/) is my PDF viewer of choice) to
no avail. Then I realized that I didn't even know where the paper size is
supposed to be set. In the pdf document itself? Or, does the PDF viewer need to
be told? After some Internet searching, I discovered the pdfinfo command -
which prints a summary of information about a PDF file. When I ran pdfinfo
against my pdf file I got:

		Producer:       ESP Ghostscript 815.03
		CreationDate:   Sun May 25 12:46:12 2008
		ModDate:        Sun May 25 12:46:12 2008
		Tagged:         no
		Pages:          1
		Encrypted:      no
		Page size:      612 x 792 pts (letter)
		File size:      1108153 bytes
		Optimized:      no
		PDF version:    1.4

Ah - my pdf file itself is indicating letter size. I'm still not sure whether
my original ps file contains any hints about paper site (although I read an
[interesting article on whether or not it should provide this
information](http://allendowney.com/essays/orientation/)). However, I did find
out that many programs use the [libpaper](http://packages.debian.org/libpaper1)
library to determine paper sizes to use. With libpaper, your default paper size
is configured in the file /etc/papersize. You can override this default by
setting the PAPERSIZE environment variable. 

ps2pdf seems to use libpaper, which is why it made my ps file into a
letter-sized pdf file. I re-ran ps2pdf with a new value for the PAPERSIZE
environmental variable:

		PAPERSIZE=11x17 ps2pdf file.ps

And sure enough, it worked:

		Producer:       ESP Ghostscript 815.03
		CreationDate:   Sun May 25 12:38:34 2008
		ModDate:        Sun May 25 12:38:34 2008
		Tagged:         no
		Pages:          1
		Encrypted:      no
		Page size:      792 x 1224 pts
		File size:      1108152 bytes
		Optimized:      no
		PDF version:    1.4
