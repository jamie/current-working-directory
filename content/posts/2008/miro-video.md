---
tags: ["mayfirst","amc2008"]
title: "YouTube for the left?"
date: 2008-01-08T09:32:44.149780
---

The Miro project has come a long way from the Democracy Player I remember from
years back. The folks at the [Participatory Culture
Foundation](http://participatoryculture.org/) have re-organized the project
from a media player to something many times more powerful: they are
building a radically decentralized and democratic system for sharing video
content.

They demonstrated [Miro](http://www.getmiro.com/) at the 2008 [Allied Media
Conference](http://alliedmediaconference.org). Essentially, it is a program
that organizes the world's video feeds and helps us all find and watch them. 

Wow.

So what exactly does that mean?

When you download Miro, by default it comes installed to use the collection of
video blogs that have been submitted to the [Miro
Guide](http://miroguide.com/). Anyone can submit a video feed to the guide and
pending a review (can't seem to find any documentation on how the feeds are
reviewed) it will show up in the guide.

However, there's no reason you need to use the Miro Guide. You can specify any
other site to pull your content from and you can individually add your own
feeds. In fact, the Miro folks have gone out of their way to de-brand the
program. They even offer a way to [brand your own miro
player](http://www.getmiro.com/co-branding/).

Let's think about why this is 100 times better than YouTube:

 * Decentralized. There is no one web site where everyone has to upload their
 video that can be taken down, sold, crash or can go out of business.

 * Licensing. You are not handing over video content to anyone except the
 server storing your video - and you can choose to store your video where ever
 you want. You only need to publish the feed on Miro Guide. Ever read the
 YouTube [terms of service](http://youtube.com/t/terms)? My favorite part:

		... by submitting User Submissions to YouTube, you hereby grant YouTube a
		worldwide, non-exclusive, royalty-free, sublicenseable and transferable
		license to use, reproduce, distribute, prepare derivative works of,
		display, and perform the User Submissions ...
	
 * Standard protocols. Miro runs on RSS - already a bedrock, standard protocol.

 * Politics. The project is engineered to prevent the Participatory Culture
 Foundation from perverting its democratic potential. The software is not only
 free/open source, but it is designed to give equal footing to any and all
 content providers. That, I think, is the best indication of any groups
 politics. In addition, they've structured their organization in a way [that keeps them honest](http://www.getmiro.com/about/):

		People often ask why we're setup as a non-profit rather than a for-profit.
		Quite simply: all of us at PCF are drawn to the project because of the
		mission and being a non-profit is the only way we can ensure that the
		mission is built into the structure of the company. So many times we've
		seen for-profit companies lose their values as financial pressures mount,
		founders leave, or they get acquired. We want to make sure that can't
		happen.

		Being non-profit has other benefits as well. Most importantly, it means
		that we are accountable to our user community and the public. There aren't
		any venture capitalists or shareholders that can force us to go in a
		direction that's bad for users but good for profits.

	And lastly, they declare their
	[mission](http://www.getmiro.com/about/mission/) which explicitly states
	their commitment to openness.

On the last point ... from a radical movement perspective, I don't want to over
estimate their politics - this is a decidedly liberal organization firmly
rooted in, and limited by, the foundation dominated non-profit world.
However, the core values that form the basis of the project are core values
that I share and provide a powerful basis for collaboration with the left. 

Miro is a project we should all be behind 100%!
