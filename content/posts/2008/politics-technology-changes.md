---
tags: ["mayfirst","another-internet"]
title: "Goals for a New Global Movement"
date: 2008-01-08T09:32:44.149780
---

The World Social Forum says: "Another world is possible." How is this world
shaping up? What role will the US play? What role will technology and the
Internet play? How can we prepare?

It's impossible to know what the world will look like in the future, but there
are some unmistakable trends visible today. While the US has experienced
recessions in the past, the current financial state of the country is bleaker
than we've seen it in a long time. Meanwhile, China is economically growing
and taking on super power stature in the world previously dominated by the
United States. Increasingly, the most important role that US is making to the
global economy is that of consumer, based largely on credit.

Similarly, the left in the US, once a significant force in the world, is
increasingly overshadowed by revolutionary movements in Latin America, ranging
from the World Social Forum movement itself (born in Brazil) to the Zapatistas
novel creation of a local/world movement to leaders like Hugo Chavez
re-writing US/Latin American relations. 

These movements have had a powerful impact on the politics and culture of the
region. For the first time in contemporary history, almost all the countries
of Latin America elect their governments.  Dictatorship, once the "norm" in
the Spanish-speaking Americas, is not only not prominent but it has become a
kind of atrocity in the popular political culture. Furthermore, the moves
being made "from the top" (like Chavez in Venuzuala and Morales in Bolivia)
have enormous and broad poliical support. In the case of Morales, his
presidency was the culmination of years of grassroots mobilization and
organizing. 

There's no mistake: the US is losing its central role in the world.
Furthermore, the new world is increasingly based on international
relationships and, from the left, the most powerful moves are being made in
Latin America.

Without knowing anything else, these trends give us a lot to work with. For US
activists, it means examining our own center-of-the-universe perspectives.
Nobody is immune from the environments in which they are raised. How does the
increasing irrelevence of the US change our political priorities? What does
this mean for locally-focused projects when the policies and trends in our
country are increasingly determined by other countries? How relevant are
political projects that are not looking beyond our borders?

In many ways, former empire countries like the US and England are perfectly
situated to build international movements. There's a much higher percentage of
bi-lingual and bi-cultural activists with close ties to other countries than
any where else. However, growing up in an empire country has its draw backs as
well. These global trends require US activists to adjust to being in a
movement in which we're no longer the center. 

It also involves a change in scale. The left in the US is tiny compared to the
global left. Our strategies, honed to work with memberships numbered in the
hundreds or thousands, need to now accomodate tens of thousdands and hundreds
of thousands.

While other countries may have more experience with scale, nobody has
experience with the scale international movements can offer. How do we
organize mass movements? Due to limits in communication, previous mass
movements have often been based on top-down, undemocratic processes. Now, with
remarkable advances in communications and the Internet, what options do we
have to democratize these processes?

One unique asset that the US has is a relatively well developed liberal
technology community. If nothing else, the nonprofit industrial complex has
created enough opportunities for left-leaning technologits to make a living
while developing extremely valuable Internet-related skills. It's based on
this community that projects like May First/People Link, Riseup (and in
Canada, Tao and Resist) have been able to flourish. 

As these projects have developed, a politics has developed with them as well,
a politics that, while rooted in technology, has broad implications well
beyond computers and the Internet. 

Technologists, even liberal ones, have much more exposure to very radical
ideas around openness, transparency, and freedom. These ideas come from some
broad Internet-based movements (the free software movement being one of the
most prominent) - but also from a deeper understanding of and experience with
many technology experiments that are rooted in these ideas (like Wikipedia or
the Debian Linux development team).  

In addition, while the US still dominates most free software projects, the
degree of internationalization in the free software world is astounding.  Free
software technologists have made an enormous contribution to understanding how
we can build systems that operate transparently in multiple languages and with
multiple cultures. This contribution is valuable in and of itself, but
additionally gives US technologists, unlike our non-technological
counterparts, some concrete experience in what it means to collaborate across
borders.

These ideas, taken to their radical roots, are well suited for democratic,
mass movements. These ideas, as they exist in the minds of technologists and
non-technologists on the left today, however, are still very under-developed,
particularly in the broader political context of building democratic mass
movements.

How do we prepare and develop technologists for a more political role? How do
we develop and prepare non-technologists for this role?

__Politicizing technology__

One strategy is to politicize our technology work. This strategy pertains as
much to technologists as it does to non-technologists. One step in this
direction is to consider common technology goals in a political light.

For example, consider these goals common to technology projects in a broader
political context:

 * Redundancy: no person or server should ever occupy a role so central that
 if they were to be removed, the entire project would fall apart

 * Scaleability: we should start with a strategy that not only works with 50,
 but with 500 and 5,000 and even 5 million as well. Similarly, our strategy
 must scale culturally - it must function with people of all backgrounds,
 races, nationalities, social groups and sexes.

 * Transparency: all decisions, meeting notes, discussions should be in the
 open, publicy available for review. No information should be proprietary or
 horded.

 * Trust: new people should be able to join the campaign with enough
responsibility to productively contribute and demonstrate their trustability,
but without the ability to do great harm to the campaign.

How many of our political organizing campaigns adhere to these goals?

Certainly not all technology projects adhere to these goals. However, based on
experience with the free software movement, many technologists do adhere to
these goals and, in some political organizing campaigns, the technologists are
the ones pressuring the political leaders on these goals. 

These goals transcend technology - they shatter the distinction between the
organizing and the tech work - demonstrating that building a global movement
means that tech work is political work and must be full incorporated into the
political thinking and development of the movement. Building from this model
means that there are no non-techies: we are all techies of different skill
levels, just as we are all organizers at different levels of political
development.

These are the goals of a global movement, one that hasn't been possible before
now. 




