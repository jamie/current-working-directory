---
title: "Welcome to the Monkeysphere!"
date: 2008-01-08T09:32:44.149780
---

After months of coding and testing, the Monkeysphere had it's [first public
announcement](http://www.debian-administration.org/users/dkg/weblog/36).

Yippee!

You can read the announcement for the technical details (or just [go to the web
site itself](http://web.monkeysphere.info)).

For me, the most important aspect of the Monkeysphere is: placing security in
the hands of us, the users. The Monkeysphere is another extension of the [web
of trust](http://en.wikipedia.org/wiki/Web_of_trust), perhaps the most powerful
approach to security in a densely populated world. With the web of trust we
build our own networks of trust: I assign trust to my friend Jose. If Jose
verifies someones identity, then I trust that verification. And so on. 

The deep dark secret on the Internet, particularly for folks on the left, is
that most Internet security systems operate differently. Using an alternative
system called [x509](http://en.wikipedia.org/wiki/X509), they rely on the
entire Internet designating a limited number of "certificate authorities" to
verify the identities of the people we work with. Most of these authorities are
for profit corporations. But even if they weren't for profit, why a
hierarchical model? Is this the world we want to build on the Internet?

The Monkeysphere introduces the ability to use the web of trust when we connect
to servers using [secure shell](http://en.wikipedia.org/wiki/Secure_shell) or
[secure FTP](http://en.wikipedia.org/wiki/SSH_file_transfer_protocol). While
the use secure shell and secure FTP is mostly limited to system administrators
and web developers, the introduction of the web of trust into this aspect of
the Internet is a major and exciting first step toward introducing it even
further, into security contexts used by everyone.
