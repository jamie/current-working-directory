---
tags: ["cispes"]
title: "CISPES Under Attack"
date: 2008-01-08T09:32:44.137780
---

One of my earliest political experiences with the US government meddling with
political movements was during the 80's. I was working with Youth for A Nuclear
Freeze. I was on the Latin America Committee (yes, we had some growth naming
issues). We talked a lot about the FBI Internal Security investigation into
CISPES (the Committee in Solidarity with the People of El Salvador).

Now, they are at it again. Please [read their
advisory](http://cispes.org/index.php?option=com_content&task=view&id=390&Itemid=27)
and take the steps asked at the end.
