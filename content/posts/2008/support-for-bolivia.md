---
title: "Bolivia getting support"
date: 2008-01-08T09:32:44.149780
---

If you live in the United States you'd be forgiven for missing the story. 

To find out what's currently happening in Bolivia, check out the [story on
nacla.org](http://nacla.org/node/5016), which provides the basic information
and some background on the right wing attempts to over throw the democratically
elected Evo Morales. 

[Portside](http://www.portside.org) also carried a [couple short
pieces](http://www.portside.org/?q=showpost&i=4800) about how the other
left-leaning governments in the region are all coming to the aid of Evo
Morales.

Update (2008/09/15): Looks like the mainstream press is starting to cover the
events in Bolivia. There's a [Newsweek article reprinted on Portside](http://www.portside.org/?q=showpost&i=4804) and a [reasonable NY Times article](http://www.nytimes.com/2008/09/15/world/americas/15bolivia.html?pagewanted=1&_r=1&hp).

Update (2008/10/02): Thanks Jeff for the link to the [Democracy Center coverage](http://www.democracyctr.org/blog/index.htm).
