---
tags: ["mayfirst","security"]
title: "What is acceptable security?"
date: 2008-01-08T09:32:44.137780
---

The security we use to protect [May First/People Link](http://mayfirst.org)
members is not perfect, but we try pretty hard with what I would consider to be
a good results. 

I'm constantly amazed to see how much better capitalized industries and
companies fail to take even basic steps to secure their systems.

Consider the banking industry. We've always known that the information that is
on our credit cards - the same credit cards we hand over to countless people,
the same information we provide over the phone and the Internet to yet more
people, is all the information anyone needs to take our money. This should not
come as a surprise, given our history with checks. The information on a single
check is enough to compromise ourselves financially (thanks
[dkg](http://fifthhorseman.net) for pointing out an [interesting articles on
the topic of identity theft via routing
numbers](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2008/01/07/nclarkson107.xml).

The other day I read an [article about a credit card
breach]((http://www.nytimes.com/2008/03/23/us/23credit.html) due to information
be stolen in transit between computers. My favorite quote was: "Wider use of
encryption might seem an obvious answer. But in practice, encryption is unused
at certain points in a data-processing chain because the computing power it
requires can slow transactions." Hm. I guess it depends on where you priorities
are.

This morning I had yet another experience - this one personal. I realized that
I didn't have the flight number or confirmation of a flight I bought for my
brother. I called the American Airlines phone number and, talking to their
automated voice thingy, I gave the city and time of the departure and my
brother's last name and AA confirmed his flight. I had my credit card ready so
I could enter the last four digits to prove that I was the one who bought the
ticket - but apparently that's not necessary. I considered calling back and
saying a number of different last names to see if I could build a list of
passengers.
