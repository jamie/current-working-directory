---
tags: ["fsa2008","mayfirst"]
title: "The Internet in 2008 without high speed access"
date: 2008-01-08T09:32:44.149780
---

Working on tech support in Guatemala City in October 2008 for the Social Forum
of the Americas is having a profound impact on my assumptions.

The Internet and all my assumptions about tech are very different in a place
without regular, high speed Internet access. It's not possible to just say it's
like being in New York 10 years ago, because it's not like that. The Internet I
use regularly is an Internet based on an assumption that you have high speed
Internet access - an assumption that didn't exist 10 years ago. 

Now, I'm re-thinking everything, like when and how often I install full system
upgrades, the value of having installation disks that don't need a network
connection to install an operating system or program, and how our
communications protocols should function if we assume inconsistent network
connections rather than the opposite.
