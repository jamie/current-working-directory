---
tags: ["fsa2008","mayfirst"]
title: "Americas Social Forum: Day One"
date: 2008-01-08T09:32:44.149780
---

I've arrived, with less than an hour of sleep. The irony of the last leg
departing from George Bush Airport is not lost on me.

The big news today: Evo Morales is going to address the Forum!

It's happening on Thursday. It's throwing a bit of wrench in the scheduling,
but there's definitely excitement here.


