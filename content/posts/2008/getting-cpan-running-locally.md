---
title: "Getting CPAN running locally"
date: 2008-01-08T09:32:44.149780
---

My goal was simple - to install the DateTime::Event::Recurrence CPAN module for
my non-privileged user. As often is the case when I try to figure out CPAN
(about once every two years - long enough to forget everything I knew about it
previously) - it seemed a lot harder than it should have been.

I eventually succeeded by following these steps:

 * I made my own local perl directory:

		mkdir share
		mkdir share/perl

 * I exported a couple environmental variables. Note, by appending a colon to
 the end of MANPATH I'm instructing the manpath program to append the system
 default MANPATH paths to the end of my custom one (see man manpath). According
 to man perlrun, the PERL5LIB variable defines local directories to check
 *before* checking standard ones - so no need to worry about over-writing the
 standard library locations.

		echo "export PERL5LIB=~/share/perl/lib" >> ~/.bashrc
		echo "export MANPATH=~/share/perl/man:" >> ~/.bashrc

 * And then sourced them:

		source ~/.bashrc

 * I ran cpan with:

		cpan

 * I chose to let cpan auto configure my settings

 * From the cpan shell, I told cpan about my own directories:

		o conf makepl_arg "LIB=~/share/perl/lib \
			INSTALLMAN1DIR=~/share/perl/man/man1 \
		  INSTALLMAN3DIR=~/share/perl/man/man3 \
			INSTALLSCRIPT=~/share/perl/bin \
			INSTALLBIN=~/share/perl/bin \
			PREFIX=~/share/perl"

		o conf makepl_arg "LIB=~/share/perl/lib \
		  INSTALLMAN1DIR=~/share/perl/man/man1 \
			INSTALLMAN3DIR=~/share/perl/man/man3 \
			INSTALLSCRIPT=~/share/perl/bin \
			INSTALLBIN=~/share/perl/bin \
			PREFIX=~/share/perl"

 * Saved these settings for next time:

		o conf commit

 * I installed the latest version of CPAN with:

		install cpan

 * And then installed what I wanted:

		install DateTime::Event::Recurrence	

