---
tags: ["mayfirst","amc2008"]
title: "The state of linux on the desktop"
date: 2008-01-08T09:32:44.149780
---

In 2001 I was certain that the linux desktop revolution was just around the
corner. It would hit in 2002, 2003 latest. Well, that didn't happen. By 2005 I
got tired of waiting and simply switched my desktop permanently to linux. At
that point, I stopped paying close attention. 

As [Grace Lee Boggs](http://en.wikipedia.org/wiki/Grace_Lee_Boggs) pointed out
during her closing key note at the 2008 [Allied Media
Conference](http://alliedmediaconference.org), unlike uprisings or revolts,
revolutions take time. They move slowly. So slowly, in fact, that we often
don't recognize or appreciate the amazing changes that have taken place.

I had that experience at Steven Mansour's [AMC presentation on using free and
open source tools to make media](http://amc.stevenmansour.com/). Steven
demonstrated, with the audience participating, how to make flyers, images,
audio programs, and even videos not only using free software, but running on an
[Ubuntu Linux](http://ubuntulinux.org) laptop. In fact, Steven barely mentioned
that he was running linux! There were occasional asides of "oh, yes, I *think*
you can do that with a Windows or a Macintosh; not sure it would work as well
though."

Since I'm not up to date on multimedia applications, I learned a lot of
practical information from the workshop. However, the most powerful realization
that I took from the workshop was the amazing progress we've made in getting
linux into the consciousness of the left.
