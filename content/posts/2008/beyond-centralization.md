---
tags: ["another-internet"]
title: "Beyond Centralization"
date: 2008-01-08T09:32:44.137780
---

We have a lot of work to do on the left to develop our political understanding
of the dangers of relying on corporate Internet services for our political
organizing work. However, the corporate part of the equation is only half the
problem. The other half is centralization - a problem that affects even the
most politically radical hosting providers.

A group of people recently began working together on the
[monkeysphere](http://cmrg.fifthhorseman.net/wiki/OpenPGPandSSH) - a project to
broaden the use of OpenPGP in our Internet work. Although progress on the
project goals has been slow going, we've done a lot of thinking about how to
collaborate in a way that avoids centralization. 

(These ideas for organizing are based on a transparent organizing model - in
other words, there is no space for private communication.)

Consider your typical project (a tech project or even a non-tech focused
political project): First you get a web site (centralized), then you get an
email list (centralized on a single list server, which sends email to people
via their centralized mail provider). If you are particularly tech savvy you
might also throw in a (centralized) wiki for collaboration or a project
management site (like [trac](http://trac.edgewall.org) or basecamp). If you are
working on a software project, you may want to use a centralized [revision
control system](http://en.wikipedia.org/wiki/Revision_control_system) (like
[subversion](http://subversion.tigris.org/)). In other words, just about every
aspect of the collaboration relies on a technologically centralized system.

What's right with this model? For one, joining the project is technically easy
- you look at a web site (piece of cake) or join a list (easy for most people
comfortable with the Internet).

What's wrong with this model? The people who control the central resources
control the project. Often, joining a list requires moderator approval.
Accessing a password-protected web site requires that someone create your
account. Usually, the people who are responsible for these duties are
responsible because they happened to be the ones who set the technology up -
not because a political decision in the group was made to empower them with
this decision-making responsibility.

In addition - what happens when the web server breaks or is seized? Or when the
email list goes down? Or when any of the central resources get hit so hard with
traffic (legitimate or not) that they can't cope?

And finally - what if the politics of the group is decidedly anarchistic or
otherwise politically committed to decentralization? 

With the monkeysphere project we decided to experiment with collaborating using
tools that were de-centralized. We started by using a tool called
[git](http://git.or.cz/). Git is a de-centralized revision control system,
which is a fancy way of saying: it's a tool that keeps track of text files -
saving all revisions and changes made by all participants (like a wiki). One
way git is special is that it does not rely on a centralized server for people
to collaborate. Instead, each participant publishes their own copy of their
files, and every other participant can choose to merge their repository with
everyone else's repository. All changes are kept track of and can be identified
by author, undone, or accepted.

In the monkeysphere project, git is about as far as we've gone with this
theory. Below, I've hashed out some ways to take it even further.

Web site: it is useful to have a single web site for people to go to in order
to learn about your project. If you are using git, then you can publish your
web site files via git. That way, every member of the project has a copy of the
site in their git repository and has the *capacity* to publish it as a web
site.  Most groups would want to *choose* a single individual to take
responsibility for this duty - however, if the group decides the chosen
individual is not doing it reliably, or the chosen individual's public web
server goes down or is seized, it is technically simple for any other
individual in the group to re-publish it somewhere else.

What about communication? If every member has a published blog capable of tags
- we could communicate with each other by publishing blog posts with the agreed
upon tag. Each project member would be responsible for pulling in everyone
else's RSS feed of their blog on a regular basis. Sending an "email" to the
group would simply be a matter of posting a new item to your blog with the
appropriate tag. New members join the communication by subscribing to the
other members RSS feeds. A list of participants RSS feeds could be stored in
the git repository.

At the moment - these ideas are barely within the reach of a fairly
sophisticated group of technologists. To expect the left to adopt these
strategies now is unrealistic. However, this model of organizing suggests some
new core competencies that we may want to consider developing - including the
use of revision control systems, RSS and blogging so we can plan for a future
when de-centralized organizing can happen on the Internet.
