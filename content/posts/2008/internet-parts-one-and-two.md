---
tags: ["mayfirst","amc2008","another-internet"]
title: "Internet Part 1 and Part 2"
date: 2008-01-08T09:32:44.149780
---

To use the [People's Production House](http://www.radiorootz.org/) slogan: The
Internet is Yours ... If you want it.

PPH started Part 1 of the workshop by exploring the [Digital Expansion
Initiative](http://www.radiorootz.org/taxonomy/term/13/). They shared both the
results and the methods they use to learn about how people use the Internet and
how we want it to develop (see some of their
[testimony](http://peoplesproductionhouse.org/audio/by/album/digital_expansion_initiative)).

I really appreciated their transparency: we went through the various methods
for collecting research (via interviews, drawing pictures, etc) - for each
method we all did it and discussed the effectiveness of the strategy.

Part II was May First/People Link - and the [Internet Rights
workshop](https://support.mayfirst.org/wiki/internet_rights_workshop).

And the results are in. The number in brackets is the number of groups that
endorsed the right. We had 5 groups total.

1. Free Speech and peacable assembly without permit for anyone, everywhere. A
	right including the right to dialogue. [4] 

2. Right to internet and technology public education. [4]

3. All people have the right to be involved in a public global democratic process to manage the full spectrum of communications resources: [3] 

4. Right to transparency about government and corporate activity, surveillance, activities. [2] 
 
5. Right to personal privacy; Control and consent over your personal information; transparency of information collection practices. [2] 

6. Airwaves/spectrum are a public resource controlled and managed by the people for public, not private, gain. [2]

7. Community ownership of internet infrastructure (domain name system, hardware, last mile connection, software source, bandwidth) [1]

8. Network managers and/or Internet Service Providers (ISPs) cannot discriminate against content of any type. [1]
 
9. all people have the right to contribute content; Rights of the common over intellectual property rights [1]

10. Peoples throughout the world have free access to ubiquitous broadband and
computing resources through public institutions such as schools and libraries
or any local resource center if they want it. [0]

