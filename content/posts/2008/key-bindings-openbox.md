---
title: "Keybindings and Openbox"
date: 2008-01-08T09:32:44.149780
---

I just had a terrifying moment when none of my custom key bindings worked. I
was completely disabled. I use [Openbox](http://icculus.org/openbox/) as my
window manager, which is a great lightweight window manager with excellent
support for custom keybindings. Except when they don't work.

Part of my problem is that I was running xorg packages from unstable in an
effort to trouble shoot my laptop crashing when I put it to sleep. After
several months, I began to think that the problem was due to MySQL (now I
shutdown MySQL and pause for 4 seconds before going to sleep). So, I finally
decided it was time to downgrade my xorg packages to Lenny (testing). After
restarting Openbox, many of my keybindings stopped working. 

Fortunately, I learned a lot in the process of trouble shooting.

I soon discovered that my problems were all related to my use of the "windows"
key - none of my key bindings that used that key worked. Everything else worked
fine.

I started by backing up my openbox rc.xml file and starting a new one. I
noticed that in the new one, the key binding for the windows key was "W"
whereas I was using "w". I switched to capital W but still no luck.

In my research I learned about the xev command. It's really cool. It prints out
what key your computer heard you press.

When I pressed the windows key the output was:

	KeyPress event, serial 32, synthetic NO, window 0x2200001,
    root 0x57, subw 0x0, time 1040577393, (134,99), root:(1122,601),
    state 0x0, keycode 115 (keysym 0x0, NoSymbol), same_screen YES,
    XLookupString gives 0 bytes: 
    XmbLookupString gives 0 bytes: 
    XFilterEvent returns: False

	KeyRelease event, serial 32, synthetic NO, window 0x2200001,
    root 0x57, subw 0x0, time 1040577525, (134,99), root:(1122,601),
    state 0x0, keycode 115 (keysym 0x0, NoSymbol), same_screen YES,
    XLookupString gives 0 bytes: 
    XFilterEvent returns: False

From this output, I learned that they key in question had keycode 115 and that
it reported NoSymbol. I learned that this should read Super_L. 

I fixed it by running:

		xmodmap -e 'keycode 115 = Super_L'

However, Openbox still didn't behave.

Then, I read the [Openbox upgrade notes relating to the Windows key binding](http://icculus.org/openbox/index.php/Help:Upgrading_to_3.4#Windows_key_in_key_bindings) and learned that:

		Key bindings that use W as a modifier key have changed. W used to be
		hard-coded to be Mod4Mask. Now it has been changed to whatever mask you have
		the Super mod key bound to.

		You can use xmodmap to see your current bindings. Most modern distributions
		bind the windows key to Super_L, and bind Super_L to Mod4Mask. So this should
		probably not affect you unless you have a highly customized system, or have
		customized your xmodmap.  

Then I ran:

		0 jamie@liberace:~$ xmodmap -pm
		xmodmap:  up to 2 keys per modifier, (keycodes in parentheses):

		shift       Shift_L (0x32),  Shift_R (0x3e)
		lock        Caps_Lock (0x42)
		control     Control_L (0x25),  Control_R (0x6d)
		mod1        Alt_L (0x40),  Alt_R (0x71)
		mod2        Num_Lock (0x4d)
		mod3      
		mod4        
		mod5        Scroll_Lock (0x4e)

		0 jamie@liberace:~$

It appears as thought mod4 is not set to Super_L, so I ran:

		xmodmap -e 'add mod4 = Super_L'

And now everything is working. I added the two xmodmap commands to my .xsession
so they are started every time I log in.

I'm still not sure what happened - but I think my switch from unstable and then
back to testing caused something to happen to my key mappings. Maybe when the xorg packages I was using make it into testing I will no longer need those lines in my .xsession file.
