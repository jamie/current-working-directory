---
tags: ["mayfirst"]
title: "What's wrong with the service economy"
date: 2008-01-08T09:32:44.149780
---

When trying to explain [May First/People Link](http://mayfirst.org) to people I
often revert to saying something along the lines of: "We're kinda like an
Internet Service Provider." 

From now on, I've decided never to use the word "service" again when describing
us. 

In fact, I'd like us to work on a statement that all people who join May
First/People Link must agree to that says: "May First/People Link does not
engage in service relationships." 

I would even extend this statement beyond May First/People Link to say: We
should never engage in service relationships when doing political work on the
Internet.

What do I mean by service relationship? Here are a few common characteristics:

* **Tunnel visioned** - aside from pleasantries, a service relationship is
focused exclusively on the task at hand. There's no room for spontaneity,
creativity or taking advantage of everyone's multi-faceted skills and
experiences. 

* **Competitive** - if not explicit, there's always an under current of
competition between the client and the provider around what the client can take
and what the provider will give. It often focuses on who controls what is
given, gaming the system and pulling rank, leaving no room for collaborative
activity.

* **Individuated** - the client is given the impression that (and is expected
to act as if) they are the only client in the world. There is nobody else and
nothing more important than the client.
In short, with a service relationship there is little if no room for
solidarity, mutual aid, creativity, transformation or any other qualities
essential for building social and political change in the world. 

In a perfect world, I can't imagine why we would want any service
relationships. What does that say about the overwhelming trend in the United
States toward a service economy?  How can we resist that trend?

Given the existing world, it's not surprising that we have a lot of service
relationships. We can't change everything at once. Within the left, our least
complicated and least important relationships are service relationships and
will probably remain that way for some time to come: electricity, accounting,
rent, post, etc. 

But wait - electricity delivery, for example, is both complicated an important.
How can I put that in the least important and complicated category? 

When I say "complicated" and "important" - there's distinction between *what*
is being provided and the *relationship* between the people and organizations
involved. In other words, it doesn't matter how great you get on with your
meter reader - it won't have much impact on your organization or mission. 

Your relationship with your technology and Internet partners on the other hand
will have a huge impact on how you advance your goals and objectives. 

Let's face it, those of us on the left who think that technology and the
Internet "should just work" are living in a world of denial. Organizing on the
left is increasingly about *how* the Internet works for our particular (and
changing) objectives. And, given the complexities involved, we find ourselves
increasingly reliant on people with particular skills on the Internet to help
us work that out.

In this situation, the kind and quality of that relationship is critical. Is it
a service relationship (with all the limitations described above) or is their a
different relationship model it should be built on? Is it a long term
relationship, worthy of spending time building a foundation, or is it a short
term relationship with limited objectives? Is it a relationship of getting, or
a relationship of mutual aid and collaboration?

This list of questions could go on and on. And, they are as important for
people seeking technology and Internet partners as they are for those of us
able to share that experience, since all parties participate in determining
what kind of relationship we are having. 

Building a new type of relationship around organizing on the Internet will take
a lot of work. Here are a few ideas that I think are important if we are to be
successful:

* Decisions on who to partner with should be based on political compatibility,
not money, personality, or who is available now. 

	By "political compatibility" I don't mean running some kind of simple
	questionnaire or political litmus test. I work with a lot of people who don't
	share my politics. However, a successful relationship should start with
	mutual respect for the goals of each partner and the goals of the project. If
	the project is to build a system to drive a get out the vote campaign, then
	the technology partner must value and support that goal. If the technology
	partner has an ideological commitment to free software, than all partners
	should respect and value those beliefs.  

	The key to achieving this goals is to develop a community of technologists
	who can make a reasonable and, most importantly, stable living while
	partnering on these projects. I make no assumptions here: it doesn't mean
	technologists *have* to get paid directly for working for the left or we need
	to create some kind of network of small businesses. Maybe that would do it,
	maybe not. Maybe we need to advance our technology skills dramatically - so
	that at any given political meeting, we can pull together a respectable
	technology team in which no one or two people will get saddled with
	everything. Who knows. But we need to figure something out because at the
	moment building technology partnerships are often acts of desperation due
	scarcity of technologists who can work at rates that the left is paying.

* Recognize what's going on: technology and the Internet is not a product.

	Despite mainstream conceptualization of technology and Internet relationships
	as service relationships, we also somehow think of them as involving a
	product. We release Requests for Proposals (RFPs) which describe what we want
	and expect bids that will say how much it will cost to deliver what is
	requested in the RFP. On the technologists end, we think we're going to "wrap
	up" the project next week, freeing us up to deliver the next project. It's
	remarkable. Even those of us who have done this work for years and years
	often insist in our heads (or out loud) that we're either going to "get" that
	web site or database in two weeks or that we're going to "hand over" the
	project on the deadline. We know that never happens. No project is every done
	(unless the relationship ends). The beginning of every project is the
	beginning of a relationship.

	Money has something to do with it. Often volunteer projects are much more
	relationship focused that product focused. Similarly, projects in which I'm
	paid a monthly stipend (as opposed to a flat fee) have a very different
	nature to them. However, there are plenty of exceptions. 

* Mutual support

	All partners must support each other's projects. That might be a
	technologists offering non-technical strategic ideas about the campaign. It
	might be an organizer pointing out technical flaws of the project in the
	context of helping the growth and development of the technologists. In either
	case, the relationship has to be supported as much as the project itself.

