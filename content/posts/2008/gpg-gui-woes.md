---
title: "GPG for the masses"
date: 2008-01-08T09:32:44.149780
---

dkg and I spent the afternoon trying to figure out how to help people use GnuPG
to send and receive encrypted email. We specifically were interested in
programs that are cross-platform or run on a Macintosh (we have no Windows
computers to test on). For linux users,
[Seahorse](http://www.gnome.org/projects/seahorse/) is hands down the best
graphical user interface for GPG.

We tackled two issues:

* Finding a decent GUI for managing keys.

* Using a removable disk (USB thumb drive) to store GPG keys so they could be
used on multiple computers.

Here are some of the notes.

## GUIs for managing keys

### [GnuPrivacy Guard (GPA)](http://www.gnupg.org/gpa.html)

Our first hopeful, is a bit rough around the edges. It does run on both Windows
and Linux, which is very promising. However, it's not being updated in Debian
or Ubuntu (last update was back in 2006). We installed the most recent upstream
source (released in the summer of 2007) and compiled it. 

The first problem happened when we tried to edit the key we had just generated.
The only key editing options were to change the passphrase and the expiration
date. There is no way to add a new user id to an existing key. Boo.

The second problem was when we tried to search for keys on a public key server.
The search interface only allows for searching by key id (as opposed to user id
- which would allow you to search for someone's key by entering their email
address).

A third problem is that, when signing a key, it forces you to sign all user ids
on the key, rather than allowing you to pick the ones to sign. Furthermore, it
doesn't list all user attributes (a photo attribute on a test key we were using
did not show up at all). And worst of all - it signs the photo attribute that
is not displayed when you sign the key!

A fourth problem is that there doesn't seem to be a way to revoke your own key
or a key signature on another key.

### [Enigmail](http://enigmail.mozdev.org/home/index.php)
 
Enigmail is a Thunderbird extension, with amazing
[documentation](http://enigmail.mozdev.org/documentation/index.php) and a very
well done user interface. We would have started with Enigmail, but it has the
draw back of requiring Thunderbird. Enigmail does allow one to add a new email
user id to a key (but no way to add a different user id, like a photo).
Enigmail allows you to search public key servers for keys based on an email
addresses. 

On the signing side of things - if you right click an un-expanded key and
select sign, Enigmail will sign all user ids. Worse, Enigmail allows you to
expand a key to see the user ids, allowing you to right click on a single user
id and select sign. Unfortunately, that operation also signs all keys.

Enigmail does allow you to revoke your own key, but no way to revoke a
signature.

### [WinPT](http://winpt.sourceforge.net/en/)

WinPT could be good, but we don't have a Windows machine to test it on.
Furthermore, the [last news
item](http://sourceforge.net/forum/forum.php?forum_id=464307) on the web site
is a request for a new project lead (and is dated 2005-05-02). The [last release](http://sourceforge.net/project/showfiles.php?group_id=71360)
seems to be dated 2003-04-23.

### [Gpg4win](http://www.gpg4win.org/)

Gpg4win provides a single package of a bunch of the gpg binaries for windows
(including both WinPT and GPA), however, there are no new GUI key management
programs offered. 

### [MacGPG](http://macgpg.sourceforge.net/)

MacGPG allows you to search by email address. However, you cannot add a user id
from within the GUI (it opens up a terminal window with gpg running the
-edit-key command). In addition, when trying to sign a key, MacGPG opens a
terminal window prompting you to sign all user ids. When you click a user id
and try to sign it, nothing happens.

### Conclusions

Wow. We're still a ways off here. All the packages do allow for basic key
management operations (create a new key pair, verify a fingerprint, export a key). However, many fairly important functions are missing from all GUIs we looked at.

## Using Removable Disk 

We started with a Fat32 formatted USB key with the idea that it could be easily
mounted on the most common operating systems in use today.

We ran into a failure when using gpg to create a private/public key pair. The
failure happened when trying to hard link a temporary file (hard links are not
supported on Fat32). This problem might not happen if we were using a gpg
compiled for Windows (but we didn't have a Windows machine to test with). We
solved the problem by formatting the USB key with ntfs. Yuck.

Then, we ran into a problem trying to search for a key on a public key server -
we received an error regarding file permissions of our gpg directory. Windows
NT on Linux makes all files owned by root and world readable and writable.
Double Yuck.

