---
tags: ["another-internet","mayfirst"]
title: "Internet Rights Workshops the NYC Grassroots Media Conference"
date: 2008-01-08T09:32:44.149780
---

The results are in. 

On March 2, 2008, [May First/People Link](http://mayfirst.org/) organized [a
session to collaboratively develop a series of Internet
Rights](https://support.mayfirst.org/wiki/internet_rights_gmc_2008) at the
[Grassroots Media Conference](http://nycgrassrootsmedia.org/).

We ran the same workshop at the [2007 US Social Forum](http://ussf2007.org/)
with [different, but similar results](https://www.ussf2007.org/en/node/17107).

At the GMC, we had less time, which seems to be reflected in the more raw
wording. Also of note - none of the rights were endorsed by all groups (the
number of endorsers are in brackets). We had a total of seven groups. And, of
course, due to the nature of the project, there was some last minute back and
forth - as you might imagine the last right was rather contentious!

1. Freedom of expression. [5] 

1. The right to space, hardware, software, and non-restricted use of existing
and future internet technology, including the right to not use a technology.
[4]

1. Labor rights for internet workers and technology produced with priorities of
ecological sustainability, labor justice and respect of community land
(production and disposal). [3] 

1. The right to a participatory governance process of the internet, including
those who are not yet online. [3] 

1. Free, equitable, and open access to the internet. As well as the codified
right to not participate. [3] 

1. Moderation against libel, slander, and defamation through the right to
rebuttal. [2] 

1. Right to privacy, and anonymity in all network based activity. [2]

1. The right to a domain name that is short descriptive and memorable,
including equal-opportunity indexing. Non-fee-based promotion and
searchability. Freedom from commercialization and speculation. [1]

1. Internet service provided by multiple, independent providers who compete
vigorously and offer access to the entire internet over a broadband connection,
with freedom to attach within the home any legal device to the net connection
and run any legal application. [1]

1. The right for communities to enforce standards/values via censorship.
[1]

