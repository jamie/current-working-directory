---
tags: ["grut2008","mayfirst","another-internet"]
title: "Grassroots Use of Technology Conference 2008"
date: 2008-01-08T09:32:44.149780
---

We had another amazing [Internet Rights
workshop](https://support.mayfirst.org/wiki/internet_rights_workshop)
- this one at the 2008 [Grassroots Use of Technology
Conference](http://organizerscollaborative.org/conference/).

We had four groups of about four people each - a much smaller gathering than in
the past. We also started the session 15 minutes late and old had an hour and
15 minutes to start with. Unfortunately we had to cut the discussion at the
end.

On the positive side, we partnered with the [Boston Action
Tank](http://www.mediaactioncenter.org/?q=ActionTank). They ran the session
immediately after ours. Taking the number one right (we ended up with two
rights tied for first place - we chose the right to govern) we did a power
analysis to determine what pre-conditions we need to achieve the right and who
are our allies in the struggle. We didn't have enough time to come out a
publishable consensus - however, it was a useful and enlightening step to take
with the rights.

And the rights are ... 

1. First Amendment Rights shall extend to all online communications. [4]

1. All users have the right to govern the Internet as a commons that allows participation and access for all. [4]

1. Keep the information as private as we want. Levels of privacy should be dictated by the users. [3]

1. Transparency about web site and network ownership. [3]

1. Universal availability of relevant tools and training for full participation in the digital environment [3]

1. Universal, free access to the Internet for everyone. [2]

1. Information sent from a "sending" machine should not be edited or obstructed in their transit to the intended "receiving" machine. [2]

1. All users have the right to form and self-govern online communities. [2]

1. Right to accountable name-anonymous access to the Internet. [2]

1. All users have the right to protect their data and transmissions from spying or editing. All users have the right to refuse to surrender their protection methods to individuals, organizations or governments. [1]

[The numbers in brackets are the number of groups that endorsed the right. 4
groups total.]
