---
tags: ["mayfirst","gutc2008"]
title: "Look for us in Boston at the Grassroots Use Of Technology Conference"
date: 2008-01-08T09:32:44.149780
---

The [Grassroots Use of Technology
Conference](http://organizerscollaborative.org/conference/) is coming up!

May First/People Link members will be there in force.
[Ross](http://ross.mayfirst.org/), [Agaric Design](http://agaricdesign.com),
[Alfredo](http://blogs.mayfirst.org/blog/6) and yours truly will be teaming up
to present the [Internet Rights
Workshop](https://support.mayfirst.org/wiki/internet_rights_workshop) at the
[1:30 pm breakout
session](http://organizerscollaborative.org/conference08/agenda). 

Even more exciting - we're partnering with the [Boston Action
Tank](http://www.mediaactioncenter.org/?q=ActionTank). They're session will
happen directly after ours and will pick up where ours leaves off. Be sure to
go to their 3:00 pm session (Strategies for shaping the media/tech future) - to
figure out how we will realize the Internet Rights we come up with in the
earlier session.
