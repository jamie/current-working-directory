---
title: "Khalil Gibran International Academy"
tags: ["mayfirst"]
date: 2008-01-08T09:32:44.149780
---

Thanks for the work of [May First/People Link](http://mayfirst.org) member
[AWAAM](http://awaam.org), I've been following the story of Khalil Gibran
International Academy, a school that opened in 2007 in Brooklyn to serve as a
dual-language Arabic/English school where a mixed group of Arabic speakers and
non-Arabic speakers would learn together.

The story of the school's opening and the original principal getting pushed out
because she acknowledged that "intifada" was a word with meaning and history
is tragic. However, that was just the beginning. Seth Wessler has written [an
article](http://colorlines.com/article.php?ID=456) documenting the continued
impact of a conservative campaign to undermine the school with the latest
twist: pictures of mosques being cut out of the text books.
