---
title: "Getting Logitech camera working with Linux"
date: 2008-01-08T09:32:44.149780
---

Josue gave me a USB camera made by logitech - and I've been meaning to get it
working with my Debian Lenny system for a while.

I started by plugging it in and watching syslog:

		Aug 14 14:13:08 liberace kernel: [128348.749001] usb 3-1: USB disconnect, address 4
		Aug 14 14:13:12 liberace kernel: [128352.657450] hub 4-0:1.0: unable to enumerate USB device on port 5
		Aug 14 14:13:12 liberace kernel: [128352.894212] usb 3-1: new full speed USB device using uhci_hcd and address 5
		Aug 14 14:13:13 liberace kernel: [128353.093083] usb 3-1: configuration #1 chosen from 1 choice
		Aug 14 14:13:13 liberace kernel: [128353.119601] usb 3-1: New USB device found, idVendor=046d, idProduct=08a9
		Aug 14 14:13:13 liberace kernel: [128353.119610] usb 3-1: New USB device strings: Mfr=0, Product=2, SerialNumber=0
		Aug 14 14:13:13 liberace kernel: [128353.119614] usb 3-1: Product: Camera


So far so good. I was hoping to see some reference to /dev/video, but that
wasn't part of the output and the device wasn't created.

Next I tried:

		aptitude install qc-usb-utils

Still nothing. Then, I noticed that apt-cache show qc-usb-utils says: "These
programs are completely useless without a qc-usb-modules package."

There is no qc-usb-modules package (yes, [Debian seems aware of that
problem](http://qa.debian.org/debcheck.php?dist=sid&package=qc-usb)).

But there is a qc-usb-source package. I installed that package and then ran:

		m-a

That's module-assistant - which makes building a kernel module a snap.

After running it, I had to do some digging to learn that the module it built is
called quickcam (I tried modprobe qc-usb to no avail). The following
successfully loaded the module:

		0 jamie@liberace:linux$ sudo modprobe -v quickcam
		insmod /lib/modules/2.6.25-2-686/drivers/media/video/quickcam.ko 
		0 jamie@liberace:linux$

But, unplugging and re-plugging the camera produced the same output in syslog
and no /dev/video.

More web searching finally turned up gspca. I removed the qc-usb modules and
then:

		aptitude install  gspca-modules-2.6.25-2-686

Followed by:

		 modprobe -v gspca

And, to load in on my next boot:

		echo "gspca" >> /etc/modules

And now, miraculously, Ekiga recognizes and uses my camera!
