---
tags: ["mayfirst", "email", "spam"]
title: "Email Scam"
date: 2008-01-08T09:32:44.149780
---

I received a rightfully concerned email from a group asking if I was the one that made the newegg purchase referenced in the email. The email (as displayed in my text mail program) looked like this:

		From: NewEgg Support
		To: info@xxxxx.org
		ReplyTo: lackey@deadbeats.com
		Sent: Jun 5, 2008 3:20 PM
		Subject: You order in process!

		Good day, info!

		Thanks for you order!
		ASUS 20X DVDąR DVD Burner with LightScribe Black SATA Model DRW-2014L1T - Retail $919.45

		You can check order status at the folowing link:
		http://www.newegg.com/mi?l=I6WC70R53692IL9KF74YV52213EL9N975LD3QS <http://www.newegg.com.id130UOZ27719H368VP0.102354124.cn/sen/index.php>

		Best regards,
		Support NewEgg.

My response via email got canned as spam. Sigh. So here it is via the web:

		I think this is an email scam.

		The link in the email that you forwarded is:

		http://www.newegg.com/mi?l=I6WC70R53692IL9KF74YV52213EL9N975LD3QS
		+<http://www.newegg.com.id130UOZ27719H368VP0.102354124.cn/sen/index.php>

		The first part looks like a legit newegg.com URL. The second part (in angle
		brackets) is typically the part that you will be re-directed to if you click
		on the link. It specifies a different server:

		http://www.newegg.com.id130UOZ27719H368VP0.102354124.cn

		The .cn top level domain belongs to china. If you go to that page, it
		redirects you to a different page:

		http://106384234523.cn/ulp/check.php

		That pages displays nothing (but, based on the name, might record the details
		of your IP address or browser).

		The web page http://106384234523.cn seems to belong to a consulting firm.

		I would suggest calling newegg.com to report the email and to ensure that
		nothing was actually billed - but the whole things seems pretty fishy to me.

		Jamie
