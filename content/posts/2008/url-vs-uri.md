---
title: "URL vs URI"
date: 2008-01-08T09:32:44.149780
---

Every couple years I scratch my head and wonder what's the difference between a
URL and URI is. I inevitably google [url vs
uri](http://www.google.com/search?q=url%20vs%20uri) and run through the first 5
hits or so and, after 15 minutes of reading explanation that don't help, give
up. I finally found an
[explanation](http://www.w3.org/TR/uri-clarification/#uri-partitioning) that
makes sense. Nothing like history to explain the present.
