---
tags: ["mayfirst","amc2008","security"]
title: "Riseup and Texas MEP on Social Networking"
date: 2008-01-08T09:32:44.149780
---

[Riseup](http://riseup.net/) and [Texas MEP](http://www.texasmep.org/) put
together a workshop on social network from a critical perspective. It was
interesting to watch the tensions within the presentations.  Texas MEP were
full-on Facebook/My Space/etc. users. While being conscious of the risks, their
position was that they are careful to only put on information that is public.

Brenna from Riseup demo's [Riseup's installation of
crabgrass](http://we.riseup.net/) - a social networking site designed from a
collective/organizing perspective rather than an individuated perspective. She
provided much of the critical analysis of the risks involved with corporate
social networking sites (lack of privacy, reliability problems, etc.).

Brenna also provided Riseup's 5 horseman of the privacy apocalypse:

1. relational surveillance: analysis of social networks via email and phone
transactions (by the government) relational-surveillance

2. data profiling: the aggregation of consumer data in order to build detailed
profiles on the consumption habits of everyone. data-profiling

3. tethered computing: devices that are controlled via a ‘tether’ by the
manufacturer. On the desktop, trusted computing can be seen as a way of
achieving tethered computing on an otherwise agnostic and innovative device.
(by corporations and the government). tethered-computing

4. Geo spacial surveillance: location tracking via RFID, cell phones, IP
addresses (by corporations) Geo spatial-surveillance

5. biometric surveillance: biometric scanning via CCTV face recognition, DNA
databases. biometric-surveillance

That alone made the workshop worth it. I think we struggle a lot to figure out
how to communicate security concerns. The organization of these concerns -
specifically the way these 5 issues are abstracted from the specific
applications - is really helpful.

We had some good discussion - one person mentioned how she's uncomfortable with
publishing our networks on corporate run servers. 

The parting words of the workshop: We're not just fighting to get our media
out, but fighting to build and own the infrastructure. 
