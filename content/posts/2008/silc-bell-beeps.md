---
tags: ["silc"]
title: "Setting Bell Beeps in SILC"
date: 2008-01-08T09:32:44.149780
---

In the best tradition of "quick write it down before I forget it again" I am
blogging about how to enable two people running [silc](http://www.silcnet.org/)
to send each other a beep. Both users should be running Linux and the silc
terminal client. And, both users should have beeping configured on their
computer (you can test by opening a terminal window and typing Ctrl-g and you
should hear a beep).

Then - in silc, turn bell_beeps on with:

  /set bell_beeps ON

Now, if you are both in the same silc room, typing: CTRL-g followed by pressing
the enter key should cause your computer to beep and the computer of everyone
in the silc room (provided their silc configuration has bell_beeps set to ON as
well) to beep as well.

Although I'm a big hater of beeping, I find this setup really useful because it
allows me to to have a silc window open and buried or otherwise not visible,
yet still allow people to get my attention.
