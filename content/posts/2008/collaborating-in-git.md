---
tags: [ "git" ]
title: "Collaborating with git"
date: 2008-01-08T09:32:44.137780
---

In a [previous blog](/posts/2008/490/) I reviewed the basic steps required to
setup a [git](http://en.wikipedia.org/wiki/Git_%28software%29) environment
designed to share with others. After following the steps in that initial post,
you should end up with: a initial developer who has a local repository and a
remote, public repository and a second developer who has cloned the initial
developer's repository and published their own repository with (if they so
desire) changes to the original code submitted by the initial developer.

In this post I want to cover the following logical next scenario: how does the
original developer review changes published by the second developer and
incorporate them?

In addition to printing generic commands, I'm also including real-working
examples from a project called mfpldues (which is a very simple project
providing links to Paypal for people to use to pay their [May First/People
Link](http://mayfirst.org) dues online).

The initial developer is me and has my public repository is published at:

http://current.workingdirectory.net/projects/mfpldues

The second developer is dkg and has his public repository published at:

http://lair.fifthhorseman.net/~dkg/git/mfpldues.git

The first step is that I need to know about dkg's public repository (in this
case it was published on May First/People Link's [issue tracking
system](https://support.mayfirst.org/ticket/527#comment:5)).

With this information, I can add the repository using the remote add command:

		git remote add dkg http://lair.fifthhorseman.net/~dkg/git/mfpldues.git

The "dkg" part is me assigning the repository a nickname that I will recognize.

I can check what remotes I've added with:

		git remote

Next, I can pull in dkg's repository with:

		git fetch dkg

Then, I can see what differences dkg has done with:

		git diff dkg/master

The git diff commands is comparing what is currently checked out (my local
repository) with the dkg/master branch. "master" is the default branch, and
dkg/master means the master branch in the dkg remote.

Next, I might want to switch to dkg's branch to see if it works for me:

		git checkout dkg/master

I can always switch back with:

		git checkout master

At this point, I decide I like dkg's changes, so I want to merge them into my
master branch.

		git merge dkg/master

This will pull in the changes and commit them to my local repository (maybe
adding --no-commit would be a better practice). If all is working and nice,
then I publish to my public repo with:

		git push

All done!
