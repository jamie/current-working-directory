---
title: "The pain of installing custom ROMs on Android phones"
tags: ["debian", "mobile"]
date: 2018-04-30T09:46:16.778301
---

A while back I bought a Nexus 5x. During a [three-day ordeal I finally got Omnirom installed](/posts/2016/trusted-mobile-device/) - with full disk encryption, root access and some stitched together fake Google Play code that allowed me to run Signal without actually letting Google into my computer.

A short while later, Open Whisper Systems released a version of [Signal](https://signal.org) that uses Web Sockets when Google Play services is not installed (and allows for installation via a web page without the need for the Google Play store). Dang. Should have waited. 

Now, post Meltdown/Spectre, I worked up the courage to go through this process again. In the [comments](/posts/2016/trusted-mobile-device/#comment-ca5ce67c58646175148ae37f3ecbc724) of my Omnirom post, I received a few suggestions about not really needing root. Hm - why didn't I think of that? Who needs root anyway? Combining root with full disk encryption was the real pain point in my previous install, so perhaps I can make things much easier. Also, not needing any of the fake Google software would be a definite plus.

This time around I decided to go with [LineageOS](https://lineageos.org/) since it seems to be the most mainstream of the custom ROMs. I found [perfectly reasonable sounding instructions](https://wiki.lineageos.org/devices/bullhead/install).

My first mistake was skipping the initial steps (since I already had TWRP recovery installed I didn't think I needed to follow them). I went straight to the step of installing LineageOS (including wiping the Cache, System and Data partitions). Unfortunately, when it came time to flash the ROM, I got the error that the ROM is for bullhead, but the hardware I was using is "" (yes, empty string there).

After some Internet searching I learned that the problem is an out-dated version of TWRP. Great, let's upgrade TWRP. I went back and started from the beginning of the LineageOS install instructions. But when it came to the `fastboot flashing unlock` step, I got the message explaining that my phone did not have OEM unlocking enabled. There are plenty of posts in the Internet demonstrating how to boot into your phone and [flip the switch to allow OEM unlocking from the Developer section of your System tools](https://www.androidsage.com/2017/06/14/what-is-oem-and-how-to-enable-oem-unlock-on-any-android-device/). Great, except that I could no longer boot into my phone thanks to the various deletion steps I had already taken. Dang. Did just brick my phone?

I started thinking through how to buy a new phone.

Then, I did more searching on the Internet and learned that I can flash a new version of TWRP the same way you flash anything else. Phew! New TWRP flashed and new LineageOS ROM installed! And, my first question: what is the point of locking your phone if you can still flash recovery images and even new ROMs?

However, when I booted, I got an "OS vendor mismatch error". WTF. Ok, now my phone is really bricked.

Fortunately, someone not only identified this problem but contributed an [exceptionally well-written step-by-step set of directions to fix the problem](https://gist.github.com/MacKentoch/48ad6b91613213ee9774c138267e2ed4). The post, in combination with some comments on it, explains that you have to [download the Google firmware](https://developers.google.com/android/images) that corresponds to the error code in your message (in case that post ever goes away: unzip the file you download, then cd into the directory created and unzip the file that starts with image-bullhead. Then, minimally flash the vendor.img to the vendor partition in TWRP).

In other words, the LineageOS ROM depends on having the right Google firmware installed. 

All of these steps were possible without unlocking the phone. However, when I tried to update the Radio and Bootloader using:

    fastboot flash bootloader bootloader-bullhead-bhz11l.img
    fastboot flash radio radio-bullhead-m8994f-2.6.37.2.21.img

It failed, so I booted into my now working install, enabled OEM unlock, unlocked the phone (which wiped everything so I had to start over) and then it all worked.

And, kudos to LineageOS for the simple setup process and ease of getting full disk encryption.

Now that I'm done, I am asking myself a few questions:

I have my own custom ROM and I am not trusting Google with everything anymore. Hooray! So ... who am I trusting? This question I know the answer to (I think...):

 * [Team Win](https://twrp.me/), which provides the TWRP recovery software has total control of everything. Geez, I hope these people aren't assholes.
 * [Google](https://google.com), since I blindly install their firmware vendor image, bootloader image and radio image. I guess they still can control my phone.
 * [Fdroid](https://f-droid.org/), I hope they vette their packages, because I blindly install them from their default archives.
 * [Guardian Project](https://guardianproject.info/), since I enable their fdroid repo too - but hey at least I have met a few of these people and they are [May First/People Link](https://mayfirst.org/) members.
 * [Firefox](https://mozilla.org), I download firefox directly from Mozilla since fdroid doesn't seem to really support them.
 * [Signal](https://signal.org), since I download that APK directly as well.
 * And the https certificate system (which pretty much means [Let's Encrypt](https://letsencrypt.org/) nowadays - since nearly everything depends on the integrity of the packages I am downloading over https.

But I'm still not sure about one more question:

Should I lock my phone? Given what I just accomplished without locking it, it seems that locking the phone could make my life harder the next time I upgrade and doesn't really stop someone else from replacing key components of my operating system without me knowing it. 
