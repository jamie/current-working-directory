---
tags: ["mayfirst", "security"]
title: "WikiLeaks offline"
date: 2010-01-08T09:32:44.185780
---

I saw the keynote speech by one of the key technologists and organizers of
WikiLeaks at the last [Hackers on Planet Earth](http://thenexthope.org)
conference. Although the talk was mostly political, there was enough techie
talk about encryption and anonymity that I assumed WikiLeaks web infrastructure
was ready for any kind of attack.

[Apparently
not](http://arstechnica.com/security/news/2010/12/wikileaks-kicked-out-of-amazons-cloud.ars).
All the encryption in the world doesn't help you if you are hosted in the
Amazon Cloud or, for that matter, with any host who doesn't care for your
politics.

To their credit, WikiLeaks moved to Amazon because a technical denial of
service attack took down their previous Swedish host (I don't imagine that they
moved without a good reason). However, essentially they traded one form of
denial of service for another one.

Today WikiLeaks encountered a new form of censorship that should make all of us
shudder. Rather than being shutdown at the web hosting level, [EveryDNS
shutdown the wikleaks.org domain
name](http://news.netcraft.com/archives/2010/12/03/wikileaks-ch-goes-down-as-everydns-pulls-the-plug-again.html). 

Unlike most aspects of the Internet, the domain name system is hierarchical.
There is pyramid - with a limited number of Domain name registrars (just "over
500" according to
[Wikipedia](http://en.wikipedia.org/wiki/Domain_name_registrar)) that control
all the domain names in the world.  When you type a domain name, like
wikileaks.org, into your web browser, that domain name must be translated into
an IP address that is used to route your request to the correct server. The 500
or so registrars control this process.

So what can you do?

Wikleaks responed by registering [wikileaks.ch](http://wikileaks.ch) ([woops!
shutdown as
well](http://news.netcraft.com/archives/2010/12/03/wikileaks-ch-goes-down-as-everydns-pulls-the-plug-again.html)), [wikileaks.de](http://wikileaks.de/),
[wikileaks.fi](http://wikileaks.fi/), and [wikileaks.nl](http://wikileaks.nl/).

That's a good start. But what if there were more?  Here's an idea. What if
everyone who controlled a domain name volunteered a subdomain for
WikiLeaks? For example: [wikileaks.mayfirst.org](http://wikileaks.mayfirst.org/). Just create an A record that points to the IP address 88.80.13.160.

If WikiLeaks has to change providers (and therefor their IP address again), our
subdomain won't work until we update it. On the other hand, seems like a good
way for us all to really pitch in and share the risk that the folks at
WikiLeaks are taking all by themselves. And, if the IP address changes,
WikiLeaks only needs to leave behind a simple page on the old IP with a
redirect to the new one. 

Any takers?

-------

Update: 2010-12-05

The [UK
Guardian](http://www.guardian.co.uk/media/2010/dec/05/wikileaks-internet-backlash-us-pressure)
has picked up this idea. Also - check out [Paul Carvill's
blog](http://www.paulcarvill.com/2010/12/opposing-government-and-corporate-censorship-of-the-web/)
where he documents the [imwikileaks twitter tag detailing many others following on this and
similar ideas](http://search.twitter.com/search?q=imwikileaks) and posts a
[link to WikiLeaks web site with directions on how to mirror their
content](http://213.251.145.96/mass-mirror).

It's great to see WikiLeaks reaching out and asking for help from the community
- there is clearly a huge number of us that want to offer our support. 

And, I'm reasonably confident that the IP addresses 213.251.145.96 and
88.80.13.160 are under the control of WikiLeaks so we're not spreading false
information. 

However, if you work with a political organization that is currently not under
fire, now is a good time to consider publishing some form of a public
cryptography key so if you are under attack in the future, people can verify
this kind of information. Since news travels and is repeated so quickly on the
Internet, it would not be hard for someone to post an "official" IP address for
WikiLeaks that doesn't belong to them.

This episode prompted us at May First/People Link to [publish two OpenPGP
keys](https://support.mayfirst.org/wiki/email_announcement_lists#Signatures).
We work hard to digitally "sign" every piece of official May First/People Link
information with one of these two keys, or with a key that is certified by one
of these two keys. This approach provides everyone with the ability to verify
that a piece of information supposedly sent by us really was sent by us. 

