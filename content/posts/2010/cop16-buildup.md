---
tags: ["mayfirst","cop16"]
title: "COP16 is coming soon"
date: 2010-01-08T09:32:44.185780
---

[COP16](http://cc2010.mx/es/), the meeting of the United Nations Framework Convention on Climate Change and the Kyoto Protocol, is coming up in early December.

The events taking place during these two weeks in Cancun, Mexico are mind
boggling. There are at least two full scale paralell forums
([Kimaforum](http://klimaforum10.org/) and [Diálogo
Climático](http://www.dialogoclimatico.org/)), [caravans are arriving from
around the
world](http://www.globalexchange.org/blogs/climatejustice/2010/11/22/updates-on-the-caravans-to-cancun/),
and [La Via Campesina](http://www.viacampesina.org/)  is calling for [1000
Cancuns](http://www.viacampesina.org/en/index.php?option=com_content&view=article&id=972:thousands-of-cancuns-for-climate-justice-&catid=50:thousands-of-cancun-for-climate-justice&Itemid=195)
to take place around the world.

May First/People Link is working with our member Grassroots Global Justice and
the Grassroots Solutions for Climate Justice - North America to build out a web
site to help bring together as much of the information about the events as
possible. 

Stay tuned for the launch, coming soon!
