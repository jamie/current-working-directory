---
tags: ["mayfirst"]
title: "A Response to Mark Engler's 'The Limits of Internet Organizing'"
date: 2010-01-08T09:32:44.185780
---

Mark Engler's customary insightful analysis is glaringly absent from his
[recent piece on Internet organizing in Dissent
Magazine](http://dissentmagazine.org/atw.php?id=278).

Engler frames his piece with:

> I will state up front the conclusion that almost all articles of this sort
> come to: the Internet is a tool. It is potentially a rather useful tool,
> but it is not more than that, and it is no substitute for person-to-person
> organizing.

> This conclusion is the correct one. Still, it never seems to quell the
>  high-tech evangelists or to sink in with the public at large.

Forget the high-tech evangelists - let's stick with the public at large. Why
doesn't it sink in with us? 

Engler devotes one paragraph to this question:

> Partially, I think this is a product of the widespread failure, outside of
> social movement circles, to understand what organizing really is. You can
> raise money on-line, you can widely disseminate information, and you can
> get people to sign a petition. But it’s very difficult and rare to be able
> to use the Internet to build the types of deep relationships that move
> people to make serious commitments to and sacrifices for social movements. 

Inside social movement circles, we've perfected our ability to criticize each
other for not being "real" organizers. This debate is as tiring as it is
unproductive. What if we broadly define organizing as affecting what people do
and how they interact with each other? From this perspective, technology has
historically had an enormous impact on how people organize and are organized.
The automobile, mass transit, television, and many other new technologies have
had a fundamental impact on our organization. The Internet, in terms of its
impact on society and how we organize, is no different.

Perhaps that's why the "public at large" has such a hard time believing that
the Internet is just another tool - we have experienced the huge impact the
Internet has had on our lives. Unlike the "best organizers" Engler knows, who
"hardly have time to check their e-mail," most of us are not afforded that
luxury. Checking our email, something unheard of 20 years ago, is not something
we do when we simply feel like it. It's a necessity. It's the way we do our
jobs. It's how we are staying in touch with our friends and family.

And, unlike the other major technology changes of the 20th century, the
Internet is based in democratic communication, affording us the ability to
exchange ideas with any one of the Internet's nearly 2 billion users. That
capability is intoxicating for the public at large, intoxicating enough that we
suffer through incredible amounts of frustration and often pain learning
something new and dealing with difficult new protocols, both technical and
social.

This mass movement of the world's population to the Internet, in a quest to
communicate and connect with others, hasn't been missed by the corporate class.
Facebook, Twitter and others, are investing millions of dollars in an effort to
profit from this movement, control the Internet so it can more predictably
produce profits, and restrain it's open and democratic nature.

Engel's piece not only fails to address this dynamic, but he dangerously
equates the Internet with these corporations, depoliticizing the Internet as if
it were just a hammer. Unlike a tool, the Internet is a fast-developing
universe of political struggle. We are engaged in constantly shifting battles
over whether or not the software that will increasingly control our lives
should be free and open or owned by a for-profit company; who should control
and have access to our data; whether our communications should be accessible to
our governments; whether our ability to communicate is a right or a privilege.

Engel's take on these struggles seems to be: "Don't worry about it, go back to
the phone bank."

How the left should use the tools of the Internet is a fine discussion - one
the left is and should continue to have. However, more critical to the future
of our movement, is: what is our role in the development of Internet? How do we
support and develop the revolutionary potential in the Internet? How will the
Internet shape our lives in the future? If we choose to fully engage in this
debate, put our resources behind our convictions and principles, we will have a
very different future.
