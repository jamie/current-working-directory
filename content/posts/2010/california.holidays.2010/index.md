---
tags: ["family"]
title: "California Holidays 2010"
date: 2010-01-08T09:32:44.185780
---

The trip started out great. Taking a walk near the house...

![Neighborhood Walk](/posts/2010/california.holidays.2010/neighborhood.walk.jpg "Taking a walk near the house")

... Meredith in the backyard

![Meredith in the Backyard](/posts/2010/california.holidays.2010/meredith.backyard.jpg "Meredith in the backyard")

We even took a walk on the beach

![Meredith at the Beach](/posts/2010/california.holidays.2010/meredith.beach.jpg "Meredith at the Beach")

Taking our chances with the tide.

![Jeff and Mom the Beach](/posts/2010/california.holidays.2010/jeff.mom.beach.jpg "Jeff and Mom at the Beach")

Breaking the law (the beach we walked on wasn't exactly open)

![Breaking the law](/posts/2010/california.holidays.2010/breaking.the.law.jpg "Breaking the law")

After our flight was rescheduled for wednesday, we thought we'd rent a car. We were never going to get this one.

![The rental car we didn't get](/posts/2010/california.holidays.2010/rental.car.jpg "The rental car we didn't get")

After our flight took off from LA and then turned around and landed in LA, we
decided to go with Amtrak. 

![Reading the train brochure](/posts/2010/california.holidays.2010/train.brochure.jpg "Reading the train brochure")

We spent a lot of time looking out the window, particularly in the south west.

![Looking out the window](/posts/2010/california.holidays.2010/train.window.jpg "Looking out the window")

We splurged for the LA - Chicago leg - an actual sleeper car.

![Sleeper car](/posts/2010/california.holidays.2010/sleeper.jpg "Sleeper car")

Six hour layover in Chicago. If you wait long enough you can find an open bathroom in the fanciest of places.

![Fancy Hotel](/posts/2010/california.holidays.2010/chicago.fancy.hotel.jpg "Fancy Hotel")

You can also see expensive public art...

![Expensive Public Art](/posts/2010/california.holidays.2010/chicago.face.jpg "Expensive Public Art")

... and inexpensive public art...

![Puppet Bike](/posts/2010/california.holidays.2010/bicycle.puppet.jpg "Puppet bike")

