---
tags: ["mayfirst","cmpcc"]
title: "Web casting and the Climate Change Conference in Cochabamba, Bolivia"
date: 2010-01-08T09:32:44.185780
---

One of my first experiences as an activist happened while I was a teenager - I
went door to door surveying my neighbors in Lincoln, Nebraska to determine
interest in recycling. My small contribution made me feel part of a movement,
an experience that helped propel me through a life time of organizing and
activism. Since then I have been heavily involved with both alternative media
and free Internet issues - following a path toward a more global and
revolutionary movement than I found in the environmental circles of the 80's in
Nebraska. 

Now, twenty years later, the issues of the environment, media, technology and
revolutionary change are coming together in a global movement unlike anything I
could have ever imagined.

##  Global Conference on Climate Change: Cochabamba, Bolivia, April 2010 

In March, May First/People Link (MFPL) was contacted by Nick, the Media Liaison
for the [Global Conference on Climate Change in
Cochabamba](http://cmpcc.org.bo/). He invited us to participate on the
technology team supporting the event. After some back and forth, we received
our assignment: provide a free/open source live Internet video stream of the
proceedings. Our contribution would compliment the non-free software based live
stream provided by the [One Climate](http://www.oneclimate.net/) team. Taking
no chances, the conference organizers wanted to ensure that we would have a way
for people outside Bolivia to experience the event - even if it meant having
two teams working on the same issue. Furthermore, the lead tech organizer
wanted to showcase free and open source software during the event. 

We immediately put our team together: Mallory, Maritza and I were tasked with
going to Bolivia to provide direct support. Greg, Nat, and Daniel provided on
the ground server support from New York. And Alfredo was in charge of
organizing an international event to support the conference: an Internet-based,
multi-city, interactive, video/audio conference connecting delegates at the
conference with people from around the world who could not be there in person.
Unlike the video stream - which would be more like watching TV, this event was
designed to allow two-way video and audio interactions between all
participants.

## Web casting 

### What is Web casting?

Most of us have watched video online - typically we stumble upon a web site
with a box and play button. This technology allows us to play video on demand -
meaning that when we, the viewer, control when it starts, etc.  This type of
video watching requires that the video itself is pre-recorded.

Live video web casting, on the other hand, means watching a video portraying an
activity that is happening live. Nothing pre-recorded; nothing on demand. We
lose the ability to control when we watch it; however, we gain the experience
of watching something together, with the rest of the world.

The setup for web casting involves: 

* A cable providing a live video signal and another cable providing a live
audio signal (much like the cables you might use to connect your VCR or DVD
player to your TV). These cables can come from a video camera shooting a live
event, or a video switcher in a TV studio, or even a Television.

* These cables are plugged into a computer that converts the signal into a
digital format.

* That computer sends the signal, over the Internet, to a server accessible to
the world.

* One (or more) web sites include the familiar box and play button that, when
clicked, connects the user's browser to the server, displaying the video
stream.

### Our first steps

In the airport in Miami, we first heard the news of the volcano eruption in
Iceland that was disrupting all European travel. Later it was confirmed: the
One Climate folks, based in Europe, were not going to be able to make it - web
casting was up to us entirely.

![Web team](/posts/2010/cmpcc/webteam-cochabamba.jpg "Web team - Mallory, Karen, Jonathan, and Daniel")

Upon arrival in Bolivia, we went to the conference site at Valley University in
Tiquipaya, 30 minutes from Cochabamba. We were introduced to the local tech
team  - organized by Daniel Viñar, whose international nature mirrored the
experience of the conference: Uruguayan by birth, his parents were exiled in
France, where he grew up until he moved to Bolivia in the nineties. His team
came from Paz, including paceños Freddy, Victor, Oliver, Armin, Mario,
Jonathan, French ex-pats Clement, and Sylvain, and Ivan (from La Paz, studying
in Peru).

In addition to us, the tech team was joined by a disparate group of web editors
from around the world, some organized by Nick, others from the New York
Bolivian mission (which brought 30 delegates), and many more folks who did a
tremendous job editing, teaching and promoting the [dynamic web
site](http://cmpcc.org.bo/).

Our first decision for web casting was on the technology to use. We had a
problem. Using free software that was controlled by the conference was
non-negotiable. In addition, we preferred to use free formats and protocols.
We had tested a successful web cast that used the free ogg/theora format and
the free shoutcast protocol. The problem is that only people running a version
of the web browser Firefox greater than 3.5 (or who had already downloaded the
software needed to view ogg/theora video) would be able to view the web cast.

After some discussion, we decided that we would need to compromise on the video
format and protocol, choosing the proprietary Flash format and proprietary RTMP
protocol - both owned by Adoboe. A very large percentage of computers already
have the software installed that is needed to view videos in the Flash format.
And the RTMP protocol provides a means to send Flash videos to and from a
server running Adobe's multi-thousand dollar Flash Streaming Server.

This compromise was made possible by the free software developers who created
Red5 - a fully free and open source server software that works as a replacement
for the Adobe software. In addition, software developers have produced client
software, like Gnash, that are free and open source flash video players.

While using Red5 and Gnash is a functional compromise now (which allows people
to participate fully using only free and open source software or, if they
choose, the Adobe proprietary versions), in the long run, relying on the
proprietary Flash format and RTMP protocol places the movement in a precarious
position. At any time, Adobe can change the protocol, which could cause Red5
and Gnash to stop working seamlessly with the proprietary versions of the
software. This compromise means we are not fully in control of our software. 

Thanks to help from our NY crew who setup our servers, we had several servers,
ready to go, with which to expirement.  We immediately went to work on
Saturday, trying to wrap our heads around Red 5, which was written in a
language (Java) that nobody on site was familiar with. We finally figured out
the steps to [properly install Red5 on
Debian](https://support.mayfirst.org/wiki/install-red5#InstallRed5onyourserver),
and then, late into the night, after hours of frustration and on the cusp of
calling it a night, Mallory discovered the [magic combination of
steps](https://support.mayfirst.org/wiki/install-red5#StreamingtotheRed5Server)
that resulted in a successful stream from a web cam in Cochabamba to our server
in New York, and then back again to a web browser.  Victory! Our job was one.
Or so we thought.

We knew that we were not going to web cast using the web cams attached to our
laptops. The quality would be terrible (no ability to zoom or change angles)
and, of course, our laptops would need to be in the room where the event was
happening. Instead, our plan was to take a video feed from the Television Team
- the group responsible for broadcasting the proceedings via satellite to the
rest of the world. The TV Team already had organized multiple camera crews and
studios for this purpose.

The next day, Sunday, we began experimenting with the device that takes a video
signal (like the kind we expected to receive from the TV team) and converts it
into a digital format that we could then send to our Red5 server. The tech team
had purchased several Creative SB0630's for this purpose. This device proved to
have a fatal problem: it shipped with proprietary Windows drivers only: we
could not stream video with a computer running the free/open source Linux
operating system.

Mallory, Armin and I followed Ivan into a cab and off we went to the Concha -
the giant Cochabamba market, which was a 45 minute drive from the conference
center. After walking for close to 30 minutes, through stalls of every item
imaginable, we finally hit the electronics section, and then the computer
section. We split into two groups in search of an alternative digital video
converter. After 30 minutes, Mallory and I turned up empty handed, but Ivan and
Armin discovered two possibilities. We settled on a a an Encore. 

Back at Tiquipaya, I spent several hours trying to get our new device working
on Linux, but without luck. Like the Creative SB0630, I couldn't get Linux to
recognize the device as a video input. The 4 hours it took to get to and from
the market really cut into our work day on Sunday, and time was getting short.

Furthermore, Daniel wanted us to test our web casting capacity that night
during a pre-conference interview. In a move that would be repeated throughout
the conference, we turned to Gavinda from [Earth
Cycles](http://www.earthcycles.net/), a media maker who demonstrated an uncanny
ability to have every piece of technical equipment we needed in the last
minute. For the test, he turned up a Pinnacle video converter and a Windows
laptop with the drivers pre-installed. Although it wasn't free software
end-to-end, we nonetheless pulled together a working web stream solution just
in the nick of time. We went home happy - knowing that we could at least
produce a working web stream - and still with one day before the official
conference kicked off on Tuesday.

On Monday morning, by the time the MFPL team had arrived, Ivan had miraculously
gotten our new Encore card to work with Linux - with the help of the free
software program TV Time. This was our final break through! We quickly ran a
test and, with still half a day to go, we had a working, end-to-end free
software based video streaming system in place.

For the second time, we thought our job was done.

## Big Blue Button

Meanwhile, Alfredo was forging ahead organizing our multi-city event. 

We were participating in a historic, global environmental summit - we needed a
way for people to not only watch what was happening, but participate
interactively with the delegates.

Our plan was to use the free/open source software [Big Blue
Button](http://bigbluebutton.org/), which allows users to login to a web site,
optionally connect your web camera, microphone and speakers, and then have a
multi-person video/audio/text conference. 

Alfredo's job was to bring in the audience. Thanks to his work and the support
of the Bolivian Mission in NY, we received a tremendous amount of interest. By
the time Mallory, Maritza and I arrived in Bolivia, Alfredo had confirmed four
cities - and not only their participation, but also their commitment to
organize an audience: [The Brecht Forum in NY](http://brechtforum.org),
[Encuentro5](http://www.encuentro5.org/home/) in Boston (with on the ground
tech support from MFPL member and USSF volunteer Ross), the Venezuelan mission
in Chicago, and a collaboration between the Latin American Solidarity
Organization and Movement for Justice and Peace in Olympia, Washington.

Keep in mind: we had run limited tests from New York, but had never tried
anything like this before, either conceptually or technically.

To be safe, we organized a test-run on Sunday night. All four cities "showed
up" online and the results of the audio and video part were ... a disaster.
Fortunately, Big Blue Button provides an interactive text chat - a format that
is consistently reliable!

Via chat, we worked through all the various struggles each city had getting
their video and audio working. Finally, after close to an hour, we got everyone
working ([and documented a BBB tip sheet for a functional multi-city
conference](http://wiki.cmpcc.egob.entel.bo/index.php/BigBlueButton)). We all
agreed to login at 6:00 pm for the real event, one hour before it was scheduled
to start.

With two days before the scheduled live event (Tuesday night), the pressure was
on to recruit delegates from the conference to participate on the Bolivia side.
Four confirmed cities! And, since the conference hadn't started yet, we had no
idea who would be available! 

Maritza went to work. With the help of the Bolivian mission (via Taleigh, our
delegation leader) we started making connections.

It was slow work. On Monday we had several names and sent out many emails. By
Monday evening, we had one confirmation, a Peruvian Economist named Emil
Sifuentes.

By Tuesday afternoon, we had two more "maybes" and we started to worry about
whether Emil would be able to make it, since we had no reliable way to
confirm his participation.

Tuesday at 5:00 pm, 1 hour before our pre-event test, we learned that the
studio we reserved wouldn't be available after all and the alternate room we
had in mind didn't have an Internet connection.  Through a lot of scrambling
and last minute assistance from Clement, we finally got our alternative room
cabled and with the help of Victor, Oliver and Freddy we got a computer moved
into place. Mallory went to work configuring the audio and video while Maritza
and I scrambled to get replacement speakers for the non-working ones we had
planned to use (a university staff person eventually "borrowed" them from a
administrator's computer). At last, at 6:15 pm, we had all our technology
working - just in time for Emil to arrive - a full 45 minutes early!

With an altitude induced migraine threatening, Maritza began prepping Emil, and
then continued when Dulfredo Moyo from Accion Vida, one of our two maybes,
arrived. By 7:00, our four slots were filled when Daniel and Nick joined the
group. Maritza continued prepping our guests while I worked out the remaining
tech kinks with our cities (every city, it turned out, was using a different
computer for the live event than they did for the test on Sunday!).

Finally, at 7:30 pm we began. Maritza jumped into the role of multi-city
facilitator *and* translator - providing a seamless transition between the
conference participants and those from the other cities. Despite some technical
difficulties, and brief outages of audio and video, we successfully transmitted
introductions from each city, presentations from the conference participants on
the Bolivia side, and lastly we took four questions - one from each city. 

And... just before we ended we were joined by another city - Barcelona, Spain!

By the end of the event, we were exhausted, yet tremendously optimistic about
the potential for building more effective virtual conferences.

## Web casting continues....

Despite repeated requests, by Monday afternoon we still were missing the final
component to make our web casting system functional: the video feed from the TV
Team.

While we waited, we worked on scaling. We setup a Red5 server in Bolivia. In
addition, Sylvain contacted allies in France who setup a second Red5 server. We
did the same at MFPL (using our second collocation facility), while Mallory
figured out how to send a single video stream to all four locations at the same
time. Through some brilliant DNS wizardry (using Bind views), Daniel figured
out how to direct viewers from inside Bolivia to the Bolivian server, and send
viewers outside Bolivia to one of the three external servers.

![Web cast](/posts/2010/cmpcc/webcast-cochabamba.jpg "Web cast - Freddy (Photo by Mallory)")

Finally, Monday night, Ivan had had enough. Under his direction, we simply
moved our computer station to the room next to the main broadcasting studio and
pulled a line in.  By this time there was no live programming (the feed was a
loop of a promo) - but at last we had a working end-to-end setup - and just in
time!

Tuesday morning - panic ensued. The opening ceremony (the second most important
piece to webcast - with the closing being the most important) wasn't coming in
from the broadcast studio. Fortunately, at the last minute, the video feed we
had been waiting for, finally arrived in the tech room, just as the opening
ceremony was beginning. We quickly trucked our equipment back to the Tech room,
plugged in all the cables, and at the last minute, we had the opening going out
over the Internet.

Despite this success, the following two days were not spent rejoicing. Instead,
we had to constantly monitor the signal - as our video source (traveling over
many meters of cable) was far less reliable than we had wanted. Ivan, who had
purchased a second digital video converter, finally installed a new digitizing
station downstairs, plugging into a cable connection of Channel 7 - the state
run channel which was also covering the event. This cable connection was
provided by a satellite dish installed on the roof of the building by Entel
(the recently nationalized Telecom company) specifically for the event. This
backup allowed us to switch between the two sources, giving us far more
flexibility in handling problems.

This approach proved critical - since on Thursday the main event of the day,
the closing ceremony, was held in Cochabamba - which the Conference TV team was
not covering.  Therefore, we switched permanently to the video feed from
Channel 7, plugging in directly to one of the Entel provide TV's in the press
room.

This setup proved fairly reliable for most of the day (with the exception of
the time someone with a remote control accidentally turned of the TV). However,
our last disaster struck just as Hugo Chavez was finishing his speech:
suddenly, the TV image froze with the message: "Sin señal" (No signal).

Through a monumental mis-communication, Entel had started breaking down the
equipment for the conference, starting with the satellite dish on the roof.
With barely 15 minutes until Evo Morales was scheduled to deliver the final
speech of the closing, we were off line.

Looking around the room - Ivan, Mallory, Sylvain - I've never seen more
dejected faces in my life. Eventually, word reached Daniel - the man we all
knew as the soft spoken and gentle leader of the tech team. He demonstrated his
ability to adjust to the situation: I couldn't quite follow his Spanish, but I
was certain that I didn't want to be the Entel employee at the other end of
that conversation.

Although his words couldn't bring back the satellite dish, his emotion sparked
the team into action.

We didn't have a satellite signal but ... we did have a coaxial cable with an
RF connector - the kind that can plug into a Television and act as an antenae.
With all hands on deck, we plugged the cable into the television, strung the
other end out of the window (ensuring that the inside wire made contact with
the metal bars on the window), and, while grainy, we managed to get an over the
air broadcast signal from channel 7. We were back online!

## The closing

Listening to Evo speak made the whole experience come together. Naming
capitalism as a primary cause of environmental destruction opens up a
discussion virtually unheard of in the United States. Evo's closing, which
reflected the conference as a whole, grounded the discussion in the idea that
the destruction of the planet cannot be fixed by changing one or two things
here or there. Instead, we need an entirely new basis for relating to each
other and the world. 

The connection with indigenous movements, while not prescriptive, provided a
powerful testament to the historic transience of capitalism. The movement to
commodify and radically reduce the world and our interactions to profit is less
than 500 years old - a drop in the bucket of human history. 

And, if "500 years" rings a bell - it's because the rise of capitalism has
mirrored the genocide of indigenous peoples in the Americas, who, through
intense struggle and resistance, continue to maintain a world view that takes a
harmonious approach to the earth as a fundamental tenet.

Looking around the room during Evo's speech, I saw the exhausted but contented
faces of the other techies, who either took time off from their corporate jobs
or who have shunned the corporate tech sector altogether, to build
international alliances based on the principles of free and open source
software. I was reminded that the entire world does not run on capitalism, even
now. The growing movement of people from around the world who are dedicated to
building a free and open Internet, unfettered by the demands of profit and
capitalism, can provide a powerful contribution to the 21st century world
imagined by the conference participants - one based on collaboration and a
common good.

See the [final declaration of the conference](http://www.cmpcc.org.bo/PRINCIPALES-PROPUESTAS-DE-LA-CMPCC) (Spanish)
