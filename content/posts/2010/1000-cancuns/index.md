---
tags: ["mayfirst","cop16"]
title: "May First/People Link participates in 1000 Cancuns"
date: 2010-01-08T09:32:44.185780
---

We've setup a video watching station at [Blue Stockings
Bookstore](http://bluestockings.com), where we're handing out print outs of the
excellent climate solutions briefs on
[grassrootsclimatesolutions.net](http://grassrootsclimatesolutions.net/).

![Video station at Bluestockings](/posts/2010/1000-cancuns/bluestockings-cop16.jpg)

We're also preparing for the protest in fron the United Nations scheduled for
5:30 pm tonight. I just finished a [slide show](cop16.ogg) that will be
projected.
