---
tags: ["mayfirst","dakar2011"]
title: "Remarks given at WSF/Dakar Convergence Center Fundraiser"
date: 2010-01-08T09:32:44.185780
---

*On January 23, 2011, the Indy Media Convergence Center will open it's doors in Dakar, Senegal. Beginning two weeks before the start of the [World Social Forum](http://fsm2011.org/en/frontpage), the Convergence Center will be a space for collaboration and skill shares, bringing together media activists from all over Africa and the rest of the world. The gathering is a continuation of the work begun at two previous African convergence centers (Dakar in 2004 and Nairobi in 2007), with the goal of fostering deeper collaboration among African media activists and with the rest of the world.*

*[May First/People Link](http://mayfirst.org) plans to raise money to send one participant. The [US Social Forum](http://ussf2010.org) has allocated funding for a delegation, which will include three members of the [Information, Communications, and Technology working group](http://ict.ussf2010.org).*

*Below are remarks I made on a panel organized in New York City as a fundraiser for the event. The other panelists were Sean Jacobs from the New School, Omoyele Sowore of [Sahara Reporters](http://www.saharareporters.com/), Valentine Eben (aka "Sphinx") of [IMC-Ambazonia](http://ambazonia.indymedia.org/) and [The Way Forward Network](http://www.wfn.memberlodge.org/) and Mohamed Keita of the [Committee to Protect Journalists](http://www.cpj.org/).*

In April, 2010, tens of thousands of people from over a hundred countries came together in Cochabama, Bolivia to draft and affirm the Cochabamba People's accord on Climate Change and the Rights of Mother Earth.  

This gathering marked a critical point in our history for a number of reasons.

1. The accord called for systemic change, identifying capitalism as a fundamental cause of the destruction of the planet

2. The leadership and full participation of the indegenous peoples of the americas was strongly reflected in both the gathering and in the final accords.

3. The gathering reflected the power of democratization and the free flow of information.

This last point is particularly important for all of us here.

Thirty years ago, there were fewer than a handful of countries in Latin America that could make any kind of credible claim to democracy. Now, almost all countries can. It's no coincidence that this democratic transition brought with it a significant political shift to the left.  

It's also no coincidence that the launch of this political shift began with the Zapatistas and their innovative use of a newly massified medium called the Internet.

Despite the enormous differences between Latin America and Africa, in both regions democratizing media has an impact on political democracy and the global movement for justice. The Convergence Center in Dakar will be one contribution to this effort.

Our impact, however, goes even further with this project.

Over the last few centuries, we've experienced a theft, on a global level, of astounding proportions. A theft that, despite its complexities, nonetheless draws a clear line of transfer that travels from Africa to Europe, North America and a handful of other countries.

The IMF and World Bank's response to this theft is to provide charity and loans with steep conditions to African countries. 

May First/People Link's response is different: We recognize that the technology skills and training that we've developed in the United States do not belong to us alone. Our skills belong to the world. Sharing what we have is a political mandate. It is required of us.

The Convergence Center in Dakar is an opportunity for such sharing. 

Despite the global in-equalities, from the outset, the Convergence Center is based on real and equal collaboration. Not charity, not outside control, but a recognition that we all have something to contribute and something to learn. We're honored to be included. 

