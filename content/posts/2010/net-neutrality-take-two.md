---
tags: ["mayfirst", "net-neutrality"]
title: "Net Neutrality: May First/People Link Takes Two"
date: 2010-01-08T09:32:44.185780
---

When [net neutraliy](http://en.wikipedia.org/wiki/Net_neutrality) first
surfaced as an issue years ago, it was hard to get behind. Not the principle -
which states that providers of Internet access should supply a connection
without restrictions or preferences based on where or who the content comes
from - that *should* go without saying.

Instead, it was hard to get behind political organizing in support of this
principle. At the time, the debate was defined by two corporate giants: Google
was standing in support of net neutraliy, and Verizon was angling to charge
content providers like Google money to be given preferential bandwidth
treatment.

The battle grounds became the Google/Verizon corporate negotiating table and
the US federal regulatory agencies - both historically have served as
empowerment sink holes for anyone trying to build a political movement. 

Furthermore, given Google's various moves to control the Internet, it was hard
to publicly support an issue so clearly staked out by them. With all the
revolutionary work being done to develop free and open source software and
protocols, the global convergences of the world social forums and climate
change activists, stopping to help Google defeat an obviously bad idea proposed
by Verizon seemed like a red herring. 

As co-directors of May First/People Link, our leadership committee has given
Alfredo and I the political responsibility to provide day-to-day leadership
based on a solid assessment of every situation affecting the work of our
organization and members. We chose to steer our organization clear of net
neutrality, and now we realize that was a political mistake.

Last week, Google and Verizon announced that their feuding is over. They
released a joint [proposal for a legislative
framework](http://www.google.com/googleblogs/pdfs/verizon_google_legislative_framework_proposal_081010.pdf).
Despite predictable [applause from the tech
mainstream](http://www.zdnet.com/blog/google/the-real-google-verizon-net-neutrality-story/2353?pg=2),
the implications are fairly profound and dangerous.

You cannot get away with standing up in the United States and saying that net
neutrality is a bad idea. That's like saying publicly that profit should come
before people. Instead, the recommendation supports the principle of net
neutrality when it comes to a wired (e.g.  DSL or cable connection) connection,
but maintains that because of the "unique technical and operational
characteristics of wireless networks," wireless Internet delivery should be
exempt. 

Most of the commentary focused on how this will impact cell phone access to the
Internet - which is critically important. However, also important is the impact
on wide area wireless networks. Similar to the wireless network you might have
in your home or office, new wireless protocols are being developed that can
cover more than your living room - providing the possibility for your home
Internet connection being provided by anyone in your city - without the need
for installing a wire to your home.

Between cell phone access and wide area wireless access, DSL and Cable will
soon lose their status as the fastest growing way to get broad band Internet.
If the most promising future method of delivering Internet access is too
technically and operationally "unique" to be covered by the principle of net
neutrality, then the future development of the Internet is in real trouble.

Fortunately, we have a few things going for us.

The first and most important is that we have public opinion on our side. And we
should use it. This issue is not a technical one - the question is: when we use
the Internet, should we have equal, non-discriminatory access to everything?
Few will disagree.

Second, let's build alternatives. Most attempts at controlling the Internet
have failed miserably (consider the scrap heap of technologies to keep you from
copying your music and videos). When, in an early violation of net neutrality,
our service providers prevented us from sending email, we routed it through a
different port. We can and will test the limits of every attempt control and
profit from our usage of the Internet.

Most importantly, however, we need to join this fight. The future of *our*
Internet depends on it.
