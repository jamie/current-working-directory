---
tags: ["debian","sysadmin"]
title: "Growing disks: upgrading from 1 TB to 2 TB disks"
date: 2010-01-08T09:32:44.185780
---

We've got a fairly layered set of software that runs beneath our file systems: RAID1 followed by cryptsetup, followed by lvm. It works remarkably well. 

However, when it's time to do something like enlarge a partition or disk, it gets complicated.

We have a server (hubert) that was running two 1 TB disks in a RAID1. We typically create two partitions: one small boot partition and one large partition the uses the rest of the available disk (with 2 TB disks [we've added a third partition for grub](/posts/2010/two-tb-disks)). 

Last month I replaced one of the disks with a 2 TB disk, partitioned the new disk with similar a partition scheme (except that the "rest of the disk" partition was twice the size as the original one), and then added the new partitions to the RAID (mdadm /dev/md0 --add /dev/sda1; mdadm /dev/md1 --add /dev/sda2).

Yesterday, I replaced the 2nd 2 TB disks, and again added the new partitions.

The result was a fairly painless transition from 1 to 2 TB disks with minimal downtime.

The problem - though - is that we're not using all the disk space available, since md1, the RAID array that uses the "rest of the disk" partition, is only about 1 TB when it should be just under 2 TB.

That problem was easy enough to solve by growing the RAID device with:

    mdadm -G /dev/md1 -z max

It took a while, but eventually, running:

    cat /proc/mdstat

And

    cat /proc/partitions

Reported the right sizes for md1.

After RAID, comes cryptsetup (/dev/md1 is the source device for md1_crypt). So, the next command was:

    cryptsetup resize md1_crypt

In the output of:

    cat /proc/partitions

I saw the change reflected in the dm-0 device.

Since md1_crypt is used as the physical volume for a volume group, the next step was to resize the physical volume:

    pvresize /dev/dm-0

This change was reflected with:

    pvs

Now, the command:

    vgs

Displayed a full 931 GB available that wasn't there before.

Next up: resizing the logical volume:

    lvresize --size 1700GB vg_hubert0/blanco_members

And, last but not least, was the file system:

     resize2fs /dev/mapper/vg_hubert0-blanco_members

See also dkg's [article](http://www.debian-administration.org/article/Resizing_Encrypted_Filesystems) on resizing crypt partitions.
