---
tags: ["debian","sysadmin"]
title: "From vservers to KVM"
date: 2010-01-08T09:32:44.185780
---

Given the impending
[deprecation](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=574529) of
[vservers](http://linux-vserver.org/Welcome_to_Linux-VServer.org), I've decided
to make the switch to [KVM](http://www.linux-kvm.org/page/Main_Page) on my
laptop. Although [lxc](http://en.wikipedia.org/wiki/Lxc_Linux_Containers) is a
closer approximation to vservers, I decided to go with KVM due to it's support
in [Virtual Machine Manager](http://virt-manager.et.redhat.com/).

My first step was to confirm that my CPU would support kvm:

    egrep -o "svm|vmx" /proc/cpuinfo

If that command outputs either svm or vmx (depending on whether you have Intel
or AMD hardware) then your CPU supports virtualization.

I'm working on a host machine called chicken, which has a logical volume called
vg_chicken0. All vservers on chicken operate on a root filesystem that is
backed by their own logical volume.  

In this post, I'll describe the steps to convert the vserver hobo (which
operates on a filesystem mounted on the host in /var/lib/vservers/hobo and
is backed by the logical volume called vg\_chicken0-hobo\_root).

Both chicken and hobo are running debian squeeze.

vservers don't have a kernel installed or grub. KVM virtual servers need both. 

I was hoping I could simply enter the vserver, install both a kernal and grub
and be ready to go. However, grub installation will fail miserably because grub
can't figure out how to install on the underlying disk (which is hidden from the
vserver).

Next, I tried launching a kvm instance, passing a
[debirf](http://cmrg.fifthhorseman.net/wiki/debirf) generated ISO with the -c
(cdrom) option. However, grub recognized that it was being installed onto a
device that did not have a partition table (the logical volume was directly
formatted with a file system).

So, since I had disk space to spare, I created a new logical volume:

    lvcreate --size 5GB --name hobo_root_new vg_chicken0

I then added a [gpt](http://en.wikipedia.org/wiki/GUID_Partition_Table)
partition table (why not prepare for the coming 2TB disks?) and created two
partitions. One partition for grub2 and one for everything else:

    parted /dev/mapper/vg_chicken0-hobo_root_new mklabel gpt
    parted /dev/mapper/vg_chicken0-hobo_root_new unit s mkpart biosboot 2048 4095 
    parted /dev/mapper/vg_chicken0-hobo_root_new set 1 bios_grub on 
    parted /dev/mapper/vg_chicken0-hobo_root_new unit s mkpart primary 4096 

When prompted for the end of the last partition, choose: -1 and accept the adjustment.

I had to eyeball cat /proc/partitions to figure out which dm device was the
second partition (dm-19).

I then created a file system:

    mkfs -t ext3 /dev/dm-19

Mounted it:

    mount /dev/dm-19 /mnt

And rsync'ed the data:

    rsync -a /var/lib/vservers/hobo/ /mnt/

With the data in place, I chroot'ed and installed the packages I needed. When prompted, I chose not to install grub to the disk, because I wanted to wait until I had an environment in which the proper disk would be available to grub as it will when the virtual server boots (see below):

    chroot /mnt
    mount /proc
    aptitude install linux-image-2.6-amd64 grub2
    umount /proc
    exit

Then, I cleaned up:

    umount /mnt
    umount /var/lib/vservers/hobo
    lvremove vg_chicken0/hobo_root
    lvrename vg_chicken0/hobo_root_new hobo_root
    dmsetup remove /dev/mapper/vg_chicken0-hobo_root_newp1
    dmsetup remove /dev/mapper/vg_chicken0-hobo_root_newp2
    kpartx -d /dev/mapper/vg_chicken0-hobo_root

And I removed it from /etc/fstab.

Next, I created a new kvm virtual server, using the disk /dev/mapper/vg\_chicken0-hobo\_root and passing a debirf cd image with -c:

  virt-install --name hobo --ram 512 --disk /dev/mapper/vg_chicken0-hobo_root -c /usr/local/share/debian/ISOs/debirf-rescue_squeeze_2.6.32-5-vserver-amd64.iso

After logging in, I installed grub2 (aptitude update; aptitude install grub2) and then I installed grub:

    mount /dev/sda2 /mnt/
    grub-install --no-floppy --root-directory=/mnt /dev/sda

After running grub-install, edit /mnt/boot/grub/device.map so it reads:

    (hd0) /dev/sda

Then, rerun grub-install command.

I tried generating the grub.cfg file, but got an error message indicating that
grub-probe would not detect the device providing / (because I was running on a
ram file system from debirf).

I added the following to /mnt/etc/fstab:

    /dev/sda2  /     ext3 errors=remount-ro 0 1
    proc       /proc proc defaults          0 0

And then re-generate the initrd image:

    chroot /mnt
    mount /proc
    update-initramfs -u

So, I rebooted the virtual machine by typing:

    exit
    reboot 

This dropped me into a grub shell. I manually typed:

    root (hd0,gpt2)
    linux /vmlinuz root=/dev/sda2 ro
    initrd /initrd.img
    boot

Once booted, I logged in a completed the task with:

    update-grub
 
