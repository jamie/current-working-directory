---
title: "Identities"
date: 2014-01-08T09:32:44.109780
---

My OpenPGP and SSH public keys are available below.

## OpenPGP 

Import from the public key servers:

    gpg --keyserver keys.openpgp.org --recv-key 1F9C30CB3CFC5DA9987FA035A014C05A607B7535 

Or, [click to download my public GPG key.](/misc/jamie.asc)

## SSH public key

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDiGm0ZoC/lgBDfyeRKcciXQWsJ/odB3zOS/8YMpTCMy8bPbRIZwNvOlIh6EG17bWzAKfMKoivY9QncXjtUeFBq4lDqxuxe2UU29Pwux01jdU+QOZ73M65/hVMFYEkV5qJAc8yf2WNq9vX472eIE6rQlo4DmtNJnuGvoQpF51ZV3uZUqc9QmT9+Vwg3RaOChKzTG3beE+SCJkrMxsMq0jhAT4BR1Xoknyo20Gmr3F7l84Un7U5YwFTRHNKs32nXv0Yhfdd7qhxZLyB4gQx+pVq/0kYCyjzhb2AZmmtLcAs7Btop045k1fbjmOkxnWDqw8XX+AAaNsNn85a8GZU1GcRn MonkeySphere2014-01-08T14:39:16 James McClelland <jamie@mayfirst.org>
