---
title: "Jamie's Bio"
date: 2014-01-08T09:32:44.109780
---

![Jamie with with his family on the subway in custumes](/bio/jamie-with-family.jpg)

Jamie is technology systems director for the [Progressive Technology
Project](http://progressivetech.org), a national organization providing
technology support to grassroots organizing groups in the US. 

Jamie is also co-founder and Board member of [May First Movement
Technology](http://mayfirst.coop), a membership organization of progressive
groups worldwide who use the Internet.  In his work with May First, Jamie does
political organizing, systems administration, and support for the members of
May First. 

May First is the result of a merger in 2005 between May First Technology
Collective (originally known as Media Jumpstart) and People-Link.  Prior to the
merger, Jamie was co-founder and co-director of May First Technology
Collective, a worker run nonprofit organization that provided technical support
to NYC's social justice movement groups.

Prior to working at May First, Jamie worked at Libraries for the Future as
network administrator, national Youth ACCESS coordinator, and Information and
Technology Policy Specialist.  Jamie was formerly on the Board of Directors of
Paper Tiger TV where he was an active producer and activist between 1994 and
2004. Previously, Jamie worked as a video instructor for Sidewalks of New York,
teaching basic production skills to homeless youth. He also worked as a
community organizer for the Association of Community Organizations for
Reform Now (ACORN) and was an active member of ACT UP New Orleans. 

You can reach jamie at jamie @ workingdirectory dot net.
