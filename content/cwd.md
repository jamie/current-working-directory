---
Title: What does current working directory mean? 
---

In computer terms, the current working directory (or just working directory)
refers to the directory or folder from where the running program or process is
operating. In a revision control system it refers to a local, working copy of
data - as opposed to the central, shared, or published copy of the data.

In the case of this site - it referes to the place to find out what I'm
currently working on.
